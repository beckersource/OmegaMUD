#!/bin/bash

FILES="$(git status --porcelain | grep ^.*\.java$ | sed s/^...//)"
if [ "$FILES" != "" ]; then
    for i in ${FILES[@]}; do
        ./_formatter.sh ${i##*/}
    done
fi
