#!/bin/bash

FILENAME="$1"
if [ "$FILENAME" == "" ]; then FILENAME="*.java"; fi;

# convert tabs to 4-spaces (expand)
# remove extra spaces on the end of lines
# add a trailing linefeed to the end of each file for consistency (see next)
# remove top/bottom lines that are just a linefeed (includes spaces also)
find ./src -type f -name "$FILENAME" -exec bash -c ' \
    echo "Formatting: $0" && \
    expand -t 4 "$0" > "$0.bak" && mv "$0.bak" "$0" && \
    sed -i "s/ *$//" "$0" && \
    echo >> "$0" && \
    sed -i -e :a -e "/[^[:blank:]]/,\$!d; /^[[:space:]]*\$/{ \$d; N; ba" -e "}" "$0" \
    ' {} \;
