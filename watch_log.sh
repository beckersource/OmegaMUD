#!/bin/bash

FILE="$1"
if [ "$FILE" == "" ]; then FILE="log_mud_buffer.log"; fi
watch -t -n 1 "tail -25 $FILE"
