#!/bin/bash

JAR_OMUD="OmegaMUD_$1.jar"
BUILD_DIR="_BUILD_$1"
LIB_DIR="lib/$1"
MAN="MANIFEST.MF"
SRC="src"
LIB_FILES=""
NEW_BUILD="$2"

# ------------
# Create Build Dir Structure
# ------------
if [ -f $JAR_OMUD  ]; then rm $JAR_OMUD; fi;
if [ "$NEW_BUILD" == "1" ]; then rm -rf $BUILD_DIR; fi;
if [ -d $BUILD_DIR ]; then
    rm -f $BUILD_DIR/*.class
else
    NEW_BUILD="1"
    mkdir -p $BUILD_DIR/fonts
    cp fonts/*     $BUILD_DIR/fonts 2>/dev/null
    cp src/$MAN    $BUILD_DIR       2>/dev/null    
    cp $LIB_DIR/*  $BUILD_DIR       2>/dev/null
fi


echo -------------------------
echo Build: \"$JAR_OMUD\" \"$JAVA_HOME\"
echo -------------------------
echo ""

# ------------
# Compile
# ------------
# build lib file list...
LIB_FILES=$(find $SRC -type f -name *.jar | sed "s/.*\//$BUILD_DIR\//; s/\.java/.class/")
for file in $LIB_DIR/*; do LIB_FILES="$LIB_FILES$BUILD_DIR/${file##*/}:"; done
# get source dirs...
SRC_DIRS=$(find $SRC -type d -exec printf "{}/*.java " \;)
# compile...
javac -Xlint:deprecation -Xlint:unchecked -d $BUILD_DIR -cp $LIB_FILES $SRC_DIRS

# ------------
# Check Class Files
# ------------
SRC_FILES=$(find $SRC -type f -name *.java | sed "s/.*\//$BUILD_DIR\//; s/\.java/.class/")
for CLASS_FILE in $SRC_FILES; do
	if [ ! -f $CLASS_FILE ]; then
		echo ""
		echo ---------------------------------------------
	    echo Failed: class file not found: $CLASS_FILE
	    exit 0
	fi
done

# ------------
# Create Jar
# ------------
cd $BUILD_DIR
# expand the lib jars on a new build...
if [ "$NEW_BUILD" == "1" ]; then for file in *.jar; do jar xf ${file##*/}; done; fi;
# create the omud jar...
jar cfm $JAR_OMUD $MAN *.class fonts org com net
cd ../

# ------------
# Clean Up + Run Jar
# ------------
if [ -f $BUILD_DIR/$JAR_OMUD ]; then
	mv $BUILD_DIR/$JAR_OMUD .
	java -jar $JAR_OMUD
fi
