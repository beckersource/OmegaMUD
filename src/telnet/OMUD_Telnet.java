import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.net.telnet.TelnetClient;
import org.apache.commons.net.telnet.EchoOptionHandler;
import org.apache.commons.net.telnet.TelnetNotificationHandler;
import org.apache.commons.net.telnet.TerminalTypeOptionHandler;
import org.apache.commons.net.telnet.SuppressGAOptionHandler;
import org.apache.commons.net.telnet.InvalidTelnetOptionException;
import org.apache.commons.net.telnet.TelnetCommand;

public class OMUD_Telnet {
    private OMUD_ITelnetEvents          _omte =         null;
    private OMUD_Char_Data              _omcd =         null;
    private ReentrantLock               _lockGUI =      null;
    private ReentrantLock               _lockParse =    null;
    private RunApache                   _runApache =    null;
    private Thread                      _threadApache = null;
    private TelnetClient                _tnc =          null;
    private TerminalTypeOptionHandler   _tncOptType =   null;
    private EchoOptionHandler           _tncOptEcho  =  null;
    private SuppressGAOptionHandler     _tncOptGA  =    null;
    private Timer                       _tmrAYT =       null;
    private TimerTask                   _taskAYT =      null;
    private StringBuilder               _sbCmdBuild =   null;
    private ArrayList<String>           _arrlCmdsNew =  null;
    private ArrayList<String>           _arrlCmdsLF =   null;
    private boolean                     _allow_send =   true;
    private boolean                     _smode_cmd =    false;
    private long                        _ayt_last_response_time_ms = 0;
    private static OMUD_TelnetParser _omtp = new OMUD_TelnetParser();
    private static OMUD_MMUD_Parser  _mmp  = new OMUD_MMUD_Parser();
    private final String    TERMINAL_TYPE =     "ANSI";
    private final String    TERMINAL_CHARSET =  "ISO-8859-1";
    private final String    TELNET_NEG_DO =     "DO";
    private final String    TELNET_NEG_DONT =   "DONT";
    private final String    TELNET_NEG_WILL =   "WILL";
    private final String    TELNET_NEG_WONT =   "WONT";
    private final int       TELNET_BUF_SIZE =   1024;
    private final long      TELNET_AYT_MS =     2000;

    public OMUD_Telnet(OMUD_ITelnetEvents omte, OMUD_Char_Data omcd) throws IOException {
        _omte =         omte;
        _omcd =         omcd;
        _lockGUI =      new ReentrantLock(true);
        _lockParse =    new ReentrantLock(true);
        _runApache =    new RunApache();
        _sbCmdBuild =   new StringBuilder();
        _arrlCmdsNew =  new ArrayList<String>();
        _arrlCmdsLF =   new ArrayList<String>();

        // create telet client object...
        _tncOptType = new TerminalTypeOptionHandler(TERMINAL_TYPE, false, false, true, false);
        _tncOptEcho = new EchoOptionHandler(true, false, true, false);
        _tncOptGA =   new SuppressGAOptionHandler(true, true, true, true);
        _tnc =        new TelnetClient();
        try {
            _tnc.addOptionHandler(_tncOptType);
            _tnc.addOptionHandler(_tncOptEcho);
            _tnc.addOptionHandler(_tncOptGA);
        } catch (InvalidTelnetOptionException e) {
            OMUD_Log.toConsoleError("Telnet: error registering option handlers: " + e.getMessage());
        }
    }

    // --------------
    // External Funcs
    // --------------
    // forceUpdateMUD(): called to force an update within MUD (just timers at the moment).
    // Already on a separate timer thread, so just call parseData() direct without another thread...
    public void forceUpdateMUD(){
        if (isConnected()){
            _lockParse.lock();
                parseData("", false);
            _lockParse.unlock();
        } else OMUD_Log.toConsoleInfo("Telnet: can't force update MUD: disconnected!");
    }

    private class ThreadSend extends Thread{
        private boolean _ready = false;
        public boolean isReady(){return _ready;}
        public void run(){
            String text = "";

            _lockGUI.lock();
                text = _arrlCmdsNew.get(0);
                _arrlCmdsNew.remove(0);
            _lockGUI.unlock();

            _ready = true;
            //OMUD_Log.toFile(OMUD_Log.eLogTypes.TELNET_CMDS, "GUI: " + text);

            _lockParse.lock();
                sendTextInternal(text);
            _lockParse.unlock();
        }
    }

    public void sendText(final String text){
        if (text.length() > 0){
            _lockGUI.lock();
                _arrlCmdsNew.add(text);
            _lockGUI.unlock();

            ThreadSend thread = new ThreadSend();
            thread.start();
            try{ while (!thread.isReady()) Thread.sleep(1);
            } catch (Exception e) {OMUD_Log.toConsoleError("Error waiting for send thread ready: " + e.getMessage());}
        }
    }

    public void clearCmds(){
        reset("");
    }

    // --------------
    // Sending Text
    // --------------
    private void sendTextInternal(String text){
        if (isConnected()){

            // text sent through GUI or manual cmd...
            if (text.length() > 0){
                boolean has_lf = text.charAt(text.length() - 1) == OMUD.ASCII_LF;

                // validate text to send:
                // check the first character for a valid range of printable chars and enter.
                // only really matters for single mode, but just check every time for simplicity...
                if (has_lf || (text.charAt(0) >= OMUD.ASCII_SPC && text.charAt(0) < OMUD.ASCII_DEL)){

                    int prev_build_len = _sbCmdBuild.length();

                    _sbCmdBuild.append(text);
                    if (has_lf){
                        //OMUD_Log.toFile(OMUD_Log.eLogTypes.TELNET_CMDS, "ADD: " + _sbCmdBuild);
                        _arrlCmdsLF.add(_sbCmdBuild.toString());

                        // non-single mode: set the text here to the first cmd -
                        // do this to preserve single-mode input on the send...
                        if (prev_build_len == 0){
                            text = _arrlCmdsLF.get(0);
                            _smode_cmd = false;
                        } else {
                            _smode_cmd = true;
                        }

                        _sbCmdBuild.setLength(0);
                    }
                // if backspace, delete the last char...
                } else if (text.charAt(0) == OMUD.ASCII_BS){
                    if (_sbCmdBuild.length() > 0)
                        _sbCmdBuild.deleteCharAt(_sbCmdBuild.length() - 1);
                }
            }

            // send for LF cmds is prevented when a command is sent and
            // until mud says it's ready for another...
            if (_allow_send){

                // only set below if coming from parser -
                // will preserver single-mode input this way...
                if (text.length() == 0 && _arrlCmdsLF.size() > 0)
                    text = _arrlCmdsLF.get(0);

                if (text.length() > 0){
                    _allow_send = text.charAt(text.length() - 1) != OMUD.ASCII_LF; // covers single and LF sends
                    try {
                        //OMUD_Log.toFile(OMUD_Log.eLogTypes.TELNET_CMDS, "OUT: " + text);
                        _tnc.getOutputStream().write(text.getBytes(), 0, text.length());
                        _tnc.getOutputStream().flush();
                    } catch (Exception e) {
                        OMUD_Log.toConsoleError("Telnet: error sending: " + e.getMessage());
                    }
                }
            }
        } else {
            OMUD_Log.toConsoleInfo("Telnet: can't send text: not connected: " + text);
        }
    }

    // --------------
    // Parsing Data
    // --------------
    private void reset(final String text){
        new Thread(){ public void run() {
            _lockParse.lock();
                _sbCmdBuild.setLength(0);
                _arrlCmdsNew.clear();
                _arrlCmdsLF.clear();
                _allow_send = true;
                parseData(text, true);
            _lockParse.unlock();
        }}.start();
    }

    private void parseData(String strData, boolean clear_mud_buffer){
        if (strData.length() > 0)
            _omcd.getParseData().sbNewData.append(strData);
        if (_omcd.getParseData().sbNewData.length() > 0)
            _omtp.parseData(_omte, _omcd);

        // queued LF-based commands...
        if (!clear_mud_buffer && _arrlCmdsLF.size() > 0){
            if (_mmp.parseData(_omcd, _arrlCmdsLF.get(0), _arrlCmdsLF.size() - 1, _smode_cmd)){
                //OMUD_Log.toFile(OMUD_Log.eLogTypes.TELNET_CMDS, "REM: " + _arrlCmdsLF.get(0));
                _allow_send = true;
                _arrlCmdsLF.remove(0);
                sendTextInternal("");
            }
        // forced parse requests from events (timers, etc)...
        } else if (_mmp.parseData(_omcd, "", clear_mud_buffer ? -1 : 0, false)){
            _allow_send = true;
        }
    }

    // --------------
    // Apache/Telnet
    // --------------
    // TimerTaskAYT: a fake AYT that checks for a connection.
    // Real AYT responses are server-custom and send printed text, which is not what we want.
    // Just send a NOP byte and check for an exception/failure on send.
    private class TimerTaskAYT extends TimerTask{
        public void run(){
            long current_time_ms = System.currentTimeMillis();
            if (current_time_ms - _ayt_last_response_time_ms >= TELNET_AYT_MS){
                //OMUD_Log.toConsoleInfo("Telnet: checking idle connection...");
                try {
                    _tnc.sendCommand((byte) TelnetCommand.NOP);
                    _ayt_last_response_time_ms = current_time_ms;
                } catch (Exception e){
                    disconnect(false);
                }
            }
        }
    }

    private class RunApache implements Runnable, TelnetNotificationHandler {
        private BufferedReader  _instrBuf = null;
        private char[]          _data =     null;
        private int             _data_len = 0;

        public void run() {
            // use a buffered reader to force the ISO char set to show characters properly on all systems -
            // windows was displaying correctly but linux wasn't showing extended ASCII correctly and
            // was displaying text very slow.  not sure about mac but just do this anyway.
            try {
                _instrBuf = new BufferedReader(new InputStreamReader(_tnc.getInputStream(), TERMINAL_CHARSET));
            } catch (Exception e) {
                OMUD_Log.toConsoleError("Telnet: error creating buffered reader:" + e.getMessage());
            }

            _data_len = 0;
            _data = new char[TELNET_BUF_SIZE];
            do {
                try {
                    _data_len = _instrBuf.read(_data, 0, TELNET_BUF_SIZE); // read() is a blocking call
                } catch (Exception e) {
                    OMUD_Log.toConsoleError("Telnet: error reading socket:" + e.getMessage());
                }

                // not sure when length would ever be zero if data was read?
                if (_data_len > 0){
                    _ayt_last_response_time_ms = System.currentTimeMillis();

                    String strData = new String(_data, 0, _data_len).replaceAll(OMUD.ASCII_CR_REGEX, ""); // strip all CR here for consistency

                    _lockParse.lock();
                        parseData(strData, false);
                    _lockParse.unlock();

                    OMUD_Log.toFile(OMUD_Log.eLogTypes.TELNET_RAW, strData);
                    //OMUD_Log.toFile(OMUD_Log.eLogTypes.TELNET_DEBUG,
                    //    "\n----[DATA LEN: " + strData.length() + "]----\n" + strData + "\n------------------------\n");
                }
            } while (_data_len > -1);

            try {
                _tnc.disconnect();
            } catch (Exception e) {
                OMUD_Log.toConsoleError("Telnet: error closing telnet:" + e.getMessage());
            }
        }

        // receivedNegotiation(): telnet negotiations...
        public void receivedNegotiation(int negotiation_code, int option_code) {
            if (_ayt_last_response_time_ms == 0){
                OMUD_Log.toConsoleInfo("Telnet: connected!");
                _omte.notifyTelnetEvent(new OMUD_TelnetEvent_Connected());
            }
            _ayt_last_response_time_ms = System.currentTimeMillis();

            String command = null;
            if (negotiation_code == TelnetNotificationHandler.RECEIVED_DO) {
                command = TELNET_NEG_DO;
            } else if (negotiation_code == TelnetNotificationHandler.RECEIVED_DONT) {
                command = TELNET_NEG_DONT;
            } else if (negotiation_code == TelnetNotificationHandler.RECEIVED_WILL) {
                command = TELNET_NEG_WILL;
            } else if (negotiation_code == TelnetNotificationHandler.RECEIVED_WONT) {
                command = TELNET_NEG_WONT;
            }
            OMUD_Log.toConsoleInfo("Telnet: cmd (" + command + "), opt (" + option_code + ")");
        }
    }

    // --------------
    // Connect/Disconnect/AYT
    // --------------
    public boolean isConnected(){return _threadApache != null;}
    public void connect(String strTelnetAdr, String strTelnetPort){
        if (isConnected()){
            OMUD_Log.toConsoleInfo("Telnet: can't connect: already connected!");
        } else {
            try{
                OMUD_Log.toConsoleInfo("Telnet: attempting connection: " + strTelnetAdr + ":" + strTelnetPort);
                _tnc.registerNotifHandler(_runApache);
                _tnc.connect(strTelnetAdr, Integer.parseInt(strTelnetPort));
                _threadApache = new Thread(_runApache);
                _threadApache.start();
                // create timer/task for connectivity checks...
                _ayt_last_response_time_ms = 0;
                _taskAYT = new TimerTaskAYT();
                _tmrAYT =  new Timer();
                _tmrAYT.schedule(_taskAYT, TELNET_AYT_MS, TELNET_AYT_MS);
            } catch (Exception e) {
                OMUD_Log.toConsoleError("Telnet: error on connect: " + e.getMessage());
            }
        }
    }

    public void disconnect(boolean user_request){
        if (!isConnected()){
            OMUD_Log.toConsoleInfo("Telnet: can't disconnect: already disconnected!");
        } else {
            try{
                if (user_request)
                     OMUD_Log.toConsoleInfo("Telnet: disconnect: user request");
                else OMUD_Log.toConsoleInfo("Telnet: disconnect: timed out");

                _tmrAYT.cancel();
                _tmrAYT.purge();
                _tnc.disconnect();
                _tnc.unregisterNotifHandler();
                _threadApache.interrupt();
                _threadApache = null;
                _omte.notifyTelnetEvent(new OMUD_TelnetEvent_Disconnected());
                _omcd.reset(OMUD.eBBSLoc.OFFLINE);
                reset("\n\n"); // add a couple linefeeds for text readability on reconnect (threaded)
            } catch (Exception e) {
                OMUD_Log.toConsoleError("Telnet: error on disconnect: " + e.getMessage());
            }
        }
    }
}
