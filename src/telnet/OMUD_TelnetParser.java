import java.util.ArrayList;

public class OMUD_TelnetParser{

    // --------------
    // Cursor Move Functions
    // --------------
    // crsrMoveUpDown(): move cursor up/down by relative or absolute value.
    // move_type ==  0: absolute: move direct to #
    // move_type ==  1: relative: move up from current by #
    // move_type == -1: relative: move down from current by #
    private static void crsrMoveUpDown(OMUD_Char_Data.DataParse dp, ArrayList<OMUD_BufferMod> arrlBMods, String strDbgFnc, int move_type, int move_val, OMUD_Buffer.RowData rdata){
        int pos_col = dp.omb.getPos() - (OMUD.getPrevLF(dp.omb.getText(), dp.omb.getPos()) + 1);
        int caret_pos_new = 0;

        if (move_val > 0){
            if (move_type == 0){
                dp.omb.setCrsrRowNum(move_val);
            } else {
                int new_row = dp.omb.getCrsrRowNum() + (move_type == 1 ? -move_val : move_val);
                rdata = dp.omb.getRowData(new_row);
                dp.omb.setCrsrRowNum(new_row);
            }

            // if the destination line isn't long enough for the new caret pos, insert spaces...
            if ((caret_pos_new = rdata.pos_start + pos_col) > rdata.pos_end){
                OMUD_ANSI ansi_spacer = new OMUD_ANSI();
                ansi_spacer.getText().append(OMUD.getFillString(" ", caret_pos_new - rdata.pos_end));
                arrlBMods.add(new OMUD_BufferModInsert(strDbgFnc, dp.omb, ansi_spacer, rdata.pos_end + 1, caret_pos_new, rdata.pos_end));
            // else just set the new pos...
            } else {
                arrlBMods.add(new OMUD_BufferModMove(strDbgFnc, dp.omb, caret_pos_new));
            }
        }
    }

    // crsrMoveLeftRight(): move cursor left/right by relative or absolute value.
    // move_type ==  0: absolute: move direct to #
    // move_type ==  1: relative: move right from current by #
    // move_type == -1: relative: move left from current by #
    private static void crsrMoveLeftRight(OMUD_Char_Data.DataParse dp, ArrayList<OMUD_BufferMod> arrlBMods, String strDbgFnc, int move_type, int move_val, OMUD_Buffer.RowData rdata){
        int caret_row_start = 0;
        int caret_row_end =   0;
        int caret_pos_new =   0;

        // get new col based on move type...
        if (move_val > 0){
            if (move_type == 0 && rdata != null){
                caret_row_start = rdata.pos_start;
                caret_row_end =   rdata.pos_end;
                caret_pos_new =   rdata.pos_start + move_val - 1; // -1 for zero-base
            } else if (move_type != 0){
                caret_row_start = OMUD.getPrevLF(dp.omb.getText(), dp.omb.getPos()) + 1;
                caret_row_end =   OMUD.getNextLF(dp.omb.getText(), dp.omb.getPos()) - 1;
                if (caret_row_end < 0)
                    caret_row_end = dp.omb.getText().length();
                caret_pos_new = dp.omb.getPos() + (move_type == -1 ? -move_val : move_val);
            }

            // if not enough space, do an insert and move the cursor using the insert mod...
            if (caret_pos_new > caret_row_end){
                OMUD_ANSI ansi_spacer = new OMUD_ANSI();
                ansi_spacer.getText().append(OMUD.getFillString(" ", caret_pos_new - caret_row_end));
                arrlBMods.add(new OMUD_BufferModInsert(strDbgFnc, dp.omb, ansi_spacer, caret_row_end, caret_pos_new));
            // else moving within bounds...
            } else {
                if (caret_pos_new < caret_row_start)
                    caret_pos_new = caret_row_start;
                arrlBMods.add(new OMUD_BufferModMove(strDbgFnc, dp.omb, caret_pos_new));
            }
        }
    }

    // --------------
    // Parsing
    // --------------
    private static void addANSIMods(OMUD_Char_Data.DataParse dp, ArrayList<OMUD_BufferMod> arrlBMods){
        if (dp.ansi.getText().length() > 0){
            arrlBMods.add(new OMUD_BufferModInsert("0", dp.omb, dp.ansi, dp.omb.getPos(), dp.omb.getPos() + dp.ansi.getText().length()));
            dp.ansi.getText().setLength(0);
        }
    }

    public static void parseData(OMUD_ITelnetEvents omte, OMUD_Char_Data omcd){
        OMUD_Char_Data.DataParse dp = omcd.getParseData();

        boolean regular_char = false;
        ArrayList<OMUD_BufferMod> arrlBMods = new ArrayList<OMUD_BufferMod>();
        char c = 0;
        int  d = 0;
        while  (dp.sbNewData.length() > 0){
            c = dp.sbNewData.charAt(0);
            d = (int) c;
            dp.sbNewData.deleteCharAt(0);

            // 0x00-0x1F: check for ANSI control codes...
            if (d < OMUD.ASCII_SPC || d == OMUD.ASCII_DEL){
                addANSIMods(dp, arrlBMods);

                // we shouldn't be here with an existing escape sequence - reset the escape sequence...
                if (dp.sbEscSeq.length() > 0)
                    dp.sbEscSeq.setLength(0);

                // BEL/bell...
                if (d == OMUD.ASCII_BEL){
                    // ignore (for now)
                // BS/backspace...
                } else if (d == OMUD.ASCII_BS){
                    if (dp.ansi.getText().length() > 0){
                        arrlBMods.add(new OMUD_BufferModInsert("BS", dp.omb, dp.ansi, dp.omb.getPos(), dp.omb.getPos() + dp.ansi.getText().length()));
                        dp.ansi.getText().setLength(0);
                    }

                    // backspacing at the end of the buffer...
                    if (dp.omb.getPos() == dp.omb.getText().length()){
                        arrlBMods.add(new OMUD_BufferModRemove("BS", dp.omb, dp.omb.getPos() - 1, 1, dp.omb.getPos() - 1, false));
                    // backspacing within a row somewhere...
                    } else if (dp.omb.getPos() - 1 > 0){
                        dp.ansi.getText().append(" ");
                        arrlBMods.add(new OMUD_BufferModInsert("BS", dp.omb, dp.ansi, dp.omb.getPos() - 1, dp.omb.getPos() - 1));
                        dp.ansi.getText().setLength(0);
                    }

                    // MajorMUD injects a random "alpha char+BS" sequence into each exit type string (like below),
                    // so just manually delete the last char that was added.  See example:
                    // Example: nEorth, eHast, wUest, dTown
                    dp.deleteLastChar();

                // LF: Process linefeeds immediately:
                // [1] Functions like 'ESC[K' (clear from cursor to end and don't move cursor)
                // can be the last thing on a row that is not full, and will have added extra spaces.
                // In that case, we need to move the buffer pos to the end before placing the linefeed.
                // [2] This also helps prevent extra complex logic for overwrite code and
                // large blocks of text with multiple linefeeds inside.
                } else if (d == OMUD.ASCII_LF){
                    // if current ansi text is empty, force the buffer pos to the very end (above notes) -
                    // otherwise use current buffer position...
                    int pos_offset = dp.ansi.getText().length() == 0 ? dp.omb.getText().length() : dp.omb.getPos();
                    dp.ansi.getText().append(c);
                    arrlBMods.add(new OMUD_BufferModInsert("LF", dp.omb, dp.ansi, pos_offset, pos_offset + dp.ansi.getText().length()));
                    dp.ansi.getText().setLength(0);

                    // majormud needs LF for finding game strings...
                    dp.appendChar(OMUD.ASCII_LF);

                // CR...
                } else if (d == OMUD.ASCII_CR){
                    OMUD_Log.toConsoleInfo("ASCII CR found ignored (should already be stripped).");
                // ESC/ANSI sequence...
                } else if (d == OMUD.ASCII_ESC){
                    dp.sbEscSeq.append(c);
                    // show/clear the current out text to use already processed sequence styles, modes, and colors...
                    if (dp.ansi.getText().length() > 0){
                        arrlBMods.add(new OMUD_BufferModInsert("ESC", dp.omb, dp.ansi, dp.omb.getPos(), dp.omb.getPos() + dp.ansi.getText().length()));
                        dp.ansi.getText().setLength(0);
                    }
                // tabs and form-feed: unhandled for now  until detected -
                // use error message for visibility...
                } else if (d == OMUD.ASCII_HTB || d == OMUD.ASCII_VTB || d == OMUD.ASCII_FF){
                    regular_char = true;
                    OMUD_Log.toConsoleError("Error: found unhandled tab or formfeed: " + d);
                // else show as printed symbol...
                } else {
                    regular_char = true;
                }
            // 0x20–0x2F: part 2 of escape sequence (zero or many)...
            } else if (d < OMUD.ASCII_ZRO){
                if (dp.sbEscSeq.length() > 0){
                    dp.sbEscSeq.append(c);

                // special: check for ANSI RIP escape sequence query (and ignore)...
                if (d == OMUD.ASCII_EXC &&
                    dp.sbEscSeq.length() > 2 &&
                    dp.sbEscSeq.charAt(0) == OMUD.ASCII_ESC &&
                    dp.sbEscSeq.charAt(1) == OMUD.ASCII_LBR &&
                    (OMUD.compareSBString(dp.sbEscSeq, OMUD.RIP_QVERSION1) ||
                     OMUD.compareSBString(dp.sbEscSeq, OMUD.RIP_QVERSION2) ||
                     OMUD.compareSBString(dp.sbEscSeq, OMUD.RIP_OFF)       ||
                     OMUD.compareSBString(dp.sbEscSeq, OMUD.RIP_ON))){
                        dp.sbEscSeq.setLength(0);
                        OMUD_Log.toConsoleInfo("RIPscrip ANSI escape sequence ignored.");
                    }
                } else regular_char = true;
            // 0x30–0x3F: part 1 of escape sequence (zero or many)...
            } else if (d < OMUD.ASCII_AT){
                if (dp.sbEscSeq.length() > 0)
                    dp.sbEscSeq.append(c);
                else regular_char = true;
            // 0x40–0x7E: '[' char after ESC byte for CSI or final byte for ESC sequence...
            } else if (d < OMUD.ASCII_DEL){
                if (dp.sbEscSeq.length() > 0){
                    dp.sbEscSeq.append(c);

                    // ------------------
                    // Check for Completed "ESC[" Sequences
                    // ------------------
                    if (dp.sbEscSeq.length() > 2){
                        dp.sbEscSeq.delete(0, 2); // remove the "ESC[" prefix

                        // add mods for existing ANSI data...
                        addANSIMods(dp, arrlBMods);

                        // some convenience vars for processing below...
                        int  esc_seq_last_pos  = dp.sbEscSeq.length() - 1;
                        char esc_seq_last_char = dp.sbEscSeq.charAt(esc_seq_last_pos);

                        // ------------------
                        // Graphics Functions
                        // ------------------
                        if (esc_seq_last_char == OMUD.CSI_GRAPHICS){
                            // majormud game string compare needs the escape sequence prefix...
                            dp.appendChar(OMUD.ASCII_ESC);
                            dp.appendChar(OMUD.ASCII_LBR);
                            dp.appendSB(dp.sbEscSeq);

                            dp.sbEscSeq.deleteCharAt(esc_seq_last_pos); // strip last char (we know the function now)

                            // split with ; to compensate for multiple modes...
                            String[] strFuncs = dp.sbEscSeq.toString().split(";");
                            for (String code : strFuncs) {
                                // Styles: ENABLE...
                                if (code.equals("0")){
                                    dp.ansi.resetStyles();
                                    dp.ansi.resetFG();
                                    dp.ansi.resetBG();
                                } else if (code.equals("1")){
                                    dp.ansi.setBold(true);
                                //} else if (code.equals("2")){
                                //    dp.ansi.setDim(true);
                                //} else if (code.equals("3")){
                                //    dp.ansi.setItalic(true);
                                //} else if (code.equals("4")){
                                //    dp.ansi.setUnderline(true);
                                } else if (code.equals("5")){
                                    dp.ansi.setBlink(true);
                                } else if (code.equals("7")){
                                    dp.ansi.setInvert(true);
                                //} else if (code.equals("8")){
                                //    dp.ansi.setInvis(true);
                                //} else if (code.equals("9")){
                                //    dp.ansi.setStrike(true)

                                // Styles: DISABLE...
                                } else if (code.equals("22")){
                                    dp.ansi.setBold(false);
                                    dp.ansi.setDim(false);
                                //} else if (code.equals("23")){
                                //    dp.ansi.setItalic(false);
                                //} else if (code.equals("24")){
                                //    dp.ansi.setUnderline(false);
                                } else if (code.equals("25")){
                                    dp.ansi.setBlink(false);
                                //} else if (code.equals("27")){
                                //    dp.ansi.setInvert(false);
                                //} else if (code.equals("28")){
                                //    dp.ansi.setInvis(false);
                                //} else if (code.equals("29")){
                                //    dp.ansi.setStrike(false)

                                // Colors 8: FG...
                                } else if (code.equals("30")){
                                    dp.ansi.setFG(OMUD.eANSIColors.BLACK);
                                } else if (code.equals("31")){
                                    dp.ansi.setFG(OMUD.eANSIColors.RED);
                                } else if (code.equals("32")){
                                    dp.ansi.setFG(OMUD.eANSIColors.GREEN);
                                } else if (code.equals("33")){
                                    dp.ansi.setFG(OMUD.eANSIColors.YELLOW);
                                } else if (code.equals("34")){
                                    dp.ansi.setFG(OMUD.eANSIColors.BLUE);
                                } else if (code.equals("35")){
                                    dp.ansi.setFG(OMUD.eANSIColors.MAGENTA);
                                } else if (code.equals("36")){
                                    dp.ansi.setFG(OMUD.eANSIColors.CYAN);
                                } else if (code.equals("37")){
                                    dp.ansi.setFG(OMUD.eANSIColors.WHITE);
                                } else if (code.equals("39")){
                                    dp.ansi.resetFG();

                                // Colors 8: BG...
                                } else if (code.equals("40")){
                                    dp.ansi.setBG(OMUD.eANSIColors.BLACK);
                                } else if (code.equals("41")){
                                    dp.ansi.setBG(OMUD.eANSIColors.RED);
                                } else if (code.equals("42")){
                                    dp.ansi.setBG(OMUD.eANSIColors.GREEN);
                                } else if (code.equals("43")){
                                    dp.ansi.setBG(OMUD.eANSIColors.YELLOW);
                                } else if (code.equals("44")){
                                    dp.ansi.setBG(OMUD.eANSIColors.BLUE);
                                } else if (code.equals("45")){
                                    dp.ansi.setBG(OMUD.eANSIColors.MAGENTA);
                                } else if (code.equals("46")){
                                    dp.ansi.setBG(OMUD.eANSIColors.CYAN);
                                } else if (code.equals("47")){
                                    dp.ansi.setBG(OMUD.eANSIColors.WHITE);
                                } else if (code.equals("49")){
                                    dp.ansi.resetBG();

                                /*
                                // Colors 256: FG ( 38;5;{ID} )...
                                } else if (code.equals("38")){
                                // Colors 256: BG ( 48;5;{ID} )...
                                } else if (code.equals("48")){
                                */

                                /*
                                // Colors 8: Bright FG (bolded)...
                                } else if (code.equals("90")){
                                } else if (code.equals("91")){
                                } else if (code.equals("92")){
                                } else if (code.equals("93")){
                                } else if (code.equals("94")){
                                } else if (code.equals("95")){
                                } else if (code.equals("96")){
                                } else if (code.equals("97")){
                                // Colors 8: Bright BG (bolded)...
                                } else if (code.equals("100")){
                                } else if (code.equals("101")){
                                } else if (code.equals("102")){
                                } else if (code.equals("103")){
                                } else if (code.equals("104")){
                                } else if (code.equals("105")){
                                } else if (code.equals("106")){
                                } else if (code.equals("107")){
                                */
                                } else {
                                    OMUD_Log.toConsoleDebug("FN_M: " + dp.sbEscSeq + ", " + code);
                                }
                            }

                        // ------------------
                        // Cursor Save/Restore
                        // ------------------
                        // save cursor position...
                        } else if (dp.sbEscSeq.charAt(0) == OMUD.CSI_CRSR_SAVE){
                            // ignore
                        // restore cursor position...
                        } else if (dp.sbEscSeq.charAt(0) == OMUD.CSI_CRSR_REST){
                            // ignore
                        // ------------------
                        // Cursor Up/Down/Right/Left
                        // NOTE: move value is not required in sequence, so default to 1 for if not present.
                        // ------------------
                        // moves cursor up...
                        } else if (esc_seq_last_char == OMUD.CSI_CRSR_UP){
                            int move_val = dp.sbEscSeq.length() == 1 ? 1 : Integer.parseInt(dp.sbEscSeq.substring(0, esc_seq_last_pos));
                            crsrMoveUpDown(dp, arrlBMods, OMUD.CSI_CRSR_UP_STR, 1, move_val, null);
                        // moves cursor down...
                        } else if (esc_seq_last_char == OMUD.CSI_CRSR_DOWN){
                            int move_val = dp.sbEscSeq.length() == 1 ? 1 : Integer.parseInt(dp.sbEscSeq.substring(0, esc_seq_last_pos));
                            crsrMoveUpDown(dp, arrlBMods, OMUD.CSI_CRSR_DOWN_STR, -1, move_val, null);
                        // moves cursor right...
                        } else if (esc_seq_last_char == OMUD.CSI_CRSR_RIGHT){
                            int move_val = dp.sbEscSeq.length() == 1 ? 1 : Integer.parseInt(dp.sbEscSeq.substring(0, esc_seq_last_pos));
                            crsrMoveLeftRight(dp, arrlBMods, OMUD.CSI_CRSR_RIGHT_STR, 1, move_val, null);
                        // moves cursor left...
                        } else if (esc_seq_last_char == OMUD.CSI_CRSR_LEFT){
                            int move_val = dp.sbEscSeq.length() == 1 ? 1 : Integer.parseInt(dp.sbEscSeq.substring(0, esc_seq_last_pos));
                            crsrMoveLeftRight(dp, arrlBMods, OMUD.CSI_CRSR_LEFT_STR, -1, move_val, null);
                            // majormud game string compare needs the escape sequence prefix...
                            dp.appendChar(OMUD.ASCII_ESC);
                            dp.appendChar(OMUD.ASCII_LBR);
                            dp.appendSB(dp.sbEscSeq);
                        // ------------------
                        // Cursor Move to Row+Col
                        // ------------------
                        // move cursor to row and column number...
                        } else if (esc_seq_last_char == OMUD.CSI_CRSR_RWCL1 ||
                                   esc_seq_last_char == OMUD.CSI_CRSR_RWCL2){

                            // strip last char id and split the move values ROW;COL...
                            dp.sbEscSeq.deleteCharAt(esc_seq_last_pos);
                            String[] strVals = dp.sbEscSeq.toString().split(";");
                            if (strVals.length == 2){
                                int dest_row = Integer.parseInt(strVals[0]);
                                int dest_col = Integer.parseInt(strVals[1]);
                                OMUD_Buffer.RowData rdata = dp.omb.getRowData(dest_row);
                                crsrMoveLeftRight(dp, arrlBMods, dp.sbEscSeq.toString(), 0, dest_col, rdata);
                                crsrMoveUpDown(dp,    arrlBMods, dp.sbEscSeq.toString(), 0, dest_row, rdata);
                            } else OMUD_Log.toConsoleError("Error: found bad values for cursor move to row/col: " + dp.sbEscSeq);

                        // ------------------
                        // Erase Functions
                        // ------------------
                        // clear screen: clear terminal and move to top-left...
                        // (clears all after caret and move cursor to start of line)
                        } else if (OMUD.compareSBString(dp.sbEscSeq, OMUD.CSI_CLR_FULL)){
                            int caret_row_start = OMUD.getPrevLF(dp.omb.getText(), dp.omb.getPos()) + 1;
                            arrlBMods.add(new OMUD_BufferModRemove(OMUD.CSI_CLR_FULL, dp.omb, caret_row_start, dp.omb.getText().length() - caret_row_start, caret_row_start, false));
                            // SPECIAL: help manage the mud parser buffer by clearing it out on screen clears.
                            // The mud parser has no way of knowing if data is still incoming for commands.
                            dp.clearTelnetData();
                        // delete from cursor to end of line (don't move cursor)...
                        } else if (OMUD.compareSBString(dp.sbEscSeq, OMUD.CSI_CLR_LINE_CRSR1) ||
                                   OMUD.compareSBString(dp.sbEscSeq, OMUD.CSI_CLR_LINE_CRSR2)){

                            // majormud game string compare needs the escape sequence prefix -
                            // (majormud only ever shows CSI_CLR_LINE_CRSR1 but assume it should be fine)...
                            dp.appendChar(OMUD.ASCII_ESC);
                            dp.appendChar(OMUD.ASCII_LBR);
                            dp.appendSB(dp.sbEscSeq);

                            int caret_row_start =   OMUD.getPrevLF(dp.omb.getText(), dp.omb.getPos()) + 1;
                            int caret_row_end =     OMUD.getNextLF(dp.omb.getText(), dp.omb.getPos()) - 1;
                            if (caret_row_end < 0)
                                caret_row_end =     dp.omb.getText().length();
                            int clear_len =         caret_row_end - dp.omb.getPos();
                            int row_len =           caret_row_end - caret_row_start;
                            int fill_len =          (OMUD.TERMINAL_COLS - row_len) + clear_len;

                            if (clear_len > 0)
                                arrlBMods.add(new OMUD_BufferModRemove(dp.sbEscSeq.toString(), dp.omb, dp.omb.getPos(), clear_len, dp.omb.getPos(), false));
                            if (fill_len > 0){
                                dp.ansi.getText().append(OMUD.getFillString(" ", fill_len));
                                arrlBMods.add(new OMUD_BufferModInsert(dp.sbEscSeq.toString(), dp.omb, dp.ansi, dp.omb.getPos(), dp.omb.getPos()));
                                dp.ansi.getText().setLength(0);
                            }

                        /*  ---- NOTE: UNTESTED! Uncomment when it's found somewhere in a game or on the BBS.
                        //  clear line (don't move cursor)...
                        } else if (OMUD.compareSBString(dp.sbEscSeq, OMUD.CSI_CLR_LINE_FULL)){
                            int caret_row_start =   OMUD.getPrevLF(dp.omb.getText(), dp.omb.getPos()) + 1;
                            int caret_row_end =     OMUD.getNextLF(dp.omb.getText(), dp.omb.getPos()) - 1;
                            if (caret_row_end < 0)
                                caret_row_end =     dp.omb.getText().length();
                            int clear_len =         caret_row_end - caret_row_start;

                            // remove entire line and fill with spaces...
                            if (clear_len > 0) {
                                arrlBMods.add(new OMUD_BufferModRemove(OMUD.CSI_CLR_LINE_FULL, dp.omb, caret_row_start, clear_len, -1, false));
                                dp.ansi.getText().append(OMUD.getFillString(" ", caret_row_end - dp.omb.getPos()));
                                arrlBMods.add(new OMUD_BufferModInsert(OMUD.CSI_CLR_LINE_FULL, dp.omb, dp.ansi, caret_row_start, -1));
                                dp.ansi.getText().setLength(0);
                            }*/

                        // ------------------
                        // Screen Functions
                        // ------------------
                        // ------------------
                        // Non-Standard Functions
                        // ------------------
                        } else {
                            OMUD_Log.toConsoleDebug("FN: " + dp.sbEscSeq);
                        }
                        dp.sbEscSeq.setLength(0);
                    }
                } else {
                    regular_char = true;
                }
            // extended ascii chars...
            } else {
                regular_char = true;
            }

            if (regular_char){
                regular_char = false;
                dp.ansi.getText().append(c);
                dp.appendChar(c);
            }
        }

        // add any remaining text...
        addANSIMods(dp, arrlBMods);

        // ------------------
        // GUI / Terminal Render
        // ------------------
        // add a remove mod if the buffer had data deleted from limit adjustments...
        if (arrlBMods.size() > 0){
            int delete_end = dp.omb.checkLimits();
            if (delete_end > 0)
                arrlBMods.add(new OMUD_BufferModRemove("BUFFER_LIMITS", dp.omb, 0, delete_end, dp.omb.getPos() - delete_end, true));

            // notify to render buffer mods...
            omcd.lockGUI();
                omte.notifyTelnetEvent(new OMUD_TelnetEvent_DataRender(dp.omb, arrlBMods));
            omcd.waitGUI("DataRender");
            omcd.unlockGUI();

            // update debug terminal rows after mods were processed...
            dp.omb.getDebugTerminal().updateRows();

            // notify rendering complete...
            omcd.lockGUI();
                omte.notifyTelnetEvent(new OMUD_TelnetEvent_DataReady(dp.omb));
            omcd.waitGUI("DataReady");
            omcd.unlockGUI();
        }
    }
}
