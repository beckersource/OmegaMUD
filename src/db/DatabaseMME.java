import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;

public class DatabaseMME {
    Connection  _con =  null;
    Statement   _sm =   null;
    ResultSet   _rs =   null;

    public class Result{
        Result(){}
    }

    public DatabaseMME(){
        open();
        queryClasses();
        close();
    }

    public void open(){
        try{
            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
        } catch(ClassNotFoundException cnfex) {
            OMUD_Log.toConsoleError("Error loading or registering MS Access JDBC driver!");
            cnfex.printStackTrace();
        }

        try{
            String strDB =  "db/bearsbbs_mudrev.mdb";
            String strURL = "jdbc:ucanaccess://" + strDB;
            _con = DriverManager.getConnection(strURL);
        } catch(SQLException sqlex){
            OMUD_Log.toConsoleError("Error connected to MME database!");
            sqlex.printStackTrace();
        }

    }

    public void close(){
        try {
            if(_con != null)
                _con.close();
        } catch (SQLException sqlex) {
            OMUD_Log.toConsoleError("Error closing MME database!");
            sqlex.printStackTrace();
        }
    }

    public Result queryClasses(){
        Result result = new Result();

        try{
            _sm = _con.createStatement();
            _rs = _sm.executeQuery("SELECT * FROM CLASSES");

            //StringBuilder sb = new StringBuilder();
            //while(_rs.next())
            //    sb.append(_rs.getInt(1) + "\t" + _rs.getString(2) + "\t" + _rs.getString(3) + "\t" + _rs.getString(4) + "\n");
            //OMUD_Log.toConsoleDebug(sb.toString());

            _sm.close();
            _rs.close();
        } catch(SQLException sqlex){
            OMUD_Log.toConsoleError("Error querying MME database!");
            sqlex.printStackTrace();
        }

        return result;
    }
}
