import java.util.Timer;
import java.util.TimerTask;

public class OMUD_TimerTimeout{

    // -----------------
    // TTTickCallback
    // -----------------
    public interface TTTickCallback{
        public abstract void callbackTimerTimeoutTick();
    }

    // -----------------
    // TTData
    // -----------------
    public static class TTData{
        public int      seconds =       0;
        public int      seconds_max =   0;
        public boolean  ticked  =       false;
        public TTData(int smax){
            seconds_max = smax;
        }
        public TTData(TTData ttd){
            seconds =       ttd.seconds;
            seconds_max =   ttd.seconds_max;
            ticked =        ttd.ticked;
        }
    }

    // -----------------
    // TTTask
    // -----------------
    private class TTTask extends TimerTask{
        private TTData          _data = null;
        private TTTickCallback  _tcb  = null;
        public TTTask(TTData data, TTTickCallback tcb){
            _data = data;
            _tcb  = tcb;
        }

        public void run(){
            _data.seconds++;
            _data.ticked = true;
            _tcb.callbackTimerTimeoutTick();
        }
    }

    // -----------------
    // OMUD_TimerTimeout
    // -----------------
    private Timer           _tmr  = null;
    private TTTickCallback  _tcb  = null;
    private TTData          _data = null;
    private static final int TIMER_TICK_MS = 1000;
    public OMUD_TimerTimeout(TTTickCallback tcb, int seconds_max){
        _tcb  = tcb;
        _data = new TTData(seconds_max);
    }

    public void restart(){
        stop();
        _tmr = new Timer();
        _tmr.scheduleAtFixedRate(new TTTask(_data, _tcb), 0, TIMER_TICK_MS);
    }

    public void stop(){
        if (_tmr != null){
            _tmr.cancel();
            _tmr.purge();
            _tmr = null;
        }
        _data.seconds = 0;
        _data.ticked  = false;
    }

    public final TTData getData()   {return _data;}
    public void resetTicked()       {_data.ticked = false;}
    public boolean isAtMax()        {return _data.seconds >= _data.seconds_max;}
    public boolean isTicked()       {return _data.ticked;}
    public boolean isRunning()      {return _tmr != null;}
}
