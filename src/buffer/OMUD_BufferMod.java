import java.util.ArrayList;
import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;

public abstract class OMUD_BufferMod{
    public static enum eModType{
        INSERT,
        MOVE,
        REMOVE
    }

    private eModType _type;
    protected String _strDbgFnc = "";
    public OMUD_BufferMod(eModType type){
        _type = type;
    }

    public final eModType getType(){return _type;}
    public abstract void render(JTextPane tp, StyledDocument docSwap, ArrayList<OMUD_GUIBlinkText> arrlBlink);

    public final String getDebugString(){return _strDbgFnc;}
}
