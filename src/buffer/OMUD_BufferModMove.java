import java.util.ArrayList;
import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;

public class OMUD_BufferModMove extends OMUD_BufferMod{
    private int _pos_caret_new = 0;

    public OMUD_BufferModMove(String strDbgFnc, OMUD_Buffer omb, int c){
        super(OMUD_BufferMod.eModType.MOVE);

        _strDbgFnc = strDbgFnc;
        _pos_caret_new = c;
        omb.setPos(_pos_caret_new);
    }

    public void render(JTextPane tp, StyledDocument docSwap, ArrayList<OMUD_GUIBlinkText> arrlBlink){
        try{
            tp.setCaretPosition(_pos_caret_new);
        } catch (Exception e){
            OMUD_Log.toConsoleError("Error on buffer move mod render: " + _strDbgFnc + ": " + e.getMessage());
        }
    }

    public final int getNewCaretPos(){return _pos_caret_new;}
}
