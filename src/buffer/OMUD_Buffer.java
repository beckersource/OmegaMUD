public class OMUD_Buffer{
    // ------------------
    // Debug Terminal
    // ------------------
    public class DebugTerminal{
        private class Row {
            public int pos_start =  0;
            public int pos_end =    0;
            public Row(){}
            public Row(Row row){
                pos_start = row.pos_start;
                pos_end =   row.pos_end;
            }
        }

        private Row[]   _rows =             null;
        private int     _row_num =          0;
        private int     _col_num =          0;
        private int     _filled_rows =      1;
        private boolean _update_rows_edit = false;
        private boolean _update_rows_move = false;

        public DebugTerminal(){
            _rows = new Row[OMUD.TERMINAL_ROWS];
            for (int i = 0; i < OMUD.TERMINAL_ROWS; ++i)
                _rows[i] = new Row();
        }

        public void addFilledRow(){
            if (_filled_rows < OMUD.TERMINAL_ROWS)
                _filled_rows++;
        }

        public String getRowText(int row_num){
            if (row_num >= 0 && row_num < _filled_rows && _rows[row_num].pos_end > _rows[row_num].pos_start)
                 return _sbText.substring(_rows[row_num].pos_start, _rows[row_num].pos_end + 1);
            else return "";
        }

        public void updateRows(){
            int row_start = 0;
            int row_end =   _sbText.length() - 1;
            int final_row = _filled_rows - 1;

            // go reverse and update the row data (and cursor position if requested)...
            for (int i = final_row; i >= 0 && (_update_rows_edit || _update_rows_move); --i){

                // try to find earliest matching row start/end vs buffer positions...
                if (_update_rows_edit){
                    row_start = OMUD.getPrevLF(_sbText, row_end) + 1;
                    if (_rows[i].pos_start == row_start && _rows[i].pos_end == row_end){
                        _update_rows_edit = false;
                    } else {
                        _rows[i].pos_start = row_start;
                        _rows[i].pos_end =   row_end;

                        row_end = row_start - 2; // skip backwards (through the LF) to the prev row end
                        if (row_end < 0)
                            row_end = 0;
                    }

                }

                // check if the cursor is here...
                if (_update_rows_move){
                    if (_pos >= _rows[i].pos_start && _pos <= _rows[i].pos_end + 1){ // +1 for at the end of the buffer
                        _row_num = i;
                        _col_num = _pos - _rows[i].pos_start;
                        _update_rows_move = false;
                    }
                }
            }

            _update_rows_edit = false;
            _update_rows_move = false;
        }

        public int getRowNum(){return _row_num;}
        public int getColNum(){return _col_num;}
        public void setUpdateRowEdit(){_update_rows_edit = true;}
        public void setUpdateRowMove(){_update_rows_move = true;}
    }

    // ------------------
    // Main Buffer
    // ------------------
    public class RowData{
        public int pos_start =  0;
        public int pos_end =    0;
        public RowData(int new_row){
            int dist = 0;
            boolean dir_up = false;
            pos_start = _pos;
            pos_end =   _pos;

            if ((dir_up = new_row < _crsr_row))
                 dist = _crsr_row - new_row;
            else dist = new_row   - _crsr_row;

            if (dist > 0){
                for (; dist > 0; --dist){
                    if (dir_up)
                         pos_end =    OMUD.getPrevLF(_sbText, pos_end)   - 1;
                    else pos_start =  OMUD.getNextLF(_sbText, pos_start) + 1;
                }

                if (dir_up){
                    pos_start =   OMUD.getPrevLF(_sbText, pos_end)   + 1; // +1 covers -1 result
                } else {
                    pos_end =     OMUD.getNextLF(_sbText, pos_start) - 1;
                    if (pos_end < 0)
                        pos_end = _sbText.length();
                }
            } else {
                pos_start = OMUD.getPrevLF(_sbText, pos_end)   + 1;
                pos_end =   OMUD.getNextLF(_sbText, pos_start) - 1;
                if (pos_end < 0)
                    pos_end = _sbText.length();
            }
        }
    }

    private int             _pos =          0;
    private int             _pos_top_left = 0;
    private int             _crsr_row =     1; // 1-based count
    private int             _line_count =   1; //
    private int             _filled_rows =  1; //
    private StringBuilder   _sbText =       null;
    private DebugTerminal   _dbgterm =      null;
    public static final int MAX_LINES = 150;

    public OMUD_Buffer(){
        _sbText = new StringBuilder();
        resetRows();
    }

    private void resetRows(){
        _crsr_row =     1;
        _filled_rows =  1;
        _dbgterm = new DebugTerminal();
    }

    public void insertText(int insert_offset, StringBuilder sbText, boolean ends_with_lf){
        _sbText.insert(insert_offset, sbText);
        _dbgterm.setUpdateRowEdit();

        if (ends_with_lf){
            _line_count++;
            _dbgterm.addFilledRow();

            if (_crsr_row < OMUD.TERMINAL_ROWS)
                _crsr_row++;

            // update stuff based on number of rows rows...
            if (_filled_rows < OMUD.TERMINAL_ROWS)
                _filled_rows++;
            // terminal rows filled, move the top-left position...
            else _pos_top_left = OMUD.getNextLF(_sbText, _pos_top_left) + 1; // skip to next row char after LF

            // append to log on LF: strip trailing spaces from lines...
            //OMUD_Log.toFile(OMUD_Log.eLogTypes.TELNET_CLEAN,
            //    _sbText.substring(OMUD.getPrevLF(_sbText, insert_offset - 1) + 1, insert_offset + sbText.length()).replaceAll(" +$", ""));
        }
    }

    public void deleteText(int delete_start, int delete_end, boolean clear_screen){
        if (delete_start < delete_end){
            _sbText.delete(delete_start, delete_end);
            _dbgterm.setUpdateRowEdit();
        }

        // if clearing the screen, update the top-left pos and reset the rows...
        if (clear_screen){
            _pos_top_left = delete_start;
            resetRows();
        }
    }

    // checkLimits(): remove buffer lines from the beginning/top if at max
    public int checkLimits(){
        int delete_end = 0;
        if (_line_count > MAX_LINES){
            for (int i = _line_count - MAX_LINES; i > 0; --i)
                delete_end = OMUD.getNextLF(_sbText, delete_end) + 1; // +1 to include LF, end is exclusive
            // if need to limit: update line count and top-left position...
            if (delete_end > 0){
                _line_count = MAX_LINES;
                _pos_top_left -= delete_end;
            }
        }
        return delete_end;
    }

    public void setPos(int pos_new){
        if (_pos != pos_new){
            _pos  = pos_new;
            _dbgterm.setUpdateRowMove();
        }
    }

    public StringBuilder getText()  {return _sbText;}
    public int getPos()             {return _pos;}
    public int getTopLeftPos()      {return _pos_top_left;}
    public int getLineCount()       {return _line_count;}
    public int getCrsrRowNum()      {return _crsr_row;}
    public void setCrsrRowNum(int row)          {_crsr_row = row;}
    public void setLineCount(int line_count)    {_line_count = line_count;}
    public void setTopLeftPos(int pos_top_left) {_pos_top_left = pos_top_left;}
    public RowData getRowData(int new_row)      {return new RowData(new_row);}
    public DebugTerminal getDebugTerminal()     {return _dbgterm;}
}
