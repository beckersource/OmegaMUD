import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.io.InputStream;
import java.util.ArrayList;

public class OMUD{
    // ------------------
    // Font
    // ------------------
    private static Font _s_font = null;
    public static Font getTerminalFont(){
        if (_s_font == null){
            try{
                InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream("fonts/dos437.ttf");
                _s_font = Font.createFont(Font.TRUETYPE_FONT, is).deriveFont(Font.PLAIN, 8);
                GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(_s_font);
            } catch (Exception e){
                OMUD_Log.toConsoleError("Error creating font: " + e.getMessage());
            }
        }
        return _s_font;
    }

    // ------------------
    // BBS Location
    // ------------------
    public static enum eBBSLoc{
        OFFLINE,
        BBS,
        MUD_MENU,
        MUD_EDITOR,
        MUD
    }
    public static final String[] BBSLOC_STRINGS = {
        "Offline",
        "BBS",
        "MUD Menu",
        "MUD Editor",
        "MUD"
    };
    public static boolean isInsideMUD(eBBSLoc eLoc){return eLoc == eBBSLoc.MUD || eLoc == eBBSLoc.MUD_EDITOR;}

    // --------------
    // Terminal Stuff
    // --------------
    public static final int TERMINAL_ROWS = 25;
    public static final int TERMINAL_COLS = 80;

    // --------------
    // ANSI Colors/Styles
    // --------------
    public static enum eANSIColors{
        BLACK,
        RED,
        GREEN,
        YELLOW,
        BLUE,
        MAGENTA,
        CYAN,
        WHITE
    }

    public static final Color[] ANSI_COLORS_BOLD = {
        new Color( 85,  85,  85),
        new Color(240,   0,   0),
        new Color(  0, 240,   0),
        new Color(240, 240,   0),
        new Color(  0,   0, 240),
        new Color(240,   0, 240),
        new Color(  0, 240, 240),
        new Color(240, 240, 240)
    };
    public static final Color[] ANSI_COLORS_NORM = {
        new Color(  0,   0,   0),
        new Color(170,   0,   0),
        new Color(  0, 170,   0),
        new Color(170,  85,   0),
        new Color(  0,   0, 170),
        new Color(170,   0, 170),
        new Color(  0, 170, 170),
        new Color(170, 170, 170)
    };
    public static final Color[] ANSI_COLORS_DIM = {
        new Color( 42,  42,  42), // dim untested on both fg/bg for accuracy - leaving for visibility when found
        new Color( 85,   0,   0),
        new Color(  0,  85,   0),
        new Color( 85,  85,   0),
        new Color(  0,   0,  85),
        new Color( 85,   0,  85),
        new Color(  0,  85,  85),
        new Color( 85,  85,  85)
    };
    public static final eANSIColors ANSI_DEFAULT_FG = eANSIColors.WHITE;
    public static final eANSIColors ANSI_DEFAULT_BG = eANSIColors.BLACK;
    public static final Color CLR_FG_WHITE =        new Color(232, 232, 232);
    public static final Color CLR_BG_BLUE =         new Color(  0,   0, 164);
    public static final Color CLR_BG_DARK_BUTTON =  new Color( 84,  84,  84);
    public static final Color CLR_BG_DARK_COMBO =   new Color( 56,  84,  84);
    public static final Color CLR_BG_DARK_TABLE =   new Color( 84,  84,  84);
    public static final Color CLR_BG_DARK_MENU =    new Color( 84,  84,  84);
    public static final Color CLR_BG_TOGGLE_ON =    new Color( 36,  36,  64);
    public static final Color CLR_BG_TOGGLE_OFF =   new Color( 48,  64,  96);
    public static final Color CLR_BG_DARK_GREY =    new Color( 42,  42,  42);
    public static final Color CLR_BG_DARK_GREEN =   new Color( 42,  84,  42);
    public static final Color CLR_BG_DARK_YELLOW =  new Color( 84,  84,  42);
    public static final Color CLR_BG_DARK_RED =     new Color( 84,  42,  42);

    // --------------
    // ASCII Codes (Decimal)
    // --------------
    public static final char ASCII_BEL  = 7;
    public static final char ASCII_BS   = 8;
    public static final char ASCII_HTB  = 9;
    public static final char ASCII_LF   = 10;
    public static final char ASCII_VTB  = 11;
    public static final char ASCII_FF   = 12; // form-feed/new page
    public static final char ASCII_CR   = 13;
    public static final char ASCII_ESC  = 27;
    public static final char ASCII_SPC  = 32;
    public static final char ASCII_EXC  = 33;
    public static final char ASCII_ZRO  = 48;
    public static final char ASCII_AT   = 64;
    public static final char ASCII_LBR  = 91;
    public static final char ASCII_DEL  = 127;
    public static final String ASCII_CR_REGEX = String.valueOf(ASCII_CR);

    // --------------
    // ANSI CSI Escape Sequqnces
    // --------------
    // char versions
    public static final char CSI_GRAPHICS =             'm';
    public static final char CSI_CRSR_SAVE =            's';
    public static final char CSI_CRSR_REST =            'u';
    public static final char CSI_CRSR_UP =              'A';
    public static final char CSI_CRSR_DOWN =            'B';
    public static final char CSI_CRSR_RIGHT =           'C';
    public static final char CSI_CRSR_LEFT =            'D';
    public static final char CSI_CRSR_RWCL1 =           'f';
    public static final char CSI_CRSR_RWCL2 =           'H';
    // string versions for indexOf and other funcs
    public static final String CSI_GRAPHICS_STR =       "m";
    public static final String CSI_CRSR_UP_STR =        "A";
    public static final String CSI_CRSR_DOWN_STR =      "B";
    public static final String CSI_CRSR_RIGHT_STR =     "C";
    public static final String CSI_CRSR_LEFT_STR =      "D";

    public static final String CSI_CLR_FULL =           "2J";
    public static final String CSI_CLR_LINE_FULL =      "2K";
    public static final String CSI_CLR_LINE_CRSR1 =     "K";
    public static final String CSI_CLR_LINE_CRSR2 =     "0K";

    public static final String ANSI_SEND_ARROW_UP =     "\u001B[A";
    public static final String ANSI_SEND_ARROW_DOWN =   "\u001B[B";
    public static final String ANSI_SEND_ARROW_RIGHT =  "\u001B[C";
    public static final String ANSI_SEND_ARROW_LEFT =   "\u001B[D";
    public static final String ANSI_SEND_BACKSPACE =    "\u0008";
    public static final String ANSI_SEND_CONTROL =      "\u001E";
    public static final String ANSI_SEND_ESCAPE =       "\u001B";
    public static final String ANSI_SEND_DELETE =       "\u007F";
    public static final String ANSI_SEND_TAB =          "\u0009";

    // --------------
    // RIPscrip Sequences
    // --------------
    public static final String RIP_QVERSION1 =          "\u001b[!";     // query version style 1
    public static final String RIP_QVERSION2 =          "\u001b[0!";    // query version style 2
    public static final String RIP_OFF =                "\u001b[1!";
    public static final String RIP_ON =                 "\u001b[2!";

    // --------------
    // Get Prev/Next LineFeed Convenience Funs
    // --------------
    public static final int getPrevLF(StringBuilder sb, int pos_offset){return sb.lastIndexOf("\n", pos_offset);}
    public static final int getNextLF(StringBuilder sb, int pos_offset){return sb.indexOf("\n", pos_offset);}
    //public static final int getLFCount(String str){return str.length() - str.replace("\n", "").length();}

    // --------------
    // Find Non-LF
    // --------------
    public static final int getPrevNonLF(StringBuilder sb, int pos_offset){
        if (pos_offset >= sb.length())
            pos_offset  = sb.length() - 1;
        while (pos_offset > -1)
            if (sb.charAt(pos_offset) != ASCII_LF)
                break;
            else pos_offset--;
        return pos_offset;
    }

    // --------------
    // StringBuilder Compares (SB.equals() doesn't exist)
    // --------------
    public static boolean compareSBString(StringBuilder sb, String str_find){
        boolean equals = sb.length() == str_find.length();
        for (int i = 0; i < sb.length() && equals; ++i)
            equals = sb.charAt(i) == str_find.charAt(i);
        return equals;
    }
    public static boolean compareSBSB(StringBuilder sb, StringBuilder sb_find){
        boolean equals = sb.length() == sb_find.length();
        for (int i = 0; i < sb.length() && equals; ++i)
            equals = sb.charAt(i) == sb_find.charAt(i);
        return equals;
    }

    // --------------
    // Misc String Functions
    // --------------
    // getFillString(): convenience func for filling spaces...
    public static String getFillString(String strFill, int fill_size){return (fill_size > 0 ? new String(new char[fill_size]).replace("\0", strFill) : "");}
    public static void copyStringArrayList(ArrayList<String> arrlSrc, ArrayList<String> arrlDest){
        for (int i = 0; i < arrlSrc.size(); ++i)
            arrlDest.add(arrlSrc.get(i));
    }

    // appendStringArrayListUniques(): assumes new array does not have dupes
    public static void appendStringArrayListUniques(ArrayList<String> arrlNew, ArrayList<String> arrlDest){
        int dest_orig_size = arrlDest.size(); // prevent checking newly addd
        for (int i = 0; i < arrlNew.size(); ++i){
            boolean found = false;
            for (int j = 0; j < dest_orig_size && !found; ++j)
                found = arrlNew.get(i).equals(arrlDest.get(j));
            if (!found)
                arrlDest.add(arrlNew.get(i));
        }
    }

    // --------------
    // Check Types
    // --------------
    public static boolean isEnum(Object o)  {return o.getClass().isEnum();}
    public static boolean isBool(Object o)  {return o.getClass().getSimpleName().equals("Boolean");}
    public static boolean isInt(Object o)   {return o.getClass().getSimpleName().equals("Integer");}
    public static boolean isLong(Object o)  {return o.getClass().getSimpleName().equals("Long");}
    public static boolean isString(Object o){return o.getClass().getSimpleName().equals("String");}
}
