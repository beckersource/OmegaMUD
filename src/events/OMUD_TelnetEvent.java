import java.util.Date;
import java.text.SimpleDateFormat;

interface OMUD_ITelnetEvents{
    public void notifyTelnetEvent(OMUD_TelnetEvent event);
}

public abstract class OMUD_TelnetEvent{
    public static enum eEventType{
        DATA_INPUT,
        DATA_RENDER,
        DATA_READY,
        CONNECTED,
        DISCONNECTED
    }

    private static final String[] EVENT_TYPE_STRINGS = {
        "Data Input",
        "Data Render",
        "Data Ready",
        "Connected",
        "Disconnected"
    };

    private eEventType _type;
    public OMUD_TelnetEvent(eEventType type){
        _type = type;

        // NOTE: performance hit here for logging.  don't leave on permanently.
        //OMUD_Log.toFile(OMUD_Log.eLogTypes.EVENTS_TELNET, new SimpleDateFormat("yyyy-MM-dd_kk-mm-ss").format(new Date()) + ": " + EVENT_TYPE_STRINGS[_type.ordinal()] + "\n");
    }

    public final eEventType getType(){return _type;}
}
