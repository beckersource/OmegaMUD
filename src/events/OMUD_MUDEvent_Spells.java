public class OMUD_MUDEvent_Spells extends OMUD_MUDEvent{
    public OMUD_MMUD_DataBlock_Spells dataSpells = null;

    public OMUD_MUDEvent_Spells(final OMUD_MMUD_DataBlock_Spells ds){
        super(OMUD_MUDEvent.eEventType.SPELLS);

        dataSpells = new OMUD_MMUD_DataBlock_Spells(ds);
    }
}
