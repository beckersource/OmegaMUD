public class OMUD_MUDEvent_Who extends OMUD_MUDEvent{
    public OMUD_MMUD_DataBlock_Who dataWho = null;

    public OMUD_MUDEvent_Who(final OMUD_MMUD_DataBlock_Who dw){
        super(OMUD_MUDEvent.eEventType.WHO);

        dataWho = new OMUD_MMUD_DataBlock_Who(dw);
    }
}
