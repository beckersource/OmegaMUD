import java.util.Date;
import java.text.SimpleDateFormat;

interface OMUD_IGUIEvents{
    public void notifyGUIEvent(OMUD_GUIEvent event);
}

public abstract class OMUD_GUIEvent{
    public static enum eEventType{
        RESET_TERMINAL_CARET,
        FOCUS_TELNET_INPUT
    }

    private static final String[] EVENT_TYPE_STRINGS = {
        "Reset Terminal Caret",
        "Focus Telnet Input"
    };

    private eEventType _type;
    public OMUD_GUIEvent(eEventType type){
        _type = type;

        // NOTE: performance hit here for logging.  don't leave on permanently.
        //OMUD_Log.toFile(OMUD_Log.eLogTypes.EVENTS_GUI, new SimpleDateFormat("yyyy-MM-dd_kk-mm-ss").format(new Date()) + ": " + EVENT_TYPE_STRINGS[_type.ordinal()] + "\n");
    }

    public final eEventType getType(){return _type;}
}
