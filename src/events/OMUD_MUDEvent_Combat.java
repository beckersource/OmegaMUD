public class OMUD_MUDEvent_Combat extends OMUD_MUDEvent{
    public OMUD_MMUD_DataBlock_Combat dataCombat = null;

    public OMUD_MUDEvent_Combat(final OMUD_MMUD_DataBlock_Combat dc){
        super(OMUD_MUDEvent.eEventType.COMBAT);

        dataCombat = new OMUD_MMUD_DataBlock_Combat(dc);
    }
}
