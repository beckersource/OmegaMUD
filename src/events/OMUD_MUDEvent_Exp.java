public class OMUD_MUDEvent_Exp extends OMUD_MUDEvent{
    public OMUD_MMUD_DataBlock_Exp dataExp = null;

    public OMUD_MUDEvent_Exp(final OMUD_MMUD_DataBlock_Exp dexp){
        super(OMUD_MUDEvent.eEventType.EXP);

        dataExp = new OMUD_MMUD_DataBlock_Exp(dexp);
    }
}
