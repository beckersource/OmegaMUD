public class OMUD_MUDEvent_Statline extends OMUD_MUDEvent{
    public OMUD_MMUD_DataBlock_Statline dataStatline =  null;
    public OMUD_TimerTimeout.TTData     tmrdStatline =  null;
    public OMUD_TimerTimeout.TTData     tmrdCombat =    null;

    public OMUD_MUDEvent_Statline(
        final OMUD_MMUD_DataBlock_Statline  ds,
        final OMUD_TimerTimeout.TTData      tmrds,
        final OMUD_TimerTimeout.TTData      tmrdc){
        super(OMUD_MUDEvent.eEventType.STATLINE);

        dataStatline =  new OMUD_MMUD_DataBlock_Statline(ds);
        tmrdStatline =  new OMUD_TimerTimeout.TTData(tmrds);
        tmrdCombat =    new OMUD_TimerTimeout.TTData(tmrdc);
    }
}
