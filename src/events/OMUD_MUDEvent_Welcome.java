public class OMUD_MUDEvent_Welcome extends OMUD_MUDEvent{
    public String text = "";

    public OMUD_MUDEvent_Welcome(final String strWelcome){
        super(OMUD_MUDEvent.eEventType.WELCOME);

        text = strWelcome;
    }
}
