public class OMUD_MUDEvent_Top extends OMUD_MUDEvent{
    public OMUD_MMUD_DataBlock_Top dataTop = null;

    public OMUD_MUDEvent_Top(final OMUD_MMUD_DataBlock_Top dt){
        super(OMUD_MUDEvent.eEventType.TOP);

        dataTop = new OMUD_MMUD_DataBlock_Top(dt);
    }
}
