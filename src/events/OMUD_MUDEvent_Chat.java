public class OMUD_MUDEvent_Chat extends OMUD_MUDEvent{
    public OMUD_MMUD_DataBlock_Chat     dataChat =      null;
    public OMUD_MMUD_DataBlock_Statline dataStatline =  null;
    public OMUD_MMUD_DataBlock_Exp      dataExp =       null;
    public OMUD_MMUD_DataBlock_Party    dataParty =     null;

    public OMUD_MUDEvent_Chat(
        final OMUD_MMUD_DataBlock_Chat      dc,
        final OMUD_MMUD_DataBlock_Statline  ds,
        final OMUD_MMUD_DataBlock_Exp       dexp,
        final OMUD_MMUD_DataBlock_Party     dp){
        super(OMUD_MUDEvent.eEventType.CHAT);

        dataChat =      new OMUD_MMUD_DataBlock_Chat(dc);
        dataStatline =  new OMUD_MMUD_DataBlock_Statline(ds);
        dataExp =       new OMUD_MMUD_DataBlock_Exp(dexp);
        dataParty =     new OMUD_MMUD_DataBlock_Party(dp);
    }
}
