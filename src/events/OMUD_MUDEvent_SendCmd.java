public class OMUD_MUDEvent_SendCmd extends OMUD_MUDEvent{
    public String cmd = "";

    public OMUD_MUDEvent_SendCmd(final String strCmd){
        super(OMUD_MUDEvent.eEventType.SEND_CMD);

        cmd = strCmd;
    }
}
