public class OMUD_MUDEvent_BufferSize extends OMUD_MUDEvent{
    public long buffer_size = 0;

    public OMUD_MUDEvent_BufferSize(final long bsize){
        super(OMUD_MUDEvent.eEventType.BUFFER_SIZE);

        buffer_size = bsize;
    }
}
