public class OMUD_MUDEvent_Room extends OMUD_MUDEvent{
    public OMUD_MMUD_DataBlock_Room dataRoom = null;

    public OMUD_MUDEvent_Room(final OMUD_MMUD_DataBlock_Room dr){
        super(OMUD_MUDEvent.eEventType.ROOM);

        dataRoom = new OMUD_MMUD_DataBlock_Room(dr);
    }
}
