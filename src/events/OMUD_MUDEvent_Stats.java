public class OMUD_MUDEvent_Stats extends OMUD_MUDEvent{
    public OMUD_MMUD_DataBlock_Stats    dataStats =     null;
    public OMUD_MMUD_DataBlock_Statline dataStatline =  null;
    public OMUD_MMUD_DataBlock_Exp      dataExp =       null;

    public OMUD_MUDEvent_Stats(
        final OMUD_MMUD_DataBlock_Stats     dstat,
        final OMUD_MMUD_DataBlock_Statline  dsline,
        final OMUD_MMUD_DataBlock_Exp       dexp){
        super(OMUD_MUDEvent.eEventType.STATS);

        dataStats =     new OMUD_MMUD_DataBlock_Stats(dstat);
        dataStatline =  new OMUD_MMUD_DataBlock_Statline(dsline);
        dataExp =       new OMUD_MMUD_DataBlock_Exp(dexp);
    }
}
