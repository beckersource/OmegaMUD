import java.util.ArrayList;

public class OMUD_MUDEvent_DebugText extends OMUD_MUDEvent{
    public ArrayList<String> arrlText = new ArrayList<String>();

    public OMUD_MUDEvent_DebugText(final ArrayList<String> arrl){
        super(OMUD_MUDEvent.eEventType.DEBUG_TEXT);

        for (int i = 0; i < arrl.size(); ++i)
            arrlText.add(arrl.get(i));
    }
}
