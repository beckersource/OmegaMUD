import java.util.Date;
import java.text.SimpleDateFormat;

interface OMUD_IMUDEvents{
    public void notifyMUDEvent(OMUD_MUDEvent event);
}

public abstract class OMUD_MUDEvent{
    public static enum eEventType{
        STATLINE,
        USER_CMD,
        BUFFER_SIZE,
        ROOM,
        COMBAT,
        EXP,
        PARTY,
        CHAT,
        STATS,
        INV,
        SHOP,
        SPELLS,
        WHO,
        TOP,
        WELCOME,
        DEBUG_TEXT,
        BBS_LOC,
        GET_DATABLOCK,
        SEND_CMD
    }

    private static final String[] EVENT_TYPE_STRINGS = {
        "Statline",
        "User Cmd",
        "Buffer Size",
        "Room",
        "Combat",
        "Exp",
        "Party",
        "Chat",
        "Stats",
        "Inv",
        "Shop",
        "Spells",
        "Who",
        "Top",
        "Welcome",
        "Debug Text",
        "BBS Location",
        "Get DataBlock",
        "Send Cmd"
    };

    private eEventType _type;
    public OMUD_MUDEvent(eEventType type){
        _type = type;

        // NOTE: performance hit here for logging.  don't leave on permanently.
        //OMUD_Log.toFile(OMUD_Log.eLogTypes.EVENTS_MUD, new SimpleDateFormat("yyyy-MM-dd_kk-mm-ss").format(new Date()) + ": " + EVENT_TYPE_STRINGS[_type.ordinal()] + "\n");
    }

    public final eEventType getType(){return _type;}
}
