public class OMUD_MUDEvent_Shop extends OMUD_MUDEvent{
    public OMUD_MMUD_DataBlock_Shop dataShop = null;

    public OMUD_MUDEvent_Shop(final OMUD_MMUD_DataBlock_Shop ds){
        super(OMUD_MUDEvent.eEventType.SHOP);

        dataShop = new OMUD_MMUD_DataBlock_Shop(ds);
    }
}
