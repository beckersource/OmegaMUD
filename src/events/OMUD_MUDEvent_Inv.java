public class OMUD_MUDEvent_Inv extends OMUD_MUDEvent{
    public OMUD_MMUD_DataBlock_Inv dataInv = null;

    public OMUD_MUDEvent_Inv(final OMUD_MMUD_DataBlock_Inv di){
        super(OMUD_MUDEvent.eEventType.INV);

        dataInv = new OMUD_MMUD_DataBlock_Inv(di);
    }
}
