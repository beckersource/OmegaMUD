public class OMUD_MUDEvent_BBSLoc extends OMUD_MUDEvent{
    public OMUD.eBBSLoc bbs_loc;

    public OMUD_MUDEvent_BBSLoc(final OMUD.eBBSLoc loc){
        super(OMUD_MUDEvent.eEventType.BBS_LOC);

        bbs_loc = loc;
    }
}
