public class OMUD_MUDEvent_Party extends OMUD_MUDEvent{
    public OMUD_MMUD_DataBlock_Party dataParty = null;

    public OMUD_MUDEvent_Party(final OMUD_MMUD_DataBlock_Party dp){
        super(OMUD_MUDEvent.eEventType.PARTY);

        dataParty = new OMUD_MMUD_DataBlock_Party(dp);
    }
}
