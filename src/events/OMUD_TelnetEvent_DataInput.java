public class OMUD_TelnetEvent_DataInput extends OMUD_TelnetEvent{
    public String text = "";

    public OMUD_TelnetEvent_DataInput(final String input_text){
        super(OMUD_TelnetEvent.eEventType.DATA_INPUT);

        text = input_text;
    }
}
