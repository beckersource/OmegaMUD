import java.util.ArrayList;

public class OMUD_TelnetEvent_DataRender extends OMUD_TelnetEvent{
    public OMUD_Buffer omb = null;
    public ArrayList<OMUD_BufferMod> arrlBMods = null;

    public OMUD_TelnetEvent_DataRender(final OMUD_Buffer buff, final ArrayList<OMUD_BufferMod> mods){
        super(OMUD_TelnetEvent.eEventType.DATA_RENDER);

        omb = buff;
        arrlBMods = mods;
    }
}
