public class OMUD_MUDEvent_UserCmd extends OMUD_MUDEvent{
    public String cmd = "";
    public int remaining = 0;

    public OMUD_MUDEvent_UserCmd(final String strCmd, int rem){
        super(OMUD_MUDEvent.eEventType.USER_CMD);

        cmd = strCmd;
        remaining = rem;
    }
}
