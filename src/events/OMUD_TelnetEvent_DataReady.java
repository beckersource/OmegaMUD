import java.util.ArrayList;

public class OMUD_TelnetEvent_DataReady extends OMUD_TelnetEvent{
    public OMUD_Buffer omb = null;

    public OMUD_TelnetEvent_DataReady(final OMUD_Buffer buff){
        super(OMUD_TelnetEvent.eEventType.DATA_READY);

        omb = buff;
    }
}
