import java.util.ArrayList;

public class OMUD_MUDEvent_GetDataBlock extends OMUD_MUDEvent{
    public ArrayList<OMUD_MMUD_DataBlock.eBlockType> arrlBlockTypes = new ArrayList<OMUD_MMUD_DataBlock.eBlockType>();

    public OMUD_MUDEvent_GetDataBlock(final OMUD_MMUD_DataBlock.eBlockType dt){
        super(OMUD_MUDEvent.eEventType.GET_DATABLOCK);

        arrlBlockTypes.add(dt);
    }

    public OMUD_MUDEvent_GetDataBlock(final ArrayList<OMUD_MMUD_DataBlock.eBlockType> arrl){
        super(OMUD_MUDEvent.eEventType.GET_DATABLOCK);

        for (int i = 0; i < arrl.size(); ++i)
            arrlBlockTypes.add(arrl.get(i));
    }
}
