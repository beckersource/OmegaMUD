import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class OMUD_Char_Data{

    public OMUD.eBBSLoc eBBSLoc = OMUD.eBBSLoc.OFFLINE;
    public OMUD_IMUDEvents  omme =       null;
    private DataParse       _dataParse = null;
    private DataMMUD        _dataMMUD =  null;
    private Semaphore       _semaGUI =   null;

    public OMUD_Char_Data(OMUD_IMUDEvents mmevents, OMUD_TimerTimeout.TTTickCallback tcb){
        omme =       mmevents;
        _dataParse = new DataParse();
        _dataMMUD =  new DataMMUD(tcb);
        _semaGUI =   new Semaphore(1, true);
    }

    public DataParse getParseData() {return _dataParse;}
    public DataMMUD  getMMUDData()  {return _dataMMUD;}
    public void reset(OMUD.eBBSLoc bbs_loc){
        eBBSLoc = bbs_loc;
        _dataParse.reset();
        _dataMMUD.reset();
    }

    // threading...
    public void unlockGUI(){_semaGUI.release();}
    public void lockGUI()  {
        try{ _semaGUI.acquire();
        } catch (Exception e) {OMUD_Log.toConsoleError("Error acquiring semaphore for GUI: " + e.getMessage());}
    }
    public void waitGUI(String strReason){
        try{ while (!_semaGUI.tryAcquire()) Thread.sleep(1);
        } catch (Exception e) {OMUD_Log.toConsoleError("Error waiting for GUI semaphore: " + strReason + ": " + e.getMessage());}
    }

    // main MUD render (GUI update) method...
    public void renderMMUD(){
        omme.notifyMUDEvent(new OMUD_MUDEvent_Statline(_dataMMUD.dataStatline, _dataMMUD.tmrStatline.getData(), _dataMMUD.tmrCombat.getData()));

        // notify: found mud data...
        for (int i = 0; i < _dataParse.arrlFoundTypes.size(); ++i){
                 if (_dataParse.arrlFoundTypes.get(i) == OMUD_MMUD_DataBlock.eBlockType.ROOM)
                omme.notifyMUDEvent(new OMUD_MUDEvent_Room(_dataMMUD.dataRoom));
            else if (_dataParse.arrlFoundTypes.get(i) == OMUD_MMUD_DataBlock.eBlockType.PARTY)
                omme.notifyMUDEvent(new OMUD_MUDEvent_Party(_dataMMUD.dataParty));
            else if (_dataParse.arrlFoundTypes.get(i) == OMUD_MMUD_DataBlock.eBlockType.CHAT){
                omme.notifyMUDEvent(new OMUD_MUDEvent_Chat(_dataMMUD.dataChat, _dataMMUD.dataStatline, _dataMMUD.dataExp, _dataMMUD.dataParty));
                _dataMMUD.dataChat = new OMUD_MMUD_DataBlock_Chat(); // hack reset for now, will change later
            }
            else if (_dataParse.arrlFoundTypes.get(i) == OMUD_MMUD_DataBlock.eBlockType.COMBAT){
                omme.notifyMUDEvent(new OMUD_MUDEvent_Combat(_dataMMUD.dataCombat));
                _dataMMUD.dataCombat = new OMUD_MMUD_DataBlock_Combat(); // hack reset for now, will change later
                // update / show combat xp gains...
                if (_dataMMUD.dataExp.arrlCombatExp.size() > 0){
                    omme.notifyMUDEvent(new OMUD_MUDEvent_Exp(_dataMMUD.dataExp));
                    _dataMMUD.dataExp.arrlCombatExp.clear();
                }
            }
            else if (_dataParse.arrlFoundTypes.get(i) == OMUD_MMUD_DataBlock.eBlockType.INV)
                omme.notifyMUDEvent(new OMUD_MUDEvent_Inv(_dataMMUD.dataInv));
            else if (_dataParse.arrlFoundTypes.get(i) == OMUD_MMUD_DataBlock.eBlockType.STATS ||
                     _dataParse.arrlFoundTypes.get(i) == OMUD_MMUD_DataBlock.eBlockType.EXP)
                omme.notifyMUDEvent(new OMUD_MUDEvent_Stats(_dataMMUD.dataStats, _dataMMUD.dataStatline, _dataMMUD.dataExp));
            else if (_dataParse.arrlFoundTypes.get(i) == OMUD_MMUD_DataBlock.eBlockType.SHOP)
                omme.notifyMUDEvent(new OMUD_MUDEvent_Shop(_dataMMUD.dataShop));
            else if (_dataParse.arrlFoundTypes.get(i) == OMUD_MMUD_DataBlock.eBlockType.SPELLS)
                omme.notifyMUDEvent(new OMUD_MUDEvent_Spells(_dataMMUD.dataSpells));
            else if (_dataParse.arrlFoundTypes.get(i) == OMUD_MMUD_DataBlock.eBlockType.WHO)
                omme.notifyMUDEvent(new OMUD_MUDEvent_Who(_dataMMUD.dataWho));
            else if (_dataParse.arrlFoundTypes.get(i) == OMUD_MMUD_DataBlock.eBlockType.TOP)
                omme.notifyMUDEvent(new OMUD_MUDEvent_Top(_dataMMUD.dataTop));
        }

        // notify: get/request data...
        if (_dataParse.arrlGetTypes.size() > 0)
            omme.notifyMUDEvent(new OMUD_MUDEvent_GetDataBlock(_dataParse.arrlGetTypes));

        // notify: debug data...
        if (_dataParse.arrlDebugText.size() > 0){
            omme.notifyMUDEvent(new OMUD_MUDEvent_DebugText(_dataParse.arrlDebugText));
            for (int i = 0; i < _dataParse.arrlDebugText.size(); ++i)
                OMUD_Log.toFile(OMUD_Log.eLogTypes.MUD_DEBUG, _dataParse.arrlDebugText.get(i));
        }

        // reset...
        _dataParse.arrlFoundTypes.clear();
        _dataParse.arrlGetTypes.clear();
        _dataParse.arrlDebugText.clear();

        // notify buffer changes...
        omme.notifyMUDEvent(new OMUD_MUDEvent_BufferSize(_dataParse.sbBuffer.length()));
    }

    // --------------
    // DataParse
    // --------------
    public class DataParse{
        // telnet stuff...
        public  OMUD_Buffer     omb =               null;
        public  OMUD_ANSI       ansi =              null;
        public  StringBuilder   sbEscSeq =          null;
        public  StringBuilder   sbNewData =         null;
        // block stuff...
        public StringBuilder    sbBuffer =          null;
        public StringBuilder    sbBlockDataLeft =   null;
        public StringBuilder    sbBlockDataRight =  null;
        public ArrayList<OMUD_MMUD_DataBlock.eBlockType> arrlFoundTypes =   null;
        public ArrayList<OMUD_MMUD_DataBlock.eBlockType> arrlGetTypes  =    null;
        public ArrayList<String>                         arrlDebugText =    null;
        private OMUD_MMUD_ParseBlocks.CmdBlock           _blkActiveCmd =    null;

        public DataParse(){
            // block stuff...
            sbBuffer =          new StringBuilder();
            sbBlockDataLeft =   new StringBuilder();
            sbBlockDataRight =  new StringBuilder();
            arrlFoundTypes =    new ArrayList<OMUD_MMUD_DataBlock.eBlockType>();
            arrlGetTypes  =     new ArrayList<OMUD_MMUD_DataBlock.eBlockType>();
            arrlDebugText =     new ArrayList<String>();
            // telnet stuff...
            omb =       new OMUD_Buffer();
            ansi =      new OMUD_ANSI();
            sbEscSeq =  new StringBuilder();
            sbNewData = new StringBuilder();
        }

        // telnet stuff...
        public void reset(){
            ansi = new OMUD_ANSI();
            sbEscSeq.setLength(0);
            sbBuffer.setLength(0);
        }

        // mud stuff...
        public void appendChar(char c)          {sbBuffer.append(c);}
        public void appendSB(StringBuilder sb)  {sbBuffer.append(sb);}
        public void deleteLastChar(){
            if (sbBuffer.length() > 0)
                sbBuffer.deleteCharAt(sbBuffer.length() - 1);
        }
        public void clearTelnetData(){
            if (eBBSLoc == OMUD.eBBSLoc.MUD_EDITOR || eBBSLoc == OMUD.eBBSLoc.BBS)
                sbBuffer.setLength(0);
        }

        // block stuff...
        public void clearActiveCmdBlock(){_blkActiveCmd = null;}
        public void setActiveCmdBlock(OMUD_MMUD_ParseBlocks.CmdBlock cb){_blkActiveCmd = cb;}
        public boolean hasActiveCmdBlock(){return _blkActiveCmd != null;}
        public OMUD_MMUD_ParseBlocks.CmdBlock getActiveCmdBlock(){return _blkActiveCmd;}
    }

    // --------------
    // DataMMUD
    // --------------
    public class DataMMUD{
        public String                       strWelcome =    "";
        public boolean                      got_statline =  false;
        public boolean                      is_kai =        false;
        public OMUD_MMUD_DataBlock_Room     dataRoom =      null;
        public OMUD_MMUD_DataBlock_Exp      dataExp =       null;
        public OMUD_MMUD_DataBlock_Stats    dataStats =     null;
        public OMUD_MMUD_DataBlock_Statline dataStatline =  null;
        public OMUD_MMUD_DataBlock_Inv      dataInv =       null;
        public OMUD_MMUD_DataBlock_Shop     dataShop =      null;
        public OMUD_MMUD_DataBlock_Spells   dataSpells =    null;
        public OMUD_MMUD_DataBlock_Who      dataWho =       null;
        public OMUD_MMUD_DataBlock_Party    dataParty =     null;
        public OMUD_MMUD_DataBlock_Combat   dataCombat =    null;
        public OMUD_MMUD_DataBlock_Chat     dataChat =      null;
        public OMUD_MMUD_DataBlock_Top      dataTop =       null;
        public OMUD_TimerTimeout            tmrStatline =   null;
        public OMUD_TimerTimeout            tmrCombat =     null;
        private static final int STATLINE_MAX_SECS = 4; // max seconds (-1) allowed to wait for the appearch of the statline after a command
        private static final int COMBAT_MAX_SECS =   7; // max seconds (-1) for assuming next round has combat if was still in combat

        public DataMMUD(OMUD_TimerTimeout.TTTickCallback tcb){
            tmrStatline =   new OMUD_TimerTimeout(tcb, STATLINE_MAX_SECS);
            tmrCombat =     new OMUD_TimerTimeout(tcb, COMBAT_MAX_SECS);
        }

        public void reset(){
            strWelcome =    "";
            got_statline =  false;
            is_kai =        false;
            dataRoom =      new OMUD_MMUD_DataBlock_Room();
            dataExp =       new OMUD_MMUD_DataBlock_Exp();
            dataStats =     new OMUD_MMUD_DataBlock_Stats();
            dataStatline =  new OMUD_MMUD_DataBlock_Statline();
            dataInv =       new OMUD_MMUD_DataBlock_Inv();
            dataShop =      new OMUD_MMUD_DataBlock_Shop();
            dataSpells =    new OMUD_MMUD_DataBlock_Spells();
            dataWho =       new OMUD_MMUD_DataBlock_Who();
            dataParty =     new OMUD_MMUD_DataBlock_Party();
            dataCombat =    new OMUD_MMUD_DataBlock_Combat();
            dataChat =      new OMUD_MMUD_DataBlock_Chat();
            dataTop =       new OMUD_MMUD_DataBlock_Top();
            tmrStatline.stop();
            tmrCombat.stop();
        }
    }
}
