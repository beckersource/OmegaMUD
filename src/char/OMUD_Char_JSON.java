// https://json2csharp.com/code-converters/json-to-pojo

import java.io.RandomAccessFile;
import java.io.ByteArrayOutputStream;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class OMUD_Char_JSON{
    // ---------------------
    // General Settings
    // ---------------------
    private class General{
        public String file_name  = "";
        public String realm_name = "";
        public General(){}
    }

    // ---------------------
    // GUI Settings
    // ---------------------
    private class GUI{
        public int     startup_view = 0;
        public boolean min_to_tray = true;
        public GUI(){}
    }

    // ---------------------
    // Party Settings
    // ---------------------
    private class Party{
        public boolean show_party_frame = true;
        public int par_refresh_ms = 15000;
        public Party(){}
    }

    // ---------------------
    // Chat Settings
    // ---------------------
    private class Chat{
        public int dummy = 0;
        public Chat(){}
    }

    // ---------------------
    // ScriptEvents Settings
    // ---------------------
    private class ScriptEvents{
        public int dummy = 0;
        public ScriptEvents(){}
    }

    public General      general =       new General();
    public GUI          gui =           new GUI();
    public Party        party =         new Party();
    public Chat         chat =          new Chat();
    public ScriptEvents scriptEvents =  new ScriptEvents();
    public OMUD_Char_JSON(){}

    // ---------------------
    // createFromFile(): create settings JSON from file
    // ---------------------
    public static OMUD_Char_JSON createFromFile(String fname){
        OMUD_Char_JSON      json =      null;
        RandomAccessFile    file =      null;
        ObjectMapper        objMapper = null;
        byte[]              bytes  =    null;

        // open/read json file...
        if (fname.length() > 0){
            try{
                file = new RandomAccessFile(fname, "r");
                bytes = new byte[(int) file.length()];
                file.readFully(bytes);
            } catch (Exception e){
                OMUD_Log.toConsoleError("Error reading character json file: " + fname + ": " + e.getMessage());
            }
        }

        // store json data into objecct...
        if (bytes != null && bytes.length > 0){
            try{
                objMapper = new ObjectMapper();
                json = objMapper.readValue(bytes, OMUD_Char_JSON.class);
            } catch (Exception e){
                OMUD_Log.toConsoleError("Error mapping character json from file data: " + fname + ": " + e.getMessage());
            }
        }

        // output json object as string...
        objMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try{
            objMapper.writeValue(baos, json);
            //OMUD_Log.toConsoleDebug("OMUD_Char_JSON.createFromFile:\n" + baos);
        } catch (Exception e){
            OMUD_Log.toConsoleError("Error writing mapped json data to string: " + fname + ": " + e.getMessage());
        }

        return json;
    }
}
