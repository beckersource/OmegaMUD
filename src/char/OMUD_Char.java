import java.util.ArrayList;
import javax.swing.SwingUtilities;

public class OMUD_Char implements OMUD_ITelnetEvents, OMUD_IMUDEvents, OMUD_IGUIEvents, OMUD_TimerTimeout.TTTickCallback{
    private OMUD_Telnet         _omt =      null;
    private OMUD_Char_Data      _data =     null;
    private OMUD_GUIFCharView   _fView =    null;
    private OMUD_GUIFCharInfo   _fInfo =    null;
    private OMUD_GUIFCharInv    _fInv =     null;
    private DatabaseMME         _dbMME =    null;
    private OMUD_Char_JSON      _json =     null;

    public OMUD_Char(){
        OMUD_Log.resetFiles();

        _json =  OMUD_Char_JSON.createFromFile("json/Syntax_MudInfo.net.json");
        _data =  new OMUD_Char_Data(this, this);
        _dbMME = new DatabaseMME();

        try{
            _omt = new OMUD_Telnet(this, _data);
        } catch (Exception e) {
            OMUD_Log.toConsoleError("Error creating core OmegaMUD objects:" + e.getMessage());
        }

        _fView = new OMUD_GUIFCharView(this, this, this, _omt);
        _fInfo = new OMUD_GUIFCharInfo(this);
        _fInv  = new OMUD_GUIFCharInv(this);
    }

    public OMUD_GUIFCharView  getViewFrame(){return _fView;}
    public OMUD_GUIFCharInfo  getInfoFrame(){return _fInfo;}
    public OMUD_GUIFCharInv   getInvFrame() {return _fInv;}

    // --------------
    // Telnet Events
    // --------------
    public void notifyTelnetEvent(final OMUD_TelnetEvent event){
        SwingUtilities.invokeLater(new Runnable(){public void run(){
                   if (event.getType() == OMUD_TelnetEvent.eEventType.DATA_INPUT){
                _omt.sendText(((OMUD_TelnetEvent_DataInput) event).text);
            } else if (event.getType() == OMUD_TelnetEvent.eEventType.DATA_RENDER){
                _fView.eventTelnet_DataRender((OMUD_TelnetEvent_DataRender) event);
                _data.unlockGUI();
            } else if (event.getType() == OMUD_TelnetEvent.eEventType.DATA_READY){
                _fInfo.eventTelnet_DataReady((OMUD_TelnetEvent_DataReady) event);
                _data.unlockGUI();
            } else if (event.getType() == OMUD_TelnetEvent.eEventType.CONNECTED){
                _fView.eventTelnet_Connected((OMUD_TelnetEvent_Connected) event);
            } else if (event.getType() == OMUD_TelnetEvent.eEventType.DISCONNECTED){
                _fView.eventTelnet_Disconnected((OMUD_TelnetEvent_Disconnected) event);
            }
        }});
    }

    // --------------
    // MUD Events
    // --------------
    public void notifyMUDEvent(final OMUD_MUDEvent event){
        SwingUtilities.invokeLater(new Runnable(){public void run(){
                   if (event.getType() == OMUD_MUDEvent.eEventType.STATLINE){
                _fView.eventMUD_Statline((OMUD_MUDEvent_Statline) event);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.USER_CMD){
                OMUD_MUDEvent_UserCmd ev = (OMUD_MUDEvent_UserCmd) event;
                _fView.eventMUD_UserCmd(ev);
                _fInfo.eventMUD_UserCmd(ev);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.ROOM){
                OMUD_MUDEvent_Room ev = (OMUD_MUDEvent_Room) event;
                _fView.eventMUD_Room(ev);
                _fInfo.eventMUD_Room(ev);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.COMBAT){
                _fInfo.eventMUD_Combat((OMUD_MUDEvent_Combat) event);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.EXP){
                OMUD_MUDEvent_Exp ev = (OMUD_MUDEvent_Exp) event;
                _fView.eventMUD_Exp(ev);
                _fInfo.eventMUD_Exp(ev);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.PARTY){
                _fInfo.eventMUD_Party((OMUD_MUDEvent_Party) event);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.CHAT){
                OMUD_MUDEvent_Chat ev = (OMUD_MUDEvent_Chat) event;
                _fInfo.eventMUD_Chat(ev);
                eventMUD_Chat(ev);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.STATS){
                OMUD_MUDEvent_Stats ev = (OMUD_MUDEvent_Stats) event;
                _fView.updateExp(ev.dataExp);
                _fInfo.eventMUD_Stats(ev);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.INV){
                OMUD_MUDEvent_Inv ev = (OMUD_MUDEvent_Inv) event;
                _fInfo.eventMUD_Inv(ev);
                _fInv.eventMUD_Inv(ev);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.SHOP){
                _fInfo.eventMUD_Shop((OMUD_MUDEvent_Shop) event);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.SPELLS){
                _fInfo.eventMUD_Spells((OMUD_MUDEvent_Spells) event);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.WHO){
                _fInfo.eventMUD_Who((OMUD_MUDEvent_Who) event);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.TOP){
                _fInfo.eventMUD_Top((OMUD_MUDEvent_Top) event);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.WELCOME){
                _fInfo.eventMUD_Welcome((OMUD_MUDEvent_Welcome) event);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.BUFFER_SIZE){
                _fView.eventMUD_BufferSize((OMUD_MUDEvent_BufferSize) event);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.DEBUG_TEXT){
                _fInfo.eventMUD_DebugText((OMUD_MUDEvent_DebugText) event);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.BBS_LOC){
                _fView.eventMUD_BBSLoc((OMUD_MUDEvent_BBSLoc) event);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.GET_DATABLOCK){
                eventMUD_GetDataBlock((OMUD_MUDEvent_GetDataBlock) event);
            } else if (event.getType() == OMUD_MUDEvent.eEventType.SEND_CMD){
                _omt.sendText(((OMUD_MUDEvent_SendCmd) event).cmd);
            }
        }});
    }

    private void eventMUD_GetDataBlock(OMUD_MUDEvent_GetDataBlock event){
        if (_data.eBBSLoc == OMUD.eBBSLoc.MUD){
            for (int i = 0; i < event.arrlBlockTypes.size(); ++i){
                // manually get spells cmd because of kai check, otherwise just use the internal array...
                _omt.sendText(
                    event.arrlBlockTypes.get(i) == OMUD_MMUD_DataBlock.eBlockType.SPELLS ?
                        OMUD_MMUD_ParseBlock_CDSpells.getCmdText(_data.getMMUDData().is_kai) :
                        OMUD_MMUD_DataBlock.BLOCK_CMD_STRINGS[event.arrlBlockTypes.get(i).ordinal()]);
            }
        }
    }

    private void eventMUD_Chat(OMUD_MUDEvent_Chat event){
        for (int i = 0; i < event.dataChat.arrlChats.size(); ++i){
            if (event.dataChat.arrlChats.get(i).mega_cmd != null){
                boolean is_do_party_cmd = false;

                // responde with the same type of chat that was sent by the author and use the author name if is directed...
                StringBuilder sbChatResponsePrefix = new StringBuilder(OMUD_MMUD_DataBlock_Chat.CHAT_CMD_STRINGS[event.dataChat.arrlChats.get(i).type.ordinal()]);
                if (event.dataChat.arrlChats.get(i).type == OMUD_MMUD_DataBlock_Chat.eChatType.TELEPATH ||
                    event.dataChat.arrlChats.get(i).type == OMUD_MMUD_DataBlock_Chat.eChatType.SAY_DIR)
                    sbChatResponsePrefix.append(event.dataChat.arrlChats.get(i).name_author);
                sbChatResponsePrefix.append(" ");

                // NON-ARG COMMANDS HERE
                if (event.dataChat.arrlChats.get(i).mega_cmd.type == OMUD_MEGA.Command.eCmdType.EXP){
                    // {Made: 571,012,701  Needed: 0  Rate: 74,936 k/hr  Will level in: Now!}
                    _omt.sendText(sbChatResponsePrefix.toString() + "{Made: " + "0  Needed: " + event.dataExp.next_rem + "0  Rate: " + "0 k/hr  Will level in: " + "Now!}\n");
                } else if (event.dataChat.arrlChats.get(i).mega_cmd.type == OMUD_MEGA.Command.eCmdType.HEALTH){
                    //{HP=44/44}
                    //{HP=44/44, Resting}
                    //{HP=41/41,MA=24/32}
                    //{HP=41/41,MA=30/32, Resting}
                    //{HP=45/45,KAI=0/1}
                    //{HP=50/50,KAI=1/1, Resting}
                    _omt.sendText(sbChatResponsePrefix.toString() + "{" + event.dataStatline.getHealthString() + "}\n");
                } else if (event.dataChat.arrlChats.get(i).mega_cmd.type == OMUD_MEGA.Command.eCmdType.JOIN){
                    _omt.sendText("join " + event.dataChat.arrlChats.get(i).name_author + "\n");
                // ARG COMMANDS HERE
                } else if (event.dataChat.arrlChats.get(i).mega_cmd.type == OMUD_MEGA.Command.eCmdType.DO){
                    is_do_party_cmd = true;
                } else if (event.dataChat.arrlChats.get(i).mega_cmd.type == OMUD_MEGA.Command.eCmdType.PARTY){
                    is_do_party_cmd = event.dataChat.arrlChats.get(i).name_author.equals(event.dataParty.leader_name);
                }

                // combined code for @do and @party...
                if (is_do_party_cmd){
                    _omt.sendText(event.dataChat.arrlChats.get(i).mega_cmd.arg + "\n");
                    _omt.sendText(sbChatResponsePrefix.toString() + OMUD_MEGA.Command.OK_RESPONSE_STRING);
                }
            }
        }
    }

    // --------------
    // GUI Events
    // --------------
    public void updateCharLists(final String[][] strChars){
        _fView.updateCharLists(strChars);
        _fInfo.updateCharLists(strChars);
        _fInv.updateCharLists(strChars);
    }

    public void notifyGUIEvent(final OMUD_GUIEvent event){
        SwingUtilities.invokeLater(new Runnable(){public void run(){
                 if (event.getType() == OMUD_GUIEvent.eEventType.RESET_TERMINAL_CARET)
                _fView.resetTerminalCaret(_data.getParseData().omb.getPos());
            else if (event.getType() == OMUD_GUIEvent.eEventType.FOCUS_TELNET_INPUT)
                _fView.focusTelnetInput();
        }});
    }

    // --------------
    // Timers
    // --------------
    public void callbackTimerTimeoutTick(){
        _omt.forceUpdateMUD();
    }
}
