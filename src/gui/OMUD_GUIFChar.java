import javax.swing.JFrame;

class OMUD_GUIFChar extends JFrame{
    protected OMUD_GUIComboBox _cboChars = null;
    public OMUD_GUIFChar(String strTitle){
        _cboChars = new OMUD_GUIComboBox(null);
        setTitle(strTitle);
    }

    @SuppressWarnings("unchecked") // ignore J6/J7 change: "warning: [unchecked] unchecked call to addItem(E) as a member of the raw type JComboBox"
    public void updateCharLists(final String[][] strChars){
        _cboChars.removeAllItems();
        for (int i = 0; i < strChars.length; ++i)
            _cboChars.addItem(strChars[i][0] + " - " + strChars[i][1]);
    }
}
