import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Dimension;
import java.awt.Font;
import javax.imageio.ImageIO;
import java.io.File;

public class OMUD_GUIToggleButtonGroup extends JPanel{
    public static abstract class OMUD_GUIToggleButtonGroupListener{
        public abstract void toggleSelected(int sel_index);
    }

    private ArrayList<JButton>  _arrlToggles =  null;
    private ArrayList<String>   _arrlText =     null;
    private AL_Toggles          _alToggles =    null;
    private boolean             _vertical =     false;
    private int                 _sel_index =    -1;
    private OMUD_GUIToggleButtonGroupListener _tbgl = null;
    private static int BTN_VERT_WIDTH = 25;

    public OMUD_GUIToggleButtonGroup(OMUD_GUIToggleButtonGroupListener tbgl, boolean vertical){
        _tbgl =         tbgl;
        _vertical =     vertical;
        _arrlText =     new ArrayList<String>();
        _arrlToggles =  new ArrayList<JButton>();
        _alToggles =    new AL_Toggles();
    }

    public void layoutToggles(){
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gblc = new GridBagConstraints();

        if (_vertical){
            gblc.weightx =      0.0;
            gblc.weighty =      1.0;
            gblc.gridwidth =    0;
            gblc.gridheight =   _arrlToggles.size() - 1;
            gblc.fill =         GridBagConstraints.VERTICAL;
        } else {
            gblc.weightx =      1.0;
            gblc.weighty =      0;
            gblc.gridwidth =    _arrlToggles.size() - 1;
            gblc.gridheight =   0;
            gblc.fill =         GridBagConstraints.HORIZONTAL;
        }
        for (int i = 0; i < _arrlToggles.size() - 1; ++i)
            gbl.setConstraints(_arrlToggles.get(i), gblc);
        if (_vertical)
             gblc.gridheight =  GridBagConstraints.REMAINDER;
        else gblc.gridwidth =   GridBagConstraints.REMAINDER;
        gbl.setConstraints(_arrlToggles.get(_arrlToggles.size() - 1), gblc);

        setLayout(gbl);
        for (int i = 0; i < _arrlToggles.size(); ++i)
            add(_arrlToggles.get(i));
    }

    private void updateToggle(JButton btnToggle, String strText, boolean selected){
        if (selected){
            if (_arrlToggles.size() == 1)
                 btnToggle.setText(strText);
            else btnToggle.setText(">> " + strText + " <<");
            btnToggle.setBackground(OMUD.CLR_BG_TOGGLE_ON);
        } else {
            btnToggle.setText(strText);
            btnToggle.setBackground(OMUD.CLR_BG_TOGGLE_OFF);
        }
    }

    public void setIndexText(int index, String strText){
        if (index < _arrlToggles.size())
            _arrlToggles.get(index).setText(strText);
    }

    public int getSelectedIndex(){return _sel_index;}
    public void setSelectedIndex(int index){
        if (index > -1 && index < _arrlToggles.size())
            _alToggles.actionPerformed(new ActionEvent(_arrlToggles.get(index), ActionEvent.ACTION_PERFORMED, ""));
    }

    public void add(String strText, String strIconFilepath){
        JButton btnToggle = new JButton(strText);

        if (strIconFilepath.length() > 0)
            try { btnToggle.setIcon(new ImageIcon(ImageIO.read(new File(strIconFilepath))));
            } catch (Exception e) {OMUD_Log.toConsoleError("Error adding toggle button image: " + e.getMessage());}

        if (btnToggle != null){
            btnToggle.setForeground(OMUD.CLR_FG_WHITE);
            btnToggle.setFont(new Font(getFont().getName(), Font.BOLD, getFont().getSize()));

            if (_vertical)
                btnToggle.setPreferredSize(new Dimension(BTN_VERT_WIDTH, 0));

            _arrlToggles.add(btnToggle);
            _arrlText.add(strText);
            updateToggle(btnToggle, strText, false);
            btnToggle.addActionListener(_alToggles);

            // set first toggle to ON since we have more than one now...
            if (_arrlToggles.size() == 2){
                _sel_index = 0; // was -1 if only had one button
                _arrlToggles.get(0).removeActionListener(_alToggles);
                    updateToggle(_arrlToggles.get(0), _arrlText.get(0), true);
                _arrlToggles.get(0).addActionListener(_alToggles);
            }
        }
    }

    // --------------
    // GUI Events
    // --------------
    private class AL_Toggles implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            // NOTE: keep only one seelcted: have to remove event listeners to avoid recursion

            // only one togglee...
            if (_arrlToggles.size() == 1){
                _arrlToggles.get(0).removeActionListener(_alToggles);
                if (_sel_index == -1) {
                    _sel_index = 0;
                    updateToggle(_arrlToggles.get(0), _arrlText.get(0), true);
                } else {
                    _sel_index = -1;
                    updateToggle(_arrlToggles.get(0), _arrlText.get(0), false);
                }
                _arrlToggles.get(0).addActionListener(_alToggles);

            // multiple toggles...
            } else if (event.getSource() != _arrlToggles.get(_sel_index)){
                for (int i = 0; i < _arrlToggles.size(); ++i){
                    _arrlToggles.get(i).removeActionListener(_alToggles);
                        if (event.getSource() == _arrlToggles.get(i))
                             updateToggle(_arrlToggles.get((_sel_index = i)), _arrlText.get(i), true);
                        else updateToggle(_arrlToggles.get(i), _arrlText.get(i), false);
                    _arrlToggles.get(i).addActionListener(_alToggles);
                }
            }

            if (_tbgl != null)
                _tbgl.toggleSelected(_sel_index);
        }
    }
}
