import java.util.ArrayList;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Dimension;
import javax.swing.JPanel;

public class OMUD_GUIToolbarChars extends OMUD_GUIToolbar {
    public static enum eBtnType{
        // open char...
        CHAR_OPEN,
        // edit/create/remove sel char(s)...
        CHAR_EDIT,
        CHAR_CREATE,
        CHAR_REMOVE,
        // windows...
        WIN_SHOW,
        WIN_HIDE,
        // other...
        SETTINGS,
        WEB_HELP
    }

    private static final String[] BTN_FILEPATH_STRINGS = {
        // open char...
        "images/icons/icon-fchars-open.png",
        // edit/create/remove sel char(s)...
        "images/icons/icon-fchars-edit.png",
        "images/icons/icon-fchars-create.png",
        "images/icons/icon-fchars-remove.png",
        // windows...
        "images/icons/icon-fchars-win_show.png",
        "images/icons/icon-fchars-win_hide.png",
        // other...
        "images/icons/icon-fchars-settings.png",
        "images/icons/icon-fchars-web_help.png"
    };

    private static final String[] BTN_TOOLTIP_STRINGS = {
        // open char...
        "Open Char",
        // edit/create/remove sel char(s)...
        "Edit Sel Char",
        "Create Char",
        "Remove Sel Char",
        // windows...
        "Show All Windows",
        "Hide All Windows",
        // other...
        "Global Settings",
        "Website/Help"
    };

    public OMUD_GUIToolbarChars(ToolbarButtonClickedCallback tbcc){
        super(tbcc);

        for (int i = 0; i < eBtnType.values().length; ++i)
            addBtn(BTN_FILEPATH_STRINGS[i], BTN_TOOLTIP_STRINGS[i], false, BTN_VHEIGHT, BTN_VWIDTH);

        layoutToolbar();
    }

    private void layoutToolbar(){
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gblc = new GridBagConstraints();
        final int SPACER_COUNT = 3;

        gblc.weightx =      0.0;
        gblc.weighty =      0.0;
        gblc.gridwidth =    _arrlBtns.size() + SPACER_COUNT - 1;
        gblc.gridheight =   0;
        gblc.fill =         GridBagConstraints.HORIZONTAL;
        setMinimumSize(new Dimension((_arrlBtns.size() * BTN_VHEIGHT) + (SPACER_COUNT * SPACER_SIZE), BTN_VWIDTH));

        for (int i = 0; i < _arrlBtns.size(); ++i){
            if (i == eBtnType.CHAR_EDIT.ordinal() || i == eBtnType.WIN_SHOW.ordinal() || i == eBtnType.SETTINGS.ordinal()){
                JPanel pnlSpacer = new JPanel();
                pnlSpacer.setBackground(OMUD.CLR_BG_DARK_GREY);
                pnlSpacer.setPreferredSize(new Dimension(SPACER_SIZE, BTN_VWIDTH));
                gbl.setConstraints(pnlSpacer, gblc);
                add(pnlSpacer);
            } else if (i == _arrlBtns.size() - 1){
                gblc.gridwidth = GridBagConstraints.REMAINDER;
            }
            gbl.setConstraints(_arrlBtns.get(i), gblc);
            add(_arrlBtns.get(i));
        }
        setLayout(gbl);
    }

    protected void toolbarButtonClicked(int btn_id){}
}
