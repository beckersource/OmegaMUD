import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.ButtonModel;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.MouseInfo;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;

public class OMUD_GUIButton extends JButton {
    public static abstract class OMUD_GUIButtonListener{
        public abstract void released(OMUD_GUIButton btn);
    }

    private int _id = -1;
    private OMUD_GUIButtonListener _btnl = null;

    public OMUD_GUIButton(ImageIcon imgIcon, String strTooltip, int id, OMUD_GUIButtonListener btnl, Color clrBG){
        super(imgIcon);
        init(strTooltip, id, btnl, clrBG);
    }

    public OMUD_GUIButton(String strText, String strTooltip, int id, OMUD_GUIButtonListener btnl, Color clrBG){
        super(strText);
        init(strTooltip, id, btnl, clrBG);
    }

    private void init(String strTooltip, int id, OMUD_GUIButtonListener btnl, Color clrBG){
        _id   = id;
        _btnl = btnl;

        setToolTipText(strTooltip);
        setForeground(OMUD.CLR_FG_WHITE);
        setBackground(clrBG != null ? clrBG : OMUD.CLR_BG_DARK_BUTTON);
        addMouseListener(new ML_Button());
    }

    private class ML_Button extends MouseAdapter {
        public void mouseReleased(MouseEvent event) {
            if (_btnl != null){
                // make sure release was actually on the button...
                OMUD_GUIButton btn = (OMUD_GUIButton) event.getSource();
                Rectangle rect = btn.getBounds();
                rect.setLocation(btn.getLocationOnScreen());
                if (rect.contains(MouseInfo.getPointerInfo().getLocation()))
                    _btnl.released((OMUD_GUIButton) event.getSource());
            }
        }
    }

    public int getID(){return _id;}
}
