import java.awt.Font;
import java.awt.Color;
import javax.swing.JComboBox;

class OMUD_GUIComboBox extends JComboBox{
    public OMUD_GUIComboBox(Color clrBG){
        setForeground(OMUD.CLR_FG_WHITE);
        setBackground(clrBG != null ? clrBG : OMUD.CLR_BG_DARK_COMBO);
        setFont(new Font(getFont().getName(), Font.BOLD, getFont().getSize()));
    }
}
