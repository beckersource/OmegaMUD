import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableCellRenderer;

public class OMUD_GUITableInv {
    private OMUD_GUITable   _table  = null;
    private JScrollPane     _scroll = null;
    private static final int COL_WIDTH_EQ =     25;
    private static final int COL_WIDTH_QTY  =   50;
    private static final int COL_WIDTH_SLOT =   100;

    public OMUD_GUITableInv(){
        _table = new OMUD_GUITable();
        _table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        _table.setModel(new OMUD_GUITableModelInv());
        _scroll = new JScrollPane(_table);
        _scroll.getViewport().setBackground(OMUD.CLR_BG_DARK_GREY);

        // header...
        _table.getTableHeader().setBackground(OMUD.CLR_BG_DARK_TABLE);
        _table.getTableHeader().setForeground(OMUD.CLR_FG_WHITE);
        _table.getTableHeader().setFont(OMUD.getTerminalFont());

        // set alignments...
        DefaultTableCellRenderer rend = new DefaultTableCellRenderer();
        rend.setHorizontalAlignment(JLabel.LEFT);
        _table.getColumnModel().getColumn(0).setCellRenderer(rend);
        _table.getColumnModel().getColumn(1).setCellRenderer(rend);
        _table.getColumnModel().getColumn(2).setCellRenderer(rend);
        rend.setHorizontalAlignment(JLabel.CENTER);
        _table.getColumnModel().getColumn(3).setCellRenderer(rend);

        // set sizes...
        _table.getColumnModel().getColumn(0).setMinWidth(COL_WIDTH_EQ);
        _table.getColumnModel().getColumn(1).setMinWidth(COL_WIDTH_QTY);
        _table.getColumnModel().getColumn(2).setMinWidth(COL_WIDTH_SLOT);
        _table.getColumnModel().getColumn(0).setMaxWidth(COL_WIDTH_EQ);
        _table.getColumnModel().getColumn(1).setMaxWidth(COL_WIDTH_QTY);
        _table.getColumnModel().getColumn(2).setMaxWidth(COL_WIDTH_SLOT);
    }

    public OMUD_GUITable getTable() {return _table;}
    public JScrollPane   getScroll(){return _scroll;}
};
