import java.util.ArrayList;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Dimension;
import javax.swing.JPanel;
import java.util.Random;

public class OMUD_GUIToolbarViewLeft extends OMUD_GUIToolbarView {
    private static enum eBtnType{
        CUSTOM1,
        CUSTOM2,
        CUSTOM3,
    }

    private static final String[] BTN_FILEPATH_STRINGS = {
        "",
        "",
        "",
    };

    private static final String[] BTN_TOOLTIP_STRINGS = {
        "Custom1",
        "Custom2",
        "Custom3",
    };

    private static final boolean[] BTN_MUD_REQ = {
        true,
        true,
        true,
    };

    public OMUD_GUIToolbarViewLeft(OMUD_Telnet omt, OMUD_IMUDEvents omme, OMUD_IGUIEvents omge){
        super(omt, omme, omge);

        for (int i = 0; i < eBtnType.values().length; ++i)
            addBtn(BTN_FILEPATH_STRINGS[i], BTN_TOOLTIP_STRINGS[i], BTN_MUD_REQ[i], BTN_VWIDTH, BTN_VHEIGHT);

        layoutToolbar();
    }

    private void layoutToolbar(){
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gblc = new GridBagConstraints();
        final int SPACER_COUNT = _arrlBtns.size() - 1;

        gblc.weightx =      0.0;
        gblc.weighty =      0.0;
        gblc.gridwidth =    0;
        gblc.gridheight =   _arrlBtns.size() + SPACER_COUNT - 1;
        gblc.fill =         GridBagConstraints.VERTICAL;
        setMinimumSize(new Dimension(BTN_VWIDTH, (_arrlBtns.size() * BTN_VHEIGHT) + (SPACER_COUNT * SPACER_SIZE)));

        for (int i = 0; i < _arrlBtns.size(); ++i){
            if (i > 0){
                JPanel pnlSpacer = new JPanel();
                pnlSpacer.setBackground(OMUD.CLR_BG_DARK_GREY);
                pnlSpacer.setPreferredSize(new Dimension(BTN_VWIDTH, SPACER_SIZE));
                gbl.setConstraints(pnlSpacer, gblc);
                add(pnlSpacer);
            }
            if (i == _arrlBtns.size() - 1)
                gblc.gridwidth = GridBagConstraints.REMAINDER;
            gbl.setConstraints(_arrlBtns.get(i), gblc);
            add(_arrlBtns.get(i));
        }
        setLayout(gbl);
    }

    // --------------
    // GUI Events
    // --------------
    protected void toolbarButtonClicked(int btn_id){
        // DEBUG: spam testing: mud entry commands...
        if (btn_id == eBtnType.CUSTOM1.ordinal()){
            _omt.sendText("\n");
            _omt.sendText("stat\n");
            _omt.sendText("exp\n");
            _omt.sendText("i\n");
            _omt.sendText("sp\n");
            _omt.sendText("who\n");
        // DEBUG: spam testing: other commnds...
        } else if (btn_id == eBtnType.CUSTOM2.ordinal()){
            _omt.sendText("sp\n");
            _omt.sendText("sp\n");
            _omt.sendText("sp\n");
            _omt.sendText("sp\n");
            _omt.sendText("sp\n");
            _omt.sendText("sp\n");
        // DEBUG: spam testing: single char spam as random numbers...
        } else if (btn_id == eBtnType.CUSTOM3.ordinal()){
            Random rand = new Random();
            for (int i = rand.nextInt(5); i >= 0; --i){
                for (int j = rand.nextInt(7); j >= 0; --j)
                    _omt.sendText(Integer.toString(rand.nextInt(10)));
                _omt.sendText("\n");
            }
        }
        _omge.notifyGUIEvent(new OMUD_GUIEvent_FocusTelnetInput());
    }
}
