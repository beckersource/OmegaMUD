public abstract class OMUD_GUIToolbarView extends OMUD_GUIToolbar {
    protected   OMUD_Telnet     _omt =  null;
    protected   OMUD_IMUDEvents _omme = null;
    protected   OMUD_IGUIEvents _omge = null;
    public OMUD_GUIToolbarView(OMUD_Telnet omt, OMUD_IMUDEvents omme, OMUD_IGUIEvents omge){
        super(null);
        _omt =  omt;
        _omme = omme;
        _omge = omge;
    }
}
