import javax.swing.JMenu;

public class OMUD_GUIMenu extends JMenu {
    public OMUD_GUIMenu(String label){
        super(label);
        setBackground(OMUD.CLR_BG_DARK_MENU);
        setForeground(OMUD.CLR_FG_WHITE);
    }
}
