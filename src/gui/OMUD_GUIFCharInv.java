import java.io.File;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.image.BufferedImage;
//import java.awt.Component;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import javax.imageio.ImageIO;

public class OMUD_GUIFCharInv extends OMUD_GUIFChar{
    private OMUD_IMUDEvents         _omme =         null;
    private JPanel                  _panel =        null;
    private JPanel                  _pnlLabels =    null;
    private JPanel                  _pnlRight =     null;
    private Image                   _imgBG =        null;
    private ImageIcon               _icoBG =        null;
    private BufferedImage           _bufBG =        null;
    private JLabel                  _lblBG =        null;
    private Dimension               _dimBG =        null;
    private OMUD_GUITableInv        _tbl =          null;
    private OMUD_GUITableModelInv   _tblm =         null;
    private OMUD_GUITextField       _lblRunic =     null;
    private OMUD_GUITextField       _lblPlat =      null;
    private OMUD_GUITextField       _lblGold =      null;
    private OMUD_GUITextField       _lblSilver =    null;
    private OMUD_GUITextField       _lblCopper =    null;
    private OMUD_GUITextField       _lblWealth =    null;
    private OMUD_GUITextField       _lblEnc =       null;
    private OMUD_GUIToggleButtonGroup _tglgSort =   null;
    private static final int FRAME_LIST_XPAD = 150;

    public OMUD_GUIFCharInv(OMUD_IMUDEvents omme){
        super("Char Inventory");
        try {_bufBG = ImageIO.read(new File("images/bg/bg-inv_male.png"));} catch (Exception e) {OMUD_Log.toConsoleError("Error loading inv bg: " + e.getMessage());}
        setMinimumSize(new Dimension((int) (_bufBG.getWidth()  * 1.5) + FRAME_LIST_XPAD, (int) (_bufBG.getHeight() * 0.75)));
        setPreferredSize(getMinimumSize()); // for now, smaller startup size

        _omme = omme;

        _pnlLabels =    new JPanel();
        _pnlRight =     new JPanel();
        _icoBG =        new ImageIcon(_bufBG);
        _dimBG =        new Dimension(_bufBG.getWidth(), _bufBG.getHeight());
        _tbl =          new OMUD_GUITableInv();
        _tblm =         (OMUD_GUITableModelInv) _tbl.getTable().getModel();

        _tglgSort = new OMUD_GUIToggleButtonGroup(new TGLL_Sort(), false);
        _tglgSort.add("Sort EQ",  "");
        _tglgSort.add("Sort All", "");
        _tglgSort.layoutToggles();

        // money...
        _lblRunic =  new OMUD_GUITextField("R:",   false, true, false);
        _lblPlat =   new OMUD_GUITextField("P:",   false, true, false);
        _lblGold =   new OMUD_GUITextField("G:",   false, true, false);
        _lblSilver = new OMUD_GUITextField("S:",   false, true, false);
        _lblCopper = new OMUD_GUITextField("C:",   false, true, false);
        _lblWealth = new OMUD_GUITextField("$:",   false, true, false);
        _lblEnc =    new OMUD_GUITextField("Enc:", false, true, false);

        add((_panel = new JPanel()));
        layoutRightBottom();
        layoutRight();
        layoutInv(false);
        pack();

        // event listeners...
        addComponentListener(new CA_Frame());
    }

    private Dimension getAspectSize(Dimension dimImg, Dimension dimBounds){
        double ratio_width =  dimBounds.getWidth()  / dimImg.getWidth();
        double ratio_height = dimBounds.getHeight() / dimImg.getHeight();
        double ratio = Math.min(ratio_width, ratio_height);
        return new Dimension((int) (dimImg.width * ratio), (int) (dimImg.height * ratio));
    }

    private void layoutRightBottom(){
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gblc = new GridBagConstraints();

        // money...
        gblc.weightx =      1.0;
        gblc.weighty =      0;
        gblc.gridwidth =    4;
        gblc.gridheight =   1;
        gblc.fill =         GridBagConstraints.HORIZONTAL;
        gbl.setConstraints(_lblRunic,   gblc);
        gbl.setConstraints(_lblPlat,    gblc);
        gbl.setConstraints(_lblGold,    gblc);
        gbl.setConstraints(_lblSilver,  gblc);
        gblc.gridwidth =    GridBagConstraints.REMAINDER;
        gbl.setConstraints(_lblCopper,  gblc);

        // wealth, enc:
        // make a separate panel for the wealth and enc so that the grid doesn't have to align with coins above...
        GridBagLayout gblWE = new GridBagLayout();
        JPanel pnlWealthEnc = new JPanel();
        gblc.weightx =      0.6;
        gblc.weighty =      0;
        gblc.gridwidth =    1;
        gblc.gridheight =   GridBagConstraints.REMAINDER;
        gblc.fill =         GridBagConstraints.HORIZONTAL;
        gblWE.setConstraints(_lblWealth,  gblc);
        gblc.weightx =      0.4;
        gblc.gridwidth =    GridBagConstraints.REMAINDER;
        gblWE.setConstraints(_lblEnc,     gblc);
        pnlWealthEnc.setLayout(gblWE);
        pnlWealthEnc.add(_lblWealth);
        pnlWealthEnc.add(_lblEnc);
        // now set the constraints on the WE panel to expand fully...
        gblc.weightx =      1.0;
        gblc.weighty =      0;
        gblc.gridwidth =    0;
        gblc.gridheight =   GridBagConstraints.REMAINDER;
        gblc.fill =         GridBagConstraints.HORIZONTAL;
        gbl.setConstraints(pnlWealthEnc,  gblc);

        _pnlLabels.setLayout(gbl);
        _pnlLabels.add(_lblRunic);
        _pnlLabels.add(_lblPlat);
        _pnlLabels.add(_lblGold);
        _pnlLabels.add(_lblSilver);
        _pnlLabels.add(_lblCopper);
        _pnlLabels.add(pnlWealthEnc);
    }

    private void layoutRight(){
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gblc = new GridBagConstraints();

        // sort panel...
        gblc.weightx =      0.0;
        gblc.weighty =      0.0;
        gblc.gridwidth =    1;
        gblc.gridheight =   2;
        gblc.fill =         GridBagConstraints.HORIZONTAL;
        gbl.setConstraints(_cboChars, gblc);
        gblc.gridwidth =    GridBagConstraints.REMAINDER;
        gbl.setConstraints(_tglgSort, gblc);

        // table...
        gblc.weightx =      1.0;
        gblc.weighty =      1.0;
        gblc.gridwidth =    0;
        //gblc.gridheight =   1;
        gblc.fill =         GridBagConstraints.BOTH;
        gbl.setConstraints(_tbl.getScroll(), gblc);

        // bottom labels...
        gblc.weightx =      1.0;
        gblc.weighty =      0;
        gblc.gridwidth =    4;
        gblc.gridheight =   GridBagConstraints.REMAINDER;
        gblc.fill =         GridBagConstraints.HORIZONTAL;
        gbl.setConstraints(_pnlLabels, gblc);

        _pnlRight.setLayout(gbl);
        _pnlRight.add(_cboChars);
        _pnlRight.add(_tglgSort);
        _pnlRight.add(_tbl.getScroll());
        _pnlRight.add(_pnlLabels);
    }

    private void layoutInv(boolean resized){
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gblc = new GridBagConstraints();

        if (resized){
            _panel.remove(_lblBG);
            _panel.remove(_tbl.getScroll());
            _panel.remove(_pnlRight);
            _panel.revalidate();
            _panel.repaint();

            Dimension dimAvail = new Dimension((int) getContentPane().getSize().getWidth() - FRAME_LIST_XPAD, (int) getContentPane().getSize().getHeight());
            Dimension dimSize = getAspectSize(_dimBG, dimAvail);
            _imgBG = _bufBG.getScaledInstance((int) dimSize.getWidth(), (int) dimSize.getHeight(), BufferedImage.SCALE_SMOOTH);
            _icoBG = new ImageIcon(_imgBG);
        }
        _lblBG = new JLabel(_icoBG);
        _lblBG.setBackground(OMUD.CLR_BG_DARK_GREY); // to prevent startup flickering

        // left: paper doll...
        gblc.weightx =      0.0;
        gblc.weighty =      0.0;
        gblc.gridwidth =    1;
        gblc.gridheight =   0;
        gblc.fill =         GridBagConstraints.VERTICAL;
        gbl.setConstraints(_lblBG, gblc);

        // right side...
        gblc.weightx =      1.0;
        gblc.weighty =      1.0;
        gblc.gridwidth =    0;
        gblc.gridheight =   GridBagConstraints.REMAINDER;
        gblc.fill =         GridBagConstraints.BOTH;
        gbl.setConstraints(_pnlRight, gblc);

        _panel.setLayout(gbl);
        _panel.add(_lblBG);
        _panel.add(_pnlRight);
    }

    private class CA_Frame extends ComponentAdapter{
        public void componentResized(ComponentEvent evt) {
            //Component cmp = (Component) evt.getSource();
            if (_lblBG != null)
                layoutInv(true);
        }
    }

    private class TGLL_Sort extends OMUD_GUIToggleButtonGroup.OMUD_GUIToggleButtonGroupListener{
        public void toggleSelected(int sel_index){
            _omme.notifyMUDEvent(new OMUD_MUDEvent_GetDataBlock(OMUD_MMUD_DataBlock.eBlockType.INV));
            SwingUtilities.invokeLater(new Runnable(){public void run(){
                _tbl.getTable().requestFocus();
            }});
        }
    }

    // --------------
    // MUD Events
    // --------------
    public void eventMUD_Inv(OMUD_MUDEvent_Inv event){
        _tblm.clear();

        // probably not the best approach, but using for now...
        if (_tglgSort.getSelectedIndex() == 0){
            for (int i = 0; i < event.dataInv.arrlItems.size(); ++i)
                if (event.dataInv.arrlItems.get(i).item_loc != OMUD_MMUD_DataItem.eItemLoc.NONE)
                    _tblm.addItem(event.dataInv.arrlItems.get(i));
            for (int i = 0; i < event.dataInv.arrlItems.size(); ++i)
                if (event.dataInv.arrlItems.get(i).item_loc == OMUD_MMUD_DataItem.eItemLoc.NONE)
                    _tblm.addItem(event.dataInv.arrlItems.get(i));
        } else {
            for (int i = 0; i < event.dataInv.arrlItems.size(); ++i)
                _tblm.addItem(event.dataInv.arrlItems.get(i));
        }

        // always show keys at the bottom...
        for (int i = 0; i < event.dataInv.arrlKeys.size(); ++i)
            _tblm.addItem(event.dataInv.arrlKeys.get(i));

        _lblRunic.setText("R: " +  event.dataInv.coins.runic);
        _lblPlat.setText("P: " +   event.dataInv.coins.plat);
        _lblGold.setText("G: " +   event.dataInv.coins.gold);
        _lblSilver.setText("S: " + event.dataInv.coins.silver);
        _lblCopper.setText("C: " + event.dataInv.coins.copper);
        _lblWealth.setText("$: " + event.dataInv.wealth);
        _lblEnc.setText("Enc: " +  event.dataInv.enc_level + " (" + event.dataInv.enc_cur + "/" + event.dataInv.enc_max + String.format(") [%.0f", ((float) event.dataInv.enc_cur / event.dataInv.enc_max) * 100) + "%]");
    }
}
