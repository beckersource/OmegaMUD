import java.util.ArrayList;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Dimension;
import javax.swing.JPanel;

public class OMUD_GUIToolbarViewBottom extends OMUD_GUIToolbarView {
    private static enum eBtnType{
        // connect/hangup...
        MAIN_CONNECT,
        MAIN_HANGUP_MODE,
        // movement...
        MAIN_GOTO,
        MAIN_LOOP,
        MAIN_ROAM,
        MAIN_BACKSTEP,
        MAIN_STOP,
        // auto...
        AUTO_MODE,
        AUTO_COMBAT,
        AUTO_COMBAT_ROOM,
        AUTO_HEAL_REST,
        AUTO_BLESS,
        AUTO_LIGHT,
        AUTO_SNEAK,
        AUTO_HIDE,
        AUTO_SEARCH,
        AUTO_GET_ITEMS,
        AUTO_GET_MONEY
    }

    private static final String   BTN_AUTO_MODE_OFF = "images/icons/icon-fview-tb-auto_mode_off.png";
    private static final String[] BTN_FILEPATH_STRINGS = {
        // connect/hangup...
        "images/icons/icon-fview-tb-main_connect.png",
        "images/icons/icon-fview-tb-main_hangup_mode.png",
        // movement...
        "images/icons/icon-fview-tb-main_goto.png",
        "images/icons/icon-fview-tb-main_loop.png",
        "images/icons/icon-fview-tb-main_roam.png",
        "images/icons/icon-fview-tb-main_backstep.png",
        "images/icons/icon-fview-tb-main_stop.png",
        // auto...
        "images/icons/icon-fview-tb-auto_mode_on.png", // OFF set as separate string
        "images/icons/icon-fview-tb-auto_combat.png",
        "images/icons/icon-fview-tb-auto_combat_room.png",
        "images/icons/icon-fview-tb-auto_heal_rest.png",
        "images/icons/icon-fview-tb-auto_bless.png",
        "images/icons/icon-fview-tb-auto_light.png",
        "images/icons/icon-fview-tb-auto_sneak.png",
        "images/icons/icon-fview-tb-auto_hide.png",
        "images/icons/icon-fview-tb-auto_search.png",
        "images/icons/icon-fview-tb-auto_get_items.png",
        "images/icons/icon-fview-tb-auto_get_money.png"
    };

    private static final String[] BTN_TOOLTIP_STRINGS = {
        // connect/hangup...
        "Connect/Disconnect",
        "Allow/Disallow Auto-Hangup",
        // movement...
        "Goto",
        "Loop",
        "Roam",
        "Backstep",
        "Stop",
        // auto...
        "Toggle Auto-mode On/Off",
        "Auto Combat",
        "Auto Room Combat (AoE/Nuke)",
        "Auto Heal/Rest",
        "Auto Bless",
        "Auto Light",
        "Auto Sneak",
        "Auto Hide",
        "Auto Search",
        "Auto Get Items",
        "Auto Get Money"
    };

    private static final boolean[] BTN_MUD_REQ = {
        // connect/hangup...
        false,
        false,
        // movement...
        true,
        true,
        true,
        true,
        true,
        // auto...
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false
    };

    public OMUD_GUIToolbarViewBottom(OMUD_Telnet omt, OMUD_IMUDEvents omme, OMUD_IGUIEvents omge){
        super(omt, omme, omge);

        for (int i = 0; i < eBtnType.values().length; ++i)
            addBtn(BTN_FILEPATH_STRINGS[i], BTN_TOOLTIP_STRINGS[i], BTN_MUD_REQ[i], BTN_VHEIGHT, BTN_VWIDTH);

        layoutToolbar();
    }

    private void layoutToolbar(){
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gblc = new GridBagConstraints();
        final int SPACER_COUNT = 2;

        gblc.weightx =      0.0;
        gblc.weighty =      0.0;
        gblc.gridwidth =    _arrlBtns.size() + SPACER_COUNT - 1;
        gblc.gridheight =   0;
        gblc.fill =         GridBagConstraints.HORIZONTAL;
        setMinimumSize(new Dimension((_arrlBtns.size() * BTN_VHEIGHT) + (SPACER_COUNT * SPACER_SIZE), BTN_VWIDTH));

        for (int i = 0; i < _arrlBtns.size(); ++i){
            if (i == eBtnType.MAIN_GOTO.ordinal() || i == eBtnType.AUTO_MODE.ordinal()){
                JPanel pnlSpacer = new JPanel();
                pnlSpacer.setBackground(OMUD.CLR_BG_DARK_GREY);
                pnlSpacer.setPreferredSize(new Dimension(SPACER_SIZE, BTN_VWIDTH));
                gbl.setConstraints(pnlSpacer, gblc);
                add(pnlSpacer);
            } else if (i == _arrlBtns.size() - 1){
                gblc.gridwidth = GridBagConstraints.REMAINDER;
            }
            gbl.setConstraints(_arrlBtns.get(i), gblc);
            add(_arrlBtns.get(i));
        }
        setLayout(gbl);
    }

    // --------------
    // GUI Events
    // --------------
    protected void toolbarButtonClicked(int btn_id){
        _omge.notifyGUIEvent(new OMUD_GUIEvent_FocusTelnetInput());
    }
}
