import javax.swing.JMenuItem;

public class OMUD_GUIMenuItem extends JMenuItem {
    public OMUD_GUIMenuItem(String label){
        super(label);
        setBackground(OMUD.CLR_BG_DARK_MENU);
        setForeground(OMUD.CLR_FG_WHITE);
    }
}
