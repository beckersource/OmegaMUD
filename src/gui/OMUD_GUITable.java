import javax.swing.JTable;

public class OMUD_GUITable extends JTable {
    public OMUD_GUITable(){
        setBackground(OMUD.CLR_BG_DARK_GREY);
        setForeground(OMUD.CLR_FG_WHITE);
        setFont(OMUD.getTerminalFont());
        setCellSelectionEnabled(false);
        setRowSelectionAllowed(true);
    }
};
