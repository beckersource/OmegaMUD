import java.util.ArrayList;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Dimension;
import javax.swing.JPanel;

public class OMUD_GUIToolbarViewRight extends OMUD_GUIToolbarView {
    private static enum eBtnType{
        // cmds...
        CMD_EXP,
        CMD_INV,
        CMD_STATS,
        CMD_PARTY,
        CMD_WHO,
        // all ops...
        ALL_GET,
        ALL_DROP,
        ALL_EQUIP,
        ALL_DEPOSIT,
        // other...
        OTHER_CHAR_CONFIG,
        OTHER_FIND_GAMEDATA,
        OTHER_VIEW_LOG,
        // stats...
        //STATS_CHAR,
        //STATS_EXP_RATE,
        //STATS_SESSION,
        //STATS_TIME
    }

    private static final String[] BTN_FILEPATH_STRINGS = {
        // cmds...
        "images/icons/icon-fview-tb-cmd_exp.png",
        "images/icons/icon-fview-tb-cmd_inv.png",
        "images/icons/icon-fview-tb-cmd_stats.png",
        "images/icons/icon-fview-tb-cmd_party.png",
        "images/icons/icon-fview-tb-cmd_who.png",
        // all ops...
        "images/icons/icon-fview-tb-all_get.png",
        "images/icons/icon-fview-tb-all_drop.png",
        "images/icons/icon-fview-tb-all_equip.png",
        "images/icons/icon-fview-tb-all_deposit.png",
        // other...
        "images/icons/icon-fview-tb-other_char_config.png",
        "images/icons/icon-fview-tb-other_find_gamedata.png",
        "images/icons/icon-fview-tb-other_view_log.png",
        // stats...
        //"images/icons/icon-fview-tb-stats_char.png",
        //"images/icons/icon-fview-tb-stats_exp_rate.png",
        //"images/icons/icon-fview-tb-stats_session.png",
        //"images/icons/icon-fview-tb-stats_time.png"
    };

    private static final String[] BTN_TOOLTIP_STRINGS = {
        // cmds...
        "Refresh Exp",
        "Refresh Inv",
        "Refresh Stats",
        "Refresh Party",
        "Refresh Who",
        // all ops...
        "Get All Items",
        "Drop All Items",
        "Equip All Items",
        "Deposit All Money",
        // other...
        "Char Config",
        "Find GameData",
        "View Log",
        // stats...
        //"View Char Stats",
        //"View Exp Rate Stats",
        //"View Session Stats",
        //"View Time Stats"
    };

    private static final boolean[] BTN_MUD_REQ = {
        // cmds...
        true,
        true,
        true,
        true,
        true,
        // all ops...
        true,
        true,
        true,
        true,
        // other...
        false,
        false,
        false,
        // stats...
        //STATS_CHAR,
        //STATS_EXP_RATE,
        //STATS_SESSION,
        //STATS_TIME
    };

    public OMUD_GUIToolbarViewRight(OMUD_Telnet omt, OMUD_IMUDEvents omme, OMUD_IGUIEvents omge){
        super(omt, omme, omge);

        for (int i = 0; i < eBtnType.values().length; ++i)
            addBtn(BTN_FILEPATH_STRINGS[i], BTN_TOOLTIP_STRINGS[i], BTN_MUD_REQ[i], BTN_VWIDTH, BTN_VHEIGHT);

        layoutToolbar();
    }

    private void layoutToolbar(){
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gblc = new GridBagConstraints();
        final int SPACER_COUNT = 2;

        gblc.weightx =      0.0;
        gblc.weighty =      0.0;
        gblc.gridwidth =    0;
        gblc.gridheight =   _arrlBtns.size() + SPACER_COUNT - 1;
        gblc.fill =         GridBagConstraints.VERTICAL;
        setMinimumSize(new Dimension(BTN_VWIDTH, (_arrlBtns.size() * BTN_VHEIGHT) + (SPACER_COUNT * SPACER_SIZE)));

        for (int i = 0; i < _arrlBtns.size(); ++i){
            if (i == eBtnType.ALL_GET.ordinal() || i == eBtnType.OTHER_CHAR_CONFIG.ordinal()){
                JPanel pnlSpacer = new JPanel();
                pnlSpacer.setBackground(OMUD.CLR_BG_DARK_GREY);
                pnlSpacer.setPreferredSize(new Dimension(BTN_VWIDTH, SPACER_SIZE));
                gbl.setConstraints(pnlSpacer, gblc);
                add(pnlSpacer);
            } else if (i == _arrlBtns.size() - 1){
                gblc.gridwidth = GridBagConstraints.REMAINDER;
            }
            gbl.setConstraints(_arrlBtns.get(i), gblc);
            add(_arrlBtns.get(i));
        }
        setLayout(gbl);
    }

    // --------------
    // GUI Events
    // --------------
    protected void toolbarButtonClicked(int btn_id){
             if (btn_id == eBtnType.CMD_EXP.ordinal())
            _omme.notifyMUDEvent(new OMUD_MUDEvent_GetDataBlock(OMUD_MMUD_DataBlock.eBlockType.EXP));
        else if (btn_id == eBtnType.CMD_INV.ordinal())
            _omme.notifyMUDEvent(new OMUD_MUDEvent_GetDataBlock(OMUD_MMUD_DataBlock.eBlockType.INV));
        else if (btn_id == eBtnType.CMD_STATS.ordinal())
            _omme.notifyMUDEvent(new OMUD_MUDEvent_GetDataBlock(OMUD_MMUD_DataBlock.eBlockType.STATS));
        else if (btn_id == eBtnType.CMD_PARTY.ordinal())
            _omme.notifyMUDEvent(new OMUD_MUDEvent_GetDataBlock(OMUD_MMUD_DataBlock.eBlockType.PARTY));
        else if (btn_id == eBtnType.CMD_WHO.ordinal())
            _omme.notifyMUDEvent(new OMUD_MUDEvent_GetDataBlock(OMUD_MMUD_DataBlock.eBlockType.WHO));
        _omge.notifyGUIEvent(new OMUD_GUIEvent_FocusTelnetInput());
    }
}
