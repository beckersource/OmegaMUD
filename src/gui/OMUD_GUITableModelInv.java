import java.util.List;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class OMUD_GUITableModelInv extends AbstractTableModel {
    public static final String[] TABLE_COL_NAMES = {
        "?",
        "QTY",
        "SLOT",
        "NAME"
    };

    private final List<OMUD_MMUD_DataItem> _arrlItems = new ArrayList<OMUD_MMUD_DataItem>();

    public void addItem(OMUD_MMUD_DataItem item){
        addItem(item, _arrlItems.size());
    }

    public void addItem(OMUD_MMUD_DataItem item, int index){
        _arrlItems.add(index, item);
        fireTableRowsInserted(index, index);
    }

    public void clear(){for (int i = _arrlItems.size() - 1; i >= 0; --i) removeItem(_arrlItems.get(i));}
    public void removeItem(OMUD_MMUD_DataItem item){
        int index = _arrlItems.indexOf(item);
        _arrlItems.remove(index);
        fireTableRowsDeleted(index, index);
    }

    public OMUD_MMUD_DataItem getItem(int index){
        return _arrlItems.get(index);
    }

    @Override
    public int getRowCount(){
        return _arrlItems.size();
    }

    @Override
    public int getColumnCount(){
        return TABLE_COL_NAMES.length;
    }

    @Override
    public String getColumnName(int col){
        if (col >= 0 && col < TABLE_COL_NAMES.length)
             return TABLE_COL_NAMES[col];
        else return "";
    }

    @Override
    public Class<?> getColumnClass(int col){
             if (col == 0) return Character.class;
        else if (col == 1) return Integer.class;
        else if (col == 2) return String.class;
        else if (col == 3) return String.class;
        else return Object.class;
    }

    @Override
    public Object getValueAt(int row, int col){

        if (col == 0){
                 if (_arrlItems.get(row).item_loc == OMUD_MMUD_DataItem.eItemLoc.KEY)
                 return 'K';
            else if (_arrlItems.get(row).item_loc != OMUD_MMUD_DataItem.eItemLoc.NONE)
                 return '*';
            else return ' ';
        } else if (col == 1)
            return _arrlItems.get(row).qty;
        else if (col == 2)
            return OMUD_MMUD_DataItem.EQUIP_SLOT_STRINGS[_arrlItems.get(row).item_loc.ordinal()];
        else if (col == 3)
            return _arrlItems.get(row).name;
        else return new Object();
    }

    @Override
    public boolean isCellEditable(int row, int column){
       return false;
    }
}
