import javax.swing.JTextArea;

public class OMUD_GUITextArea extends JTextArea{
    public OMUD_GUITextArea(boolean line_wrap){
        setEditable(false);
        setBackground(OMUD.CLR_BG_DARK_GREY);
        setForeground(OMUD.CLR_FG_WHITE);
        setCaretColor(OMUD.CLR_FG_WHITE);
        getCaret().setVisible(true);
        setFont(OMUD.getTerminalFont());
        setLineWrap(line_wrap);
    }
}
