import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("unchecked") // ignore "unchecked" warning for Java7+ ComboBoxes (they requie a <> type now)
public class OMUD_GUIFCharInfo extends OMUD_GUIFChar {
    private enum eCBO{
        CHAT,
        ROOM,
        COMBAT,
        PARTY,
        INV,
        STATS,
        SHOP,
        SPELLS,
        WHO,
        TOP,
        WELCOME,
        CMDS,
        DBG_MUD,
        DBG_TERM
    }

    private static final String[] CBO_STRINGS = {
        "Chat",
        "Room",
        "Combat",
        "Party",
        "Inv",
        "Stats",
        "Shop",
        "Spells",
        "Who",
        "Top",
        "Welcome",
        "Cmds",
        "DEBUG_MUD",
        "DEBUG_Term",
    };

    private OMUD_GUITextArea[] CBO_TEXTAREA = {
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    };

    private SimpleDateFormat    _sdf =          null;
    private OMUD_IMUDEvents     _omme =         null;
    private OMUD_GUIComboBox    _cboInfo =      null;
    private eCBO                _cbo_prev =     eCBO.DBG_TERM;
    private OMUD_GUITextArea    _txtChat =      null;
    private OMUD_GUITextArea    _txtRoom =      null;
    private OMUD_GUITextArea    _txtCombat =    null;
    private OMUD_GUITextArea    _txtParty =     null;
    private OMUD_GUITextArea    _txtInv =       null;
    private OMUD_GUITextArea    _txtStats =     null;
    private OMUD_GUITextArea    _txtShop =      null;
    private OMUD_GUITextArea    _txtSpells =    null;
    private OMUD_GUITextArea    _txtWho =       null;
    private OMUD_GUITextArea    _txtTop =       null;
    private OMUD_GUITextArea    _txtWelcome =   null;
    private OMUD_GUITextArea    _txtCmds =      null;
    private OMUD_GUITextArea    _txtDbgMUD =    null;
    private OMUD_GUITextArea    _txtDbgTerm =   null;
    private OMUD_GUITextArea    _text =         null;
    private JScrollPane         _scroll =       null;
    private JPanel              _panel =        null;
    private static final int FRAME_MIN_WIDTH  = 710;
    private static final int FRAME_MIN_HEIGHT = 550;

    public OMUD_GUIFCharInfo(OMUD_IMUDEvents omme){
        super("Char Info");
        setMinimumSize(new Dimension(FRAME_MIN_WIDTH, FRAME_MIN_HEIGHT));

        _omme = omme;
        _sdf  = new SimpleDateFormat("yyyy-MM-dd_kk-mm-ss");

        // text areas...
        CBO_TEXTAREA[eCBO.CHAT.ordinal()] =     _txtChat =      new OMUD_GUITextArea(false);
        CBO_TEXTAREA[eCBO.ROOM.ordinal()] =     _txtRoom =      new OMUD_GUITextArea(true);
        CBO_TEXTAREA[eCBO.COMBAT.ordinal()] =   _txtCombat =    new OMUD_GUITextArea(false);
        CBO_TEXTAREA[eCBO.PARTY.ordinal()] =    _txtParty =     new OMUD_GUITextArea(true);
        CBO_TEXTAREA[eCBO.INV.ordinal()] =      _txtInv =       new OMUD_GUITextArea(true);
        CBO_TEXTAREA[eCBO.STATS.ordinal()] =    _txtStats =     new OMUD_GUITextArea(false);
        CBO_TEXTAREA[eCBO.SHOP.ordinal()] =     _txtShop =      new OMUD_GUITextArea(false);
        CBO_TEXTAREA[eCBO.SPELLS.ordinal()] =   _txtSpells =    new OMUD_GUITextArea(false);
        CBO_TEXTAREA[eCBO.WHO.ordinal()] =      _txtWho =       new OMUD_GUITextArea(false);
        CBO_TEXTAREA[eCBO.TOP.ordinal()] =      _txtTop =       new OMUD_GUITextArea(false);
        CBO_TEXTAREA[eCBO.WELCOME.ordinal()] =  _txtWelcome =   new OMUD_GUITextArea(false);
        CBO_TEXTAREA[eCBO.CMDS.ordinal()] =     _txtCmds =      new OMUD_GUITextArea(false);
        CBO_TEXTAREA[eCBO.DBG_MUD.ordinal()] =  _txtDbgMUD =    new OMUD_GUITextArea(false);
        CBO_TEXTAREA[eCBO.DBG_TERM.ordinal()] = _txtDbgTerm =   new OMUD_GUITextArea(false);
        _text = _txtDbgTerm;

        // combo...
        _cboInfo = new OMUD_GUIComboBox(OMUD.CLR_BG_DARK_BUTTON);
        _cboInfo.addItem(CBO_STRINGS[eCBO.CHAT.ordinal()]);
        _cboInfo.addItem(CBO_STRINGS[eCBO.ROOM.ordinal()]);
        _cboInfo.addItem(CBO_STRINGS[eCBO.COMBAT.ordinal()]);
        _cboInfo.addItem(CBO_STRINGS[eCBO.PARTY.ordinal()]);
        _cboInfo.addItem(CBO_STRINGS[eCBO.INV.ordinal()]);
        _cboInfo.addItem(CBO_STRINGS[eCBO.STATS.ordinal()]);
        _cboInfo.addItem(CBO_STRINGS[eCBO.SHOP.ordinal()]);
        _cboInfo.addItem(CBO_STRINGS[eCBO.SPELLS.ordinal()]);
        _cboInfo.addItem(CBO_STRINGS[eCBO.WHO.ordinal()]);
        _cboInfo.addItem(CBO_STRINGS[eCBO.TOP.ordinal()]);
        _cboInfo.addItem(CBO_STRINGS[eCBO.WELCOME.ordinal()]);
        _cboInfo.addItem(CBO_STRINGS[eCBO.CMDS.ordinal()]);
        _cboInfo.addItem(CBO_STRINGS[eCBO.DBG_MUD.ordinal()]);
        _cboInfo.addItem(CBO_STRINGS[eCBO.DBG_TERM.ordinal()]);
        _cboInfo.addActionListener(new AL_CBO());

        add((_panel = new JPanel()));
        layoutInfo(null);
        pack();

        // start on terminal...
        setCBO(eCBO.DBG_TERM, true);
    }

    private void layoutInfo(OMUD_GUITextArea txtInfoOld){
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gblc = new GridBagConstraints();

        // if this is an update, remove the scroll/text and replace with the requested...
        if (_scroll != null){
            _panel.remove(_scroll);
            _panel.revalidate();
            _panel.repaint();
        }

        gblc.weightx =      0.0;
        gblc.weighty =      0.0;
        gblc.gridwidth =    1;
        gblc.gridheight =   1;
        gblc.fill =         GridBagConstraints.HORIZONTAL;
        gbl.setConstraints(_cboChars, gblc);
        gblc.gridwidth =    GridBagConstraints.REMAINDER;
        gbl.setConstraints(_cboInfo,  gblc);

        _scroll = new JScrollPane(_text);
        gblc.weightx =      1.0;
        gblc.weighty =      1.0;
        gblc.gridwidth =    GridBagConstraints.REMAINDER;
        gblc.fill =         GridBagConstraints.BOTH;
        gbl.setConstraints(_scroll, gblc);

        _panel.setLayout(gbl);

        // only add the combo from the constructor...
        if (txtInfoOld == null){
            _panel.add(_cboChars);
            _panel.add(_cboInfo);
        }
        _panel.add(_scroll);
    }

    private void updateCBO(eCBO index){
        OMUD_GUITextArea txtInfoOld = _text;
        _text = CBO_TEXTAREA[index.ordinal()];
        layoutInfo(txtInfoOld);
    }

    // --------------
    // GUI Events
    // --------------
    private class AL_CBO implements ActionListener{
        public void actionPerformed(ActionEvent event) {
            eCBO eCBOval = eCBO.values()[_cboInfo.getSelectedIndex()];
            if (eCBOval != _cbo_prev){
                _cbo_prev = eCBOval;

                     if (eCBOval == eCBO.ROOM)
                    _omme.notifyMUDEvent(new OMUD_MUDEvent_GetDataBlock(OMUD_MMUD_DataBlock.eBlockType.ROOM));
                else if (eCBOval == eCBO.PARTY)
                    _omme.notifyMUDEvent(new OMUD_MUDEvent_GetDataBlock(OMUD_MMUD_DataBlock.eBlockType.PARTY));
                else if (eCBOval == eCBO.INV)
                    _omme.notifyMUDEvent(new OMUD_MUDEvent_GetDataBlock(OMUD_MMUD_DataBlock.eBlockType.INV));
                else if (eCBOval == eCBO.STATS)
                    _omme.notifyMUDEvent(new OMUD_MUDEvent_GetDataBlock(OMUD_MMUD_DataBlock.eBlockType.STATS));
                else if (eCBOval == eCBO.SHOP)
                    _omme.notifyMUDEvent(new OMUD_MUDEvent_GetDataBlock(OMUD_MMUD_DataBlock.eBlockType.SHOP));
                else if (eCBOval == eCBO.SPELLS)
                    _omme.notifyMUDEvent(new OMUD_MUDEvent_GetDataBlock(OMUD_MMUD_DataBlock.eBlockType.SPELLS));
                else if (eCBOval == eCBO.WHO)
                    _omme.notifyMUDEvent(new OMUD_MUDEvent_GetDataBlock(OMUD_MMUD_DataBlock.eBlockType.WHO));

                updateCBO(eCBOval);
            }
        }
    }

    private boolean setCBO(eCBO index, boolean force){
        if (force || (_cbo_prev != eCBO.DBG_MUD && _cbo_prev != eCBO.DBG_TERM)){
            _cboInfo.setSelectedIndex((_cbo_prev = index).ordinal());
            updateCBO(index);
            return true;
        } else return false;
    }

    // --------------
    // Telnet Events
    // --------------
    public void eventTelnet_DataReady(OMUD_TelnetEvent_DataReady event){
        if (isVisible() && _cboInfo.getSelectedIndex() == eCBO.DBG_TERM.ordinal()){
            // terminal debug text...
            int caret_pos = 0;
            String strRow;
            Boolean is_buffer_row = false;
            StringBuilder sbTermDebug = new StringBuilder();
            for (int i = 0; i < OMUD.TERMINAL_ROWS; ++i){
                strRow = event.omb.getDebugTerminal().getRowText(i);
                sbTermDebug.append(String.format("%02d]", i + 1) + (strRow.length() > 0 ? " " : "_"));

                is_buffer_row = i == event.omb.getDebugTerminal().getRowNum();
                if (is_buffer_row)
                    caret_pos = sbTermDebug.length();
                if (strRow.length() > 0){
                    sbTermDebug.append(strRow);
                    if (is_buffer_row)
                        caret_pos += event.omb.getDebugTerminal().getColNum();
                }

                sbTermDebug.append("\n");
            }
            sbTermDebug.append("-------------------------------------------------------------------------------------\n");
            sbTermDebug.append("    12345678901234567890123456789012345678901234567890123456789012345678901234567890\n");
            sbTermDebug.append("-------------------------------------------------------------------------------------\n");
            sbTermDebug.append("BLC/BTL/BSZ: " + event.omb.getLineCount() + ", " + event.omb.getTopLeftPos() + ", " + event.omb.getText().length() + "\n");
            sbTermDebug.append("ROW/COL: " + (event.omb.getDebugTerminal().getRowNum() + 1) + ", " + (event.omb.getDebugTerminal().getColNum() + 1) + "\n");
            _txtDbgTerm.setText(sbTermDebug.toString());
            _txtDbgTerm.setCaretPosition(caret_pos);
        }
    }

    // --------------
    // MUD Events
    // --------------
    public void eventMUD_UserCmd(OMUD_MUDEvent_UserCmd event){
        _txtCmds.setText(_txtCmds.getText() + _sdf.format(new Date()) + ": " + event.cmd + "\n");
    }

    public void eventMUD_DebugText(OMUD_MUDEvent_DebugText event){
        for (int i = 0; i < event.arrlText.size(); ++i)
            _txtDbgMUD.setText(_txtDbgMUD.getText() + _sdf.format(new Date()) + ": " + event.arrlText.get(i) + "\n");
        _txtDbgMUD.setCaretPosition(_txtDbgMUD.getText().length());
    }

    public void eventMUD_Welcome(OMUD_MUDEvent_Welcome event){
        setCBO(eCBO.WELCOME, true);
        _txtWelcome.setText(event.text);
    }

    public void eventMUD_Room(OMUD_MUDEvent_Room event){
        if (!setCBO(eCBO.ROOM, false))
            return;

        StringBuilder sb = new StringBuilder();
        sb.append("[MegaID]: "      + event.dataRoom.megaID);
        sb.append("\n[RoomName]: "  + event.dataRoom.name + " (" + OMUD_MMUD_DataBlock_Room.ROOM_LIGHT_STRINGS[event.dataRoom.light.ordinal()] + ")");

        sb.append("\n\n=========> [RoomCoins] (*=HIDDEN)\n");
        sb.append("  R: "   + event.dataRoom.coins.runic          + ", ");
        sb.append("P: "     + event.dataRoom.coins.plat           + ", ");
        sb.append("G: "     + event.dataRoom.coins.gold           + ", ");
        sb.append("S: "     + event.dataRoom.coins.silver         + ", ");
        sb.append("C: "     + event.dataRoom.coins.copper         + "\n");
        sb.append("* R: "   + event.dataRoom.coins_hidden.runic   + ", ");
        sb.append("P: "     + event.dataRoom.coins_hidden.plat    + ", ");
        sb.append("G: "     + event.dataRoom.coins_hidden.gold    + ", ");
        sb.append("S: "     + event.dataRoom.coins_hidden.silver  + ", ");
        sb.append("C: "     + event.dataRoom.coins_hidden.copper  + "\n");

        sb.append("\n=========> [RoomItems] (*=HIDDEN)\n");
        for (int i = 0; i < event.dataRoom.arrlItems.size(); ++i)
            sb.append("  (" + event.dataRoom.arrlItems.get(i).qty + ") " + event.dataRoom.arrlItems.get(i).name + "\n");
        for (int i = 0; i < event.dataRoom.arrlItemsHidden.size(); ++i)
            sb.append("* (" + event.dataRoom.arrlItemsHidden.get(i).qty + ") " + event.dataRoom.arrlItemsHidden.get(i).name + "\n");

        sb.append("\n=========> [RoomUnits]\n");
        for (int i = 0; i < event.dataRoom.arrlUnits.size(); ++i)
            sb.append(event.dataRoom.arrlUnits.get(i).name + "\n");

        sb.append("\n=========> [RoomExits]\n");
        for (int i = 0; i < event.dataRoom.arrlExits.size(); ++i)
            sb.append(OMUD_MMUD_DataExit.EXIT_DIR_STRINGS[event.dataRoom.arrlExits.get(i).eDir.ordinal()] + "\n");

        sb.append("\n=========> [RoomDesc]\n");
        sb.append(event.dataRoom.desc);

        _txtRoom.setText(sb.toString());
        _txtRoom.setCaretPosition(0);
    }

    public void eventMUD_Inv(OMUD_MUDEvent_Inv event){
        if (!setCBO(eCBO.INV, false))
            return;

        StringBuilder sb = new StringBuilder();

        // money + enc...
        sb.append("=========> [InvMoney]:");
        sb.append("  R: " + event.dataInv.coins.runic);
        sb.append(", P: " + event.dataInv.coins.plat);
        sb.append(", G: " + event.dataInv.coins.gold);
        sb.append(", S: " + event.dataInv.coins.silver);
        sb.append(", C: " + event.dataInv.coins.copper);
        sb.append(", W: " + event.dataInv.wealth);
        sb.append("\n=========> [InvEnc]: (" + event.dataInv.enc_level + ") " + event.dataInv.enc_cur + "/" + event.dataInv.enc_max + String.format(" [%.0f", ((float) event.dataInv.enc_cur / event.dataInv.enc_max) * 100) + "%]");

        sb.append("\n\n=========> [InvItemsEQ]\n");
        for (int i = 0; i < event.dataInv.arrlEQ.size(); ++i){
            String strSlot = OMUD_MMUD_DataItem.EQUIP_SLOT_STRINGS[event.dataInv.arrlEQ.get(i).item_loc.ordinal()].toUpperCase();
            sb.append(strSlot);
            sb.append(OMUD.getFillString(" ", 10 - strSlot.length()));
            sb.append(" " + event.dataInv.arrlEQ.get(i).name + "\n");
        }

        sb.append("\n=========> [InvItems] (*=EQ)\n");
        for (int i = 0; i < event.dataInv.arrlItems.size(); ++i){
            if (event.dataInv.arrlItems.get(i).item_loc != OMUD_MMUD_DataItem.eItemLoc.NONE)
                 sb.append("* ");
            else sb.append("  ");
            sb.append("(" + event.dataInv.arrlItems.get(i).qty + ") " + event.dataInv.arrlItems.get(i).name + "\n");
        }

        sb.append("\n=========> [InvKeys]\n");
        for (int i = 0; i < event.dataInv.arrlKeys.size(); ++i)
            sb.append(event.dataInv.arrlKeys.get(i).name + "\n");

        _txtInv.setText(sb.toString());
        _txtInv.setCaretPosition(0);
    }

    public void eventMUD_Stats(OMUD_MUDEvent_Stats event){
        if (!setCBO(eCBO.STATS, false))
            return;

        StringBuilder sb = new StringBuilder();
        sb.append("[NameFirst]: "   + event.dataStats.name_first);
        sb.append("\n[NameLast]: "  + event.dataStats.name_last);
        sb.append("\n[Race]: "      + event.dataStats.stats_race);
        sb.append("\n[Exp]: "       + event.dataExp.cur_perc + "% [" + event.dataExp.cur_total + "/" + event.dataExp.next_total + "]");
        sb.append("\n[Class]: "     + event.dataStats.stats_class);
        sb.append("\n[Level]: "     + event.dataStats.level);
        sb.append("\n[Lives]: "     + event.dataStats.lives);
        sb.append("\n[CP]: "        + event.dataStats.cp);
        sb.append("\n[HP]: "        + (event.dataStatline.hp_mod ? "* " : "  ") + event.dataStatline.hp_cur + "/" + event.dataStatline.hp_max);
        if (event.dataStatline.ma_str.length() > 0)
            sb.append("\n[" + event.dataStatline.ma_str + "]: " + (event.dataStatline.ma_mod ? "* " : "  ") + event.dataStatline.ma_cur + "/" + event.dataStatline.ma_max);
        sb.append("\n--------------------");
        sb.append("\n[Str]: "       + (event.dataStats.str_mod    ? "* " : "  ") + event.dataStats.str);
        sb.append("\n[Int]: "       + (event.dataStats.intel_mod  ? "* " : "  ") + event.dataStats.intel);
        sb.append("\n[Wil]: "       + (event.dataStats.wil_mod    ? "* " : "  ") + event.dataStats.wil);
        sb.append("\n[Agi]: "       + (event.dataStats.agi_mod    ? "* " : "  ") + event.dataStats.agi);
        sb.append("\n[Hea]: "       + (event.dataStats.hea_mod    ? "* " : "  ") + event.dataStats.hea);
        sb.append("\n[Cha]: "       + (event.dataStats.cha_mod    ? "* " : "  ") + event.dataStats.cha);
        sb.append("\n--------------------");
        sb.append("\n[AC]: "        + event.dataStats.ac_ac + "/" + event.dataStats.ac_accy);
        sb.append("\n[SC]: "        + event.dataStats.sc);
        sb.append("\n[Perc]: "      + event.dataStats.perc);
        sb.append("\n[Stealth]: "   + event.dataStats.stealth);
        sb.append("\n[Thiev]: "     + event.dataStats.thievery);
        sb.append("\n[Traps]: "     + event.dataStats.traps);
        sb.append("\n[Pick]: "      + event.dataStats.pick);
        sb.append("\n[Track]: "     + event.dataStats.track);
        sb.append("\n[MA]: "        + event.dataStats.ma);
        sb.append("\n[MR]: "        + event.dataStats.mr);
        sb.append("\n------[BUFFS]-------");
        for (int i = 0; i < event.dataStats.buffs.size(); ++i)
            sb.append(String.format("\n%02d: ", i + 1) + event.dataStats.buffs.get(i).active_text);

        _txtStats.setText(sb.toString());
        _txtStats.setCaretPosition(0);
    }

    public void eventMUD_Shop(OMUD_MUDEvent_Shop event){
        if (!setCBO(eCBO.SHOP, false))
            return;

        StringBuilder sb = new StringBuilder();
        sb.append("[MegaID]: "      + event.dataShop.megaID);
        sb.append("\n[RoomName]: "  + event.dataShop.roomName);
        sb.append("\n\n=========> [Qty] Name: Price (*=NO USE)\n");
        for (int i = 0; i < event.dataShop.shop_items.size(); ++i){
            sb.append("[");
            if (event.dataShop.shop_items.get(i).qty < 10)
                sb.append(" ");
            sb.append(event.dataShop.shop_items.get(i).qty + "]: ");
            sb.append(event.dataShop.shop_items.get(i).can_use ? "  " : "* ");
            sb.append(event.dataShop.shop_items.get(i).name);
            sb.append(OMUD.getFillString(" ", 30 - event.dataShop.shop_items.get(i).name.length()));

            sb.append(event.dataShop.shop_items.get(i).getPriceString() + "\n");
        }

        _txtShop.setText(sb.toString());
        _txtShop.setCaretPosition(0);
    }

    public void eventMUD_Spells(OMUD_MUDEvent_Spells event){
        if (!setCBO(eCBO.SPELLS, false))
            return;

        StringBuilder sb = new StringBuilder();
        sb.append("=========> Short (Lvl, Cost) Full\n");
        for (int i = 0; i < event.dataSpells.arrlSpells.size(); ++i){
            sb.append(event.dataSpells.arrlSpells.get(i).name_short + " (" + event.dataSpells.arrlSpells.get(i).level + ", " + event.dataSpells.arrlSpells.get(i).cost + ") ");
            sb.append(event.dataSpells.arrlSpells.get(i).name_long + "\n");
        }

        _txtSpells.setText(sb.toString());
        _txtSpells.setCaretPosition(_txtSpells.getText().length());
    }

    public void eventMUD_Who(OMUD_MUDEvent_Who event){
        if (!setCBO(eCBO.WHO, false))
            return;

        StringBuilder sb = new StringBuilder();
        sb.append("=========> FName LName (Align Title of Guild)\n");
        for (int i = 0; i < event.dataWho.chars.size(); ++i){
            // name...
            int fill_len = 24 - event.dataWho.chars.get(i).name_first.length();
            sb.append(event.dataWho.chars.get(i).name_first);
            if (event.dataWho.chars.get(i).name_last.length() > 0){
                fill_len -= (event.dataWho.chars.get(i).name_last.length() + 1); // +1 for inner space delim
                sb.append(" " + event.dataWho.chars.get(i).name_last);
            }
            sb.append(OMUD.getFillString(" ", fill_len));

            // align...
            String strAlign = OMUD_MMUD_DataBlock_Who.ALIGNMENT_STRINGS[event.dataWho.chars.get(i).alignment.ordinal()];
            sb.append(" (" + strAlign);
            sb.append(OMUD.getFillString(" ", 8 - strAlign.length()));

            // title + guild...
            sb.append(" " + event.dataWho.chars.get(i).title);
            if (event.dataWho.chars.get(i).guild.length() > 0){
                sb.append(OMUD.getFillString(" ", 16 - event.dataWho.chars.get(i).title.length()));
                sb.append(" of " + event.dataWho.chars.get(i).guild);
            }
            sb.append(")\n");
        }

        _txtWho.setText(sb.toString());
        _txtWho.setCaretPosition(0);
    }

    public void eventMUD_Combat(OMUD_MUDEvent_Combat event){
        if (!setCBO(eCBO.COMBAT, false))
            return;

        StringBuilder sb = new StringBuilder("=========> [TYPE] Attacker <V> Target (ACTION) (WEAP)\n");
        if (_txtCombat.getText().length() > 0)
            sb.append(_txtCombat.getText().substring(sb.length(), _txtCombat.getText().length()));

        // combat lines added in reverse, so loop reverse...
        for (int i = 0; i < event.dataCombat.arrlLines.size(); ++i){
            // H/M/D/F...
            StringBuilder sbHit = new StringBuilder(OMUD_MMUD_DataBlock_Combat.HIT_TYPE_STRINGS[event.dataCombat.arrlLines.get(i).type.ordinal()]);
            if (event.dataCombat.arrlLines.get(i).type == OMUD_MMUD_DataBlock_Combat.eLineType.HIT)
                sbHit.append(": " + event.dataCombat.arrlLines.get(i).tgt_dmg);
            StringBuilder sbLine = new StringBuilder("[");
            sbLine.append(OMUD.getFillString(" ", 8 - sbHit.length()));
            sbLine.append(sbHit + "] ");
            // others...
            if (event.dataCombat.arrlLines.get(i).unit.name.length() > 0){
                sbLine.append(event.dataCombat.arrlLines.get(i).unit.name);
                if (event.dataCombat.new_round){
                    event.dataCombat.new_round = false;
                    sbLine.insert(0, "\n____NEW_ROUND____\n");
                }
            }
            if (event.dataCombat.arrlLines.get(i).tgt_name.length() > 0)
                sbLine.append(" <V> " + event.dataCombat.arrlLines.get(i).tgt_name);
            if (event.dataCombat.arrlLines.get(i).unit_action.length() > 0)
                sbLine.append(" (" + event.dataCombat.arrlLines.get(i).unit_action + ")");
            if (event.dataCombat.arrlLines.get(i).tgt_weapon.length() > 0)
                sbLine.append(" (" + event.dataCombat.arrlLines.get(i).tgt_weapon + ")");
            sb.append(sbLine + "\n");
        }

        _txtCombat.setText(sb.toString());
        _txtCombat.setCaretPosition(_txtCombat.getText().length());
    }

    public void eventMUD_Exp(OMUD_MUDEvent_Exp event){
        if (!setCBO(eCBO.COMBAT, false))
            return;

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < event.dataExp.arrlCombatExp.size(); ++i){
            sb.append("DeathExp: " +   event.dataExp.arrlCombatExp.get(i).exp +   "\n");
            sb.append("DeathDesc: " +  event.dataExp.arrlCombatExp.get(i).desc +  "\n");
            sb.append("DeathMoney: " + event.dataExp.arrlCombatExp.get(i).money + "\n");
        }

        _txtCombat.setText(_txtCombat.getText() + sb.toString());
        _txtCombat.setCaretPosition(_txtCombat.getText().length());
    }

    public void eventMUD_Party(OMUD_MUDEvent_Party event){
        if (!setCBO(eCBO.PARTY, false))
            return;

        StringBuilder sb = new StringBuilder();

        sb.append("=========> (*) Name (Class) [MA%] [HP%] RANK (*=LEADER)\n");
        for (int i = 0; i < event.dataParty.arrlMembers.size(); ++i){
            // leader check...
            if (event.dataParty.leader_name.equals(event.dataParty.arrlMembers.get(i).name_first))
                 sb.append("* ");
            else sb.append("  ");

            // first name...
            sb.append(event.dataParty.arrlMembers.get(i).name_first);
            sb.append(OMUD.getFillString(" ", OMUD_MMUD_DataBlock.NAME_FIRST_MAXLEN - event.dataParty.arrlMembers.get(i).name_first.length()));

            // last name...
            sb.append(" " + event.dataParty.arrlMembers.get(i).name_last);
            sb.append(OMUD.getFillString(" ", OMUD_MMUD_DataBlock.NAME_LAST_MAXLEN - event.dataParty.arrlMembers.get(i).name_last.length()));

            // class...
            sb.append(" (" + event.dataParty.arrlMembers.get(i).class_name + ")");
            sb.append(OMUD.getFillString(" ", OMUD_MMUD_DataBlock.CLASS_NAME_MAXLEN - event.dataParty.arrlMembers.get(i).class_name.length()));

            // check if actually accepted invite...
            if (event.dataParty.arrlMembers.get(i).accepted){
                // mana...
                sb.append(" " + "[MA: ");
                int ma_perc = 0;
                //if (event.dataParty.arrlMembers.get(i).ma_cur > 0)
                //    ma_perc = (int) ((float) event.dataParty.arrlMembers.get(i).ma_cur / event.dataParty.arrlMembers.get(i).ma_max) * 100;
                ma_perc = event.dataParty.arrlMembers.get(i).ma_perc;
                int fill_len = 0;
                if (ma_perc < 10)
                    fill_len = 2;
                else if (ma_perc < 100)
                    fill_len = 1;
                sb.append(OMUD.getFillString(" ", fill_len));

                // hp...
                sb.append(ma_perc + "%] [HP: ");
                int hp_perc = 0;
                //if (event.dataParty.arrlMembers.get(i).hp_cur > 0)
                //    hp_perc = (int) ((float) event.dataParty.arrlMembers.get(i).hp_cur / event.dataParty.arrlMembers.get(i).hp_max) * 100;
                hp_perc = event.dataParty.arrlMembers.get(i).hp_perc;
                fill_len = 0;
                if (hp_perc < 10)
                    fill_len = 2;
                else if (hp_perc < 100)
                    fill_len = 1;
                sb.append(OMUD.getFillString(" ", fill_len));

                // rank...
                sb.append(event.dataParty.arrlMembers.get(i).hp_perc + "%] " + OMUD_MMUD_DataBlock_Party.RANK_STRINGS[event.dataParty.arrlMembers.get(i).rank.ordinal()]);
            } else {
                sb.append(" [INVITED]");
            }
            sb.append("\n");
        }

        _txtParty.setText(sb.toString());
        _txtParty.setCaretPosition(0);
    }

    public void eventMUD_Chat(OMUD_MUDEvent_Chat event){
        if (!setCBO(eCBO.CHAT, false))
            return;

        StringBuilder sb = new StringBuilder("=========> [TIME_TYPE] Name: (?TGT) MSG\n");
        if (_txtChat.getText().length() > 0)
            sb.append(_txtChat.getText().substring(sb.length(), _txtChat.getText().length()));

        for (int i = 0; i < event.dataChat.arrlChats.size(); ++i){
            sb.append("[" + _sdf.format(new Date(event.dataChat.arrlChats.get(i).unix_time * 1000L)) + "_");
            sb.append(OMUD_MMUD_DataBlock_Chat.CHAT_TYPE_STRINGS[event.dataChat.arrlChats.get(i).type.ordinal()] + "] ");
            sb.append(event.dataChat.arrlChats.get(i).name_author);

            // check for adjacent yell...
            if (event.dataChat.arrlChats.get(i).type == OMUD_MMUD_DataBlock_Chat.eChatType.YELL_ADJ)
                sb.append(OMUD_MMUD_DataExit.EXIT_DIR_SHORTU_STRINGS[event.dataChat.arrlChats.get(i).yell_dir.ordinal()]);

            // check if for a specific target...
            if (event.dataChat.arrlChats.get(i).name_target.length() > 0 &&
                (event.dataChat.arrlChats.get(i).type == OMUD_MMUD_DataBlock_Chat.eChatType.TELEPATH ||
                 event.dataChat.arrlChats.get(i).type == OMUD_MMUD_DataBlock_Chat.eChatType.SAY_DIR))
                sb.append(OMUD_MMUD_DataBlock_Chat.CHAT_CMD_STRINGS[event.dataChat.arrlChats.get(i).type.ordinal()] + event.dataChat.arrlChats.get(i).name_target);

            sb.append(": " + event.dataChat.arrlChats.get(i).message + "\n");
        }

        _txtChat.setText(sb.toString());
        _txtChat.setCaretPosition(_txtChat.getText().length());
    }

    public void eventMUD_Top(OMUD_MUDEvent_Top event){
        if (!setCBO(eCBO.TOP, false))
            return;

        StringBuilder sb = new StringBuilder("=========> ");

        if (event.dataTop.type == OMUD_MMUD_DataBlock_Top.eTopType.PLAYERS_BY_ALL)
            sb.append("Top Players: All [R/N/C/G/EXP]\n");
        else if (event.dataTop.type == OMUD_MMUD_DataBlock_Top.eTopType.PLAYERS_BY_CLASS)
            sb.append("Top Players: Class [R/N/C/G/EXP]\n");
        else if (event.dataTop.type == OMUD_MMUD_DataBlock_Top.eTopType.GANGS)
            sb.append("Top Gangs [R/G/L/M/C/EXP]\n");

        for (int i = 0; i < event.dataTop.arrlRows.size(); ++i){
                 if (event.dataTop.arrlRows.get(i).rank < 10)  sb.append(OMUD.getFillString(" ", 2));
            else if (event.dataTop.arrlRows.get(i).rank < 100) sb.append(OMUD.getFillString(" ", 1));
            sb.append(event.dataTop.arrlRows.get(i).rank);

            if (event.dataTop.type == OMUD_MMUD_DataBlock_Top.eTopType.GANGS){
                sb.append(" " + event.dataTop.arrlRows.get(i).strGang);
                sb.append(OMUD.getFillString(" ", OMUD_MMUD_DataBlock.NAME_GANG_MAXLEN  - event.dataTop.arrlRows.get(i).strGang.length()));
                sb.append(" " + event.dataTop.arrlRows.get(i).strLeader);
                sb.append(OMUD.getFillString(" ", OMUD_MMUD_DataBlock.NAME_FIRST_MAXLEN - event.dataTop.arrlRows.get(i).strLeader.length()));
                sb.append(" " + event.dataTop.arrlRows.get(i).members);
                     if (event.dataTop.arrlRows.get(i).members < 10)   sb.append(OMUD.getFillString(" ", 3));
                else if (event.dataTop.arrlRows.get(i).members < 100)  sb.append(OMUD.getFillString(" ", 2));
                else if (event.dataTop.arrlRows.get(i).members < 1000) sb.append(OMUD.getFillString(" ", 1));
                sb.append(" (" + event.dataTop.arrlRows.get(i).strCreated + ")");
                sb.append(" " + event.dataTop.arrlRows.get(i).exp_base);
                if (event.dataTop.arrlRows.get(i).exp_times > 0){
                    sb.append(" (x" + event.dataTop.arrlRows.get(i).exp_times + " +");
                    sb.append(event.dataTop.arrlRows.get(i).exp_plus + ")");
                }
            } else {
                sb.append(" " + event.dataTop.arrlRows.get(i).strName);
                sb.append(OMUD.getFillString(" ", OMUD_MMUD_DataBlock.NAME_FULL_MAXLEN  - event.dataTop.arrlRows.get(i).strName.length()));
                sb.append(" (" + event.dataTop.arrlRows.get(i).strClass + ")");
                sb.append(OMUD.getFillString(" ", OMUD_MMUD_DataBlock.CLASS_NAME_MAXLEN - event.dataTop.arrlRows.get(i).strClass.length()));
                sb.append(" " + event.dataTop.arrlRows.get(i).strGang);
                sb.append(OMUD.getFillString(" ", OMUD_MMUD_DataBlock.NAME_GANG_MAXLEN  - event.dataTop.arrlRows.get(i).strGang.length()));
                sb.append(" " + event.dataTop.arrlRows.get(i).exp_base);
            }
            sb.append("\n");
        }

        _txtTop.setText(sb.toString());
        _txtTop.setCaretPosition(0);
    }
}
