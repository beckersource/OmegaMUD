import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.datatransfer.StringSelection;
import javax.swing.Timer;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.ImageIcon;
import javax.imageio.ImageIO;
import java.io.File;

public class OMUD_GUIFCharView extends OMUD_GUIFChar {
    private OMUD_Telnet         _omt =              null;
    private OMUD_IGUIEvents     _omge =             null;
    private OMUD_GUIScrollPane  _scroll =           null;
    private OMUD_GUITerminal    _term =             null;
    private OMUD_GUITelnetInput _telInput =         null;
    private OMUD_GUITextField   _lblBBSNetAddr =    null;
    private OMUD_GUITextField   _lblBBSNetPort =    null;
    private OMUD_GUITextField   _lblBBSLoc =        null;
    private OMUD_GUITextField   _lblMUDStatline =   null;
    private OMUD_GUITextField   _lblMUDExp =        null;
    private OMUD_GUITextField   _lblMUDRoomID =     null;
    private OMUD_GUITextField   _lblMUDAState =     null;
    private OMUD_GUITextField   _lblMUDCTimer =     null;
    private OMUD_GUITextField   _lblMUDIState =     null;
    private OMUD_GUITextField   _lblMUDBufSize =    null;
    private OMUD_GUITextField   _lblMUDSState =     null;
    private OMUD_GUITextField   _lblMUDCmd =        null;
    private OMUD_GUIButton      _btnMUDClearCmds =  null;
    private OMUD_GUIButton      _btnBBSConnect =    null;
    private JPanel              _pnlViewType =      null;
    private JPanel              _pnlMUDRow1 =       null;
    private JPanel              _pnlMUDRow2 =       null;
    private JPanel              _pnlBBS =           null;
    private JPanel              _pnlInput =         null;
    private JPanel              _panel =            null;
    private OMUD_GUIToggleButtonGroup   _tglgViews =  null;
    private OMUD_GUIToggleButtonGroup   _tglIMode =   null;
    private OMUD_GUIToolbarViewLeft     _tbarLeft =   null;
    private OMUD_GUIToolbarViewRight    _tbarRight =  null;
    private OMUD_GUIToolbarViewBottom   _tbarBottom = null;
    private static final int TERMINAL_WIDTH  =      675;
    private static final int TERMINAL_HEIGHT =      515;
    private static final int FRAME_MIN_WIDTH  =     690;
    private static final int FRAME_MIN_HEIGHT =     600;
    private static final int FRAME_MIN_PADDING =    10;
    private static final int LBL_HEIGHT =           24;

    public OMUD_GUIFCharView(OMUD_ITelnetEvents omte, OMUD_IMUDEvents omme, OMUD_IGUIEvents omge, OMUD_Telnet omt){
        super("Char View");

        _omt  = omt;
        _omge = omge;

        // toolbar...
        _tbarLeft =   new OMUD_GUIToolbarViewLeft(omt, omme, omge);
        _tbarRight =  new OMUD_GUIToolbarViewRight(omt, omme, omge);
        _tbarBottom = new OMUD_GUIToolbarViewBottom(omt, omme, omge);

        // views...
        _tglgViews = new OMUD_GUIToggleButtonGroup(new TGLL_Views(), false);
        _tglgViews.add("View Terminal",  "images/icons/icon-fview-tgl_terminal.png");
        _tglgViews.add("View Graphical", "images/icons/icon-fview-tgl_graphical.png");
        _tglgViews.layoutToggles();

        // terminal/scroll...
        _pnlViewType =  new JPanel();
        _scroll =       new OMUD_GUIScrollPane();
        _term =         new OMUD_GUITerminal(_scroll);
        _term.addMouseListener(new MA_TerminalFocus());
        _scroll.setViewportView(_term);
        _scroll.removeCaretListeners(_term);

        // status fields...
        _pnlMUDRow1 =       new JPanel();
        _pnlMUDRow2 =       new JPanel();
        _lblMUDStatline =   new OMUD_GUITextField("Statline: ?");
        _lblMUDExp =        new OMUD_GUITextField("XP: ?");
        _lblMUDRoomID =     new OMUD_GUITextField("RID: ?");
        _lblMUDAState =     new OMUD_GUITextField("Action State", false, false, true);
        _lblMUDCTimer =     new OMUD_GUITextField("0/0", false, false, true);
        _lblMUDIState =     new OMUD_GUITextField("Input State");
        _lblMUDBufSize =    new OMUD_GUITextField("BufSize");
        _lblMUDSState =     new OMUD_GUITextField("Script State");
        _lblMUDCmd =        new OMUD_GUITextField("CMD: ?");
        try { _btnMUDClearCmds =  new OMUD_GUIButton(new ImageIcon(ImageIO.read(new File("images/icons/icon-fview-clear_cmds.png"))), "Clear CMDs", 0, new BL_BtnClearCmds(), null);
        } catch (Exception e) {OMUD_Log.toConsoleError("Error adding view button: " + e.getMessage());}

        // panel: BBS + telnet...
        _pnlBBS =           new JPanel();
        _lblBBSNetAddr =    new OMUD_GUITextField("bbs.bearfather.net", true, false, false);
        _lblBBSNetPort =    new OMUD_GUITextField("23", true, false, false);
        _lblBBSLoc =        new OMUD_GUITextField();
        _btnBBSConnect =    new OMUD_GUIButton("Connect", "", 0, new BL_BtnConnect(), OMUD.CLR_BG_DARK_GREEN);
        updateBBSLocText(OMUD.eBBSLoc.OFFLINE);

        // input...
        _pnlInput = new JPanel();
        _telInput = new OMUD_GUITelnetInput(omte);
        _tglIMode = new OMUD_GUIToggleButtonGroup(new TGLL_IMode(), false);
        _tglIMode.add("Enable Single Mode", "");
        _tglIMode.layoutToggles();

        // sizes...
        _scroll.setPreferredSize(           new Dimension(TERMINAL_WIDTH, 0));
        _scroll.setMinimumSize(             new Dimension(TERMINAL_WIDTH, 0));
        _lblBBSNetAddr.setPreferredSize(    new Dimension(0,    LBL_HEIGHT));
        _lblBBSNetPort.setPreferredSize(    new Dimension(50,   LBL_HEIGHT));
        _lblBBSLoc.setPreferredSize(        new Dimension(125,  LBL_HEIGHT));
        _btnBBSConnect.setPreferredSize(    new Dimension(150,  LBL_HEIGHT));
        _lblMUDStatline.setPreferredSize(   new Dimension(0,    LBL_HEIGHT));
        _lblMUDExp.setPreferredSize(        new Dimension(0,    LBL_HEIGHT));
        _lblMUDRoomID.setPreferredSize(     new Dimension(100,  LBL_HEIGHT));
        _lblMUDAState.setPreferredSize(     new Dimension(75,   LBL_HEIGHT));
        _lblMUDCTimer.setPreferredSize(     new Dimension(30,   LBL_HEIGHT));
        _lblMUDIState.setPreferredSize(     new Dimension(150,  LBL_HEIGHT));
        _lblMUDBufSize.setPreferredSize(    new Dimension(50,   LBL_HEIGHT));
        _lblMUDSState.setPreferredSize(     new Dimension(150,  LBL_HEIGHT));
        _btnMUDClearCmds.setPreferredSize(  new Dimension(LBL_HEIGHT, LBL_HEIGHT));
        _lblMUDCmd.setPreferredSize(        new Dimension(0,    LBL_HEIGHT));
        _telInput.setPreferredSize(         new Dimension(0,    LBL_HEIGHT));
        _tglIMode.setPreferredSize(         new Dimension(150,  LBL_HEIGHT));

        add((_panel = new JPanel()));
        layoutDisplay();
        layoutMUDRow1();
        layoutMUDRow2();
        layoutBBS();
        layoutInput();
        layoutView();
        pack();

        // set min size after layout adjustments...
        setMinimumSize(new Dimension(
            (int) getContentPane().getSize().getWidth() + FRAME_MIN_PADDING,
            (int) (TERMINAL_HEIGHT + (LBL_HEIGHT * 3))));
    }

    private void layoutDisplay(){
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gblc = new GridBagConstraints();

        // view-type toggles...
        JPanel pnlTerminal = new JPanel();
        pnlTerminal.setBackground(OMUD.CLR_BG_DARK_GREY);
        gblc.weightx =      0.0;
        gblc.weighty =      0.0;
        gblc.gridwidth =    1;
        gblc.gridheight =   2;
        gblc.fill =         GridBagConstraints.HORIZONTAL;
        gbl.setConstraints(_cboChars, gblc);
        gblc.gridwidth =    GridBagConstraints.REMAINDER;
        gbl.setConstraints(_tglgViews, gblc);
        // terminal...
        gblc.weightx =      0.0;
        gblc.weighty =      1.0;
        gblc.fill =         GridBagConstraints.VERTICAL;
        gbl.setConstraints(_scroll, gblc);
        // bottom toolbar...
        pnlTerminal.setBackground(OMUD.CLR_BG_DARK_GREY);
        gblc.weightx =      1.0;
        gblc.weighty =      0.0;
        gblc.fill =         GridBagConstraints.HORIZONTAL;
        gbl.setConstraints(_tbarBottom, gblc);
        pnlTerminal.setLayout(gbl);
        pnlTerminal.add(_cboChars);
        pnlTerminal.add(_tglgViews);
        pnlTerminal.add(_scroll);
        pnlTerminal.add(_tbarBottom);

        // left toolbar...
        gblc.weightx =      0.0;
        gblc.weighty =      1.0;
        gblc.gridwidth =    2;
        gblc.gridheight =   0;
        gblc.fill =         GridBagConstraints.VERTICAL;
        gbl.setConstraints(_tbarLeft, gblc);
        // terminal column...
        gblc.weightx =      1.0;
        gblc.fill =         GridBagConstraints.BOTH;
        gbl.setConstraints(pnlTerminal, gblc);
        // right toolbar...
        gblc.weightx =      0.0;
        gblc.fill =         GridBagConstraints.VERTICAL;
        gbl.setConstraints(_tbarRight, gblc);

        _pnlViewType.setLayout(gbl);
        _pnlViewType.add(_tbarLeft);
        _pnlViewType.add(pnlTerminal);
        _pnlViewType.add(_tbarRight);
    }

    private void layoutMUDRow1(){
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gblc = new GridBagConstraints();

        gblc.weightx =      0.0;
        gblc.weighty =      0.0;
        gblc.gridwidth =    4;
        gblc.gridheight =   0;
        gblc.fill =         GridBagConstraints.HORIZONTAL;
        gbl.setConstraints(_lblBBSLoc,      gblc);
        gbl.setConstraints(_lblMUDAState,  gblc);
        gbl.setConstraints(_lblMUDCTimer,  gblc);
        gblc.weightx =      1.0;
        gbl.setConstraints(_lblMUDStatline, gblc);
        gblc.gridwidth =    GridBagConstraints.REMAINDER;
        gbl.setConstraints(_lblMUDExp,      gblc);

        _pnlMUDRow1.setLayout(gbl);
        _pnlMUDRow1.add(_lblBBSLoc);
        _pnlMUDRow1.add(_lblMUDAState);
        _pnlMUDRow1.add(_lblMUDCTimer);
        _pnlMUDRow1.add(_lblMUDStatline);
        _pnlMUDRow1.add(_lblMUDExp);
    }

    private void layoutMUDRow2(){
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gblc = new GridBagConstraints();

        gblc.weightx =      0.0;
        gblc.weighty =      0.0;
        gblc.gridwidth =    5;
        gblc.gridheight =   0;
        gblc.fill =         GridBagConstraints.HORIZONTAL;
        gbl.setConstraints(_lblMUDRoomID,    gblc);
        gbl.setConstraints(_lblMUDIState,    gblc);
        gbl.setConstraints(_lblMUDBufSize,   gblc);
        gbl.setConstraints(_lblMUDSState,    gblc);
        gbl.setConstraints(_btnMUDClearCmds, gblc);
        gblc.weightx =      1.0;
        gblc.gridwidth =    GridBagConstraints.REMAINDER;
        gbl.setConstraints(_lblMUDCmd,       gblc);

        _pnlMUDRow2.setLayout(gbl);
        _pnlMUDRow2.add(_lblMUDRoomID);
        _pnlMUDRow2.add(_lblMUDIState);
        _pnlMUDRow2.add(_lblMUDBufSize);
        _pnlMUDRow2.add(_lblMUDSState);
        _pnlMUDRow2.add(_btnMUDClearCmds);
        _pnlMUDRow2.add(_lblMUDCmd);
    }

    private void layoutBBS(){
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gblc = new GridBagConstraints();

        // address...
        gblc.weightx =      1.0;
        gblc.weighty =      0.0;
        gblc.gridwidth =    3;
        gblc.gridheight =   0;
        gblc.fill =         GridBagConstraints.HORIZONTAL;
        gbl.setConstraints(_lblBBSNetAddr,  gblc);
        // port + loc + connect...
        gblc.weightx =      0.0;
        gblc.fill =         GridBagConstraints.NONE;
        gbl.setConstraints(_lblBBSNetPort,  gblc);
        gbl.setConstraints(_btnBBSConnect,  gblc);

        _pnlBBS.setLayout(gbl);
        _pnlBBS.add(_lblBBSNetAddr);
        _pnlBBS.add(_lblBBSNetPort);
        _pnlBBS.add(_btnBBSConnect);
    }

    private void layoutInput(){
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gblc = new GridBagConstraints();

        // input...
        gblc.weightx =      1.0;
        gblc.weighty =      0.0;
        gblc.gridwidth =    1;
        gblc.gridheight =   0;
        gblc.fill =         GridBagConstraints.HORIZONTAL;
        gbl.setConstraints(_telInput, gblc);
        // input mode button...
        gblc.weightx =      0.0;
        gblc.fill =         GridBagConstraints.NONE;
        gbl.setConstraints(_tglIMode, gblc);

        _pnlInput.setLayout(gbl);
        _pnlInput.add(_telInput);
        _pnlInput.add(_tglIMode);
    }

    private void layoutView(){
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gblc = new GridBagConstraints();

        // main display...
        gblc.weightx =      1.0;
        gblc.weighty =      1.0;
        gblc.gridwidth =    0;
        gblc.gridheight =   4;
        gblc.fill =         GridBagConstraints.BOTH;
        gbl.setConstraints(_pnlViewType,    gblc);
        gblc.weightx =      1.0;
        gblc.weighty =      0.0;
        gblc.fill =         GridBagConstraints.HORIZONTAL;
        // bottom labels...
        gbl.setConstraints(_pnlMUDRow1, gblc);
        gbl.setConstraints(_pnlMUDRow2, gblc);
        gbl.setConstraints(_pnlBBS,     gblc);
        gblc.gridheight =   GridBagConstraints.REMAINDER;
        gbl.setConstraints(_pnlInput,   gblc);

        // terminal label panel...
        gblc.weightx =      1.0;
        gblc.weighty =      0.0;
        gblc.fill =         GridBagConstraints.HORIZONTAL;

        _panel.setLayout(gbl);
        _panel.add(_pnlViewType);
        _panel.add(_pnlMUDRow1);
        _panel.add(_pnlMUDRow2);
        _panel.add(_pnlBBS);
        _panel.add(_pnlInput);
    }

    private void updateBBSLocText(OMUD.eBBSLoc bbs_loc){
        boolean inside_mud = (bbs_loc == OMUD.eBBSLoc.MUD);

        _lblBBSLoc.setText("BBSLoc: " + OMUD.BBSLOC_STRINGS[bbs_loc.ordinal()]);
             if (bbs_loc == OMUD.eBBSLoc.OFFLINE)
             _lblBBSLoc.setBackground(OMUD.CLR_BG_DARK_RED);
        else if (inside_mud || bbs_loc == OMUD.eBBSLoc.MUD_EDITOR)
             _lblBBSLoc.setBackground(OMUD.CLR_BG_DARK_GREEN);
        else _lblBBSLoc.setBackground(OMUD.CLR_BG_DARK_YELLOW);

        // update toolbar "in mud" setting for specific buttons...
        _tbarLeft.setInsideMUD(inside_mud);
        _tbarRight.setInsideMUD(inside_mud);
        _tbarBottom.setInsideMUD(inside_mud);
    }

    // --------------
    // GUI Events
    // --------------
    public void finalizeGUI(){
        _term.finalizeGUI();
        _telInput.requestFocus();

        // --------------
        // Optional Auto-Stuff
        // --------------
        // auto single-mode...
        // _tglIMode.setSelectedIndex(0);
        // auto connect...
        // _omt.connect(_lblBBSNetAddr.getText(), _lblBBSNetPort.getText());
    }

    private class TGLL_Views extends OMUD_GUIToggleButtonGroup.OMUD_GUIToggleButtonGroupListener {
        public void toggleSelected(int sel_index){
            SwingUtilities.invokeLater(new Runnable(){public void run(){
                _telInput.requestFocus();
            }});
        }
    }

    private class TGLL_IMode extends OMUD_GUIToggleButtonGroup.OMUD_GUIToggleButtonGroupListener {
        public void toggleSelected(int sel_index){
            if (sel_index > -1){
                _telInput.setSingleMode(true);
                _tglIMode.setIndexText(0, "Disable Single Mode");
            } else {
                _telInput.setSingleMode(false);
                _tglIMode.setIndexText(0, "Enable Single Mode");
            }
            SwingUtilities.invokeLater(new Runnable(){public void run(){
                _telInput.requestFocus();
            }});
        }
    }

    private class BL_BtnClearCmds extends OMUD_GUIButton.OMUD_GUIButtonListener {
        public void released(OMUD_GUIButton btn) {
            _omt.clearCmds();

            SwingUtilities.invokeLater(new Runnable(){public void run(){
                _telInput.requestFocus();
            }});
        }
    }

    private class BL_BtnConnect extends OMUD_GUIButton.OMUD_GUIButtonListener {
        public void released(OMUD_GUIButton btn) {
            if (_omt.isConnected())
                 _omt.disconnect(true);
            else _omt.connect(_lblBBSNetAddr.getText(), _lblBBSNetPort.getText());

            SwingUtilities.invokeLater(new Runnable(){public void run(){
                _telInput.requestFocus();
            }});
        }
    }

    private class MA_TerminalFocus extends MouseAdapter {
        private Timer _timer = null;
        public void mousePressed(MouseEvent e) {
            _timer = new Timer(100, new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    // waiting for mouse to be released
                }
            });
            _timer.start();
        }
        public void mouseReleased(MouseEvent e) {
            if (_timer != null)
                _timer.stop();

            // copy to clipboard...
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(
                new StringSelection(_term.getSelectedText()), null);

            // return focus...
            SwingUtilities.invokeLater(new Runnable(){public void run(){
                _term.getCaret().setVisible(false);
                _term.getCaret().setVisible(true);
                _telInput.requestFocus();
            }});

            // restore the terminal buffer/caret position in case it moved...
            _omge.notifyGUIEvent(new OMUD_GUIEvent_ResetTerminalCaret());
        }
    }

    public void resetTerminalCaret(int pos){
        _term.setCaretPosition(pos);
    }

    public void focusTelnetInput(){
        _telInput.requestFocus();
    }

    // --------------
    // Telnet Events
    // --------------
    public void eventTelnet_Connected(OMUD_TelnetEvent_Connected event){
        _btnBBSConnect.setText("Disconnect");
        _btnBBSConnect.setBackground(OMUD.CLR_BG_DARK_RED);
        updateBBSLocText(OMUD.eBBSLoc.BBS);
    }

    public void eventTelnet_Disconnected(OMUD_TelnetEvent_Disconnected event){
        _btnBBSConnect.setText("Connect");
        _btnBBSConnect.setBackground(OMUD.CLR_BG_DARK_GREEN);
        updateBBSLocText(OMUD.eBBSLoc.OFFLINE);
    }

    public void eventTelnet_DataRender(OMUD_TelnetEvent_DataRender event){
        // allow the terminal to render buffer mods...
        _term.render(event.omb, event.arrlBMods);

        // force update scrollbars...
        _scroll.scrollToBottom();
        // horiz update requires GUI update delay...
        //SwingUtilities.invokeLater(new Runnable(){public void run(){
        //    sp.getHorizontalScrollBar().setValue(0);}});
    }

    // --------------
    // MUD Events
    // --------------
    public void eventMUD_BBSLoc(OMUD_MUDEvent_BBSLoc event){
        updateBBSLocText(event.bbs_loc);
    }

    public void eventMUD_UserCmd(OMUD_MUDEvent_UserCmd event){
        _lblMUDCmd.setText("CMD: [" + event.remaining + "] " + event.cmd);
        if (event.remaining > 0)
             _lblMUDCmd.setBackground(OMUD.CLR_BG_DARK_YELLOW);
        else _lblMUDCmd.setBackground(OMUD.CLR_BG_DARK_GREY);
    }

    public void eventMUD_Statline(OMUD_MUDEvent_Statline event){
        _lblMUDAState.setText(OMUD_MMUD_DataBlock_Statline.ACTION_STATE_STRINGS[event.dataStatline.action_state.ordinal()]);
        _lblMUDCTimer.setText(event.tmrdCombat.seconds + "/" + event.tmrdCombat.seconds_max);
        _lblMUDIState.setText(event.tmrdStatline.seconds == 0 ? "Input: Ready" :
            "Input: Statline Wait (" +  event.tmrdStatline.seconds + "/" + event.tmrdStatline.seconds_max + ")");
        _lblMUDStatline.setText(event.dataStatline.getHealthString());

        // action state colors...
        if (event.dataStatline.action_state == OMUD_MMUD_DataBlock_Statline.eActionState.READY)
             _lblMUDAState.setBackground(OMUD.CLR_BG_DARK_GREY);
        else if (event.dataStatline.action_state == OMUD_MMUD_DataBlock_Statline.eActionState.COMBAT)
             _lblMUDAState.setBackground(OMUD.CLR_BG_DARK_RED);
        else _lblMUDAState.setBackground(OMUD.CLR_BG_DARK_GREEN);

        // statline/input wait colors...
        if (event.tmrdStatline.seconds < 3)
             _lblMUDIState.setBackground(OMUD.CLR_BG_DARK_GREY);
        else _lblMUDIState.setBackground(OMUD.CLR_BG_DARK_RED);

        // combat timer colors...
        if (event.tmrdCombat.seconds == 0)
             _lblMUDCTimer.setBackground(OMUD.CLR_BG_DARK_GREY);
        else _lblMUDCTimer.setBackground(OMUD.CLR_BG_DARK_RED);
    }

    public void eventMUD_Exp(OMUD_MUDEvent_Exp event){updateExp(event.dataExp);}
    public void updateExp(OMUD_MMUD_DataBlock_Exp dataExp){
        _lblMUDExp.setText("XP: " + dataExp.cur_perc + "% (" + dataExp.next_rem + ")");
        _lblMUDExp.setCaretPosition(0);
    }

    public void eventMUD_Room(OMUD_MUDEvent_Room event){
        _lblMUDRoomID.setText("RID: " + event.dataRoom.megaID);
    }

    public void eventMUD_BufferSize(OMUD_MUDEvent_BufferSize event){
        _lblMUDBufSize.setText(Long.toString(event.buffer_size));
        if (event.buffer_size > 0)
             _lblMUDBufSize.setBackground(OMUD.CLR_BG_DARK_RED);
        else _lblMUDBufSize.setBackground(OMUD.CLR_BG_DARK_GREY);
    }
}
