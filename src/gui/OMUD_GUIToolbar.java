import java.util.ArrayList;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.imageio.ImageIO;
import java.io.File;

public abstract class OMUD_GUIToolbar extends JPanel {
    public interface ToolbarButtonClickedCallback{
        public abstract void toolbarButtonClicked(int btn_id);
    }

    protected static final int BTN_VWIDTH =  25;
    protected static final int BTN_VHEIGHT = 35;
    protected static final int SPACER_SIZE = 10;

    protected ArrayList<OMUD_GUIButton>    _arrlBtns =       null;
    private   ArrayList<Boolean>           _arrlBtnsMUDReq = null;
    private   BL_TbarBtn                   _tbarl =          null;
    private   ToolbarButtonClickedCallback _tbcc =           null;

    public OMUD_GUIToolbar(ToolbarButtonClickedCallback tbcc){
        setBackground(OMUD.CLR_BG_DARK_GREY);

        _tbcc = tbcc;
        _arrlBtns = new ArrayList<OMUD_GUIButton>();
        _arrlBtnsMUDReq = new ArrayList<Boolean>();
        _tbarl = new BL_TbarBtn();
    }

    private class BL_TbarBtn extends OMUD_GUIButton.OMUD_GUIButtonListener {
        public void released(OMUD_GUIButton btn){
            toolbarButtonClicked(btn.getID());
            if (_tbcc != null)
                _tbcc.toolbarButtonClicked(btn.getID());
        }
    }

    public void setInsideMUD(boolean inside_mud){
        for (int i = 0; i < _arrlBtns.size(); ++i)
            _arrlBtns.get(i).setEnabled(_arrlBtnsMUDReq.get(i) ? inside_mud : true);
    }

    protected void addBtn(String strFilepath, String strTooltip, boolean mud_req, int width, int height){
        try {
            OMUD_GUIButton btn = null;
            if (strFilepath.length() > 0){
                btn = new OMUD_GUIButton(new ImageIcon(ImageIO.read(new File(strFilepath))), strTooltip, _arrlBtns.size(), _tbarl, null);
            } else {
                btn = new OMUD_GUIButton(Integer.toString(_arrlBtns.size() + 1),  strTooltip, _arrlBtns.size(), _tbarl, null);
                btn.setFont(new Font(btn.getFont().getName(), btn.getFont().getStyle(), 14));
                btn.setMargin(new Insets(5, 5, 5, 5));
            }
            btn.setPreferredSize(new Dimension(width, height));
            btn.setEnabled(!mud_req);
            _arrlBtns.add(btn);
            _arrlBtnsMUDReq.add(mud_req);
        } catch (Exception e) {OMUD_Log.toConsoleError("Error adding toolbar button: " + e.getMessage());}
    }

    protected abstract void toolbarButtonClicked(int btn_id);
}
