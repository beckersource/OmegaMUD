import java.awt.Font;
import javax.swing.JTextField;

public class OMUD_GUITextField extends JTextField{
    public OMUD_GUITextField()                 {this("",       false, false, false);}
    public OMUD_GUITextField(String strText)   {this(strText,  false, false, false);}
    public OMUD_GUITextField(String strText, boolean editable, boolean bold, boolean center_text){
        setText(strText);

        setEditable(editable);
        setBackground(OMUD.CLR_BG_DARK_GREY);
        setForeground(OMUD.CLR_FG_WHITE);
        if (bold)
            setFont(getFont().deriveFont(Font.BOLD, getFont().getSize()));
        if (center_text)
            setHorizontalAlignment(JTextField.CENTER);
    }
}
