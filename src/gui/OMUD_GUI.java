import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Desktop;
import java.net.URI;

public class OMUD_GUI{

    // ------------------
    // FrameCharList
    // ------------------
    private class FrameCharList extends JFrame implements OMUD_GUIToolbar.ToolbarButtonClickedCallback {
        private OMUD_Char _omc =    null;
        private JTable    _table =  null;
        private OMUD_GUIMenuItem _mnuiOMPrefs =     null;
        private OMUD_GUIMenuItem _mnuiOMWeb =       null;
        private OMUD_GUIMenuItem _mnuiOMExit =      null;
        private OMUD_GUIMenuItem _mnuiCharAdd  =    null;
        private OMUD_GUIMenuItem _mnuiCharDel  =    null;
        private OMUD_GUIMenuItem _mnuiCharEdit =    null;
        private OMUD_GUIMenuItem _mnuiWinShow  =    null;
        private OMUD_GUIMenuItem _mnuiWinHide  =    null;
        private OMUD_GUIToolbarChars _tbar =        null;

        private final int        FRAME_MIN_WIDTH  =  450;
        private final int        FRAME_MIN_HEIGHT =  175;
        private final String[]   TABLE_COL_STRINGS = {"REALM", "NAME"};

        public FrameCharList(){
            setMinimumSize(new Dimension(FRAME_MIN_WIDTH, FRAME_MIN_HEIGHT));
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setTitle("OmegaMUD v0 (Char Status/Editor)");
            //createMenu();

            _tbar = new OMUD_GUIToolbarChars(this);
            //_tbar.setMaximumSize(new Dimension(35, 100));

            String[][] strChars = {
                {"BearsBBS_Normal", "Gandalf LastName"},
                {"BearsBBS_Edited", "Aragorn LastName"},
                {"BearsBBS_MudRev", "Legolas LastName"},
                {"ClassicMUD",      "Gimli LastName"},
                {"DarkwoodBBS",     "Syntax MudInfo"}};
            _table = new JTable(strChars, TABLE_COL_STRINGS);
            _omc =   new OMUD_Char();
            _omc.updateCharLists(strChars);

            // table rows...
            _table.setBackground(OMUD.CLR_BG_DARK_GREY);
            _table.setForeground(OMUD.CLR_FG_WHITE);
            _table.setFont(OMUD.getTerminalFont());
            // table header...
            _table.getTableHeader().setBackground(OMUD.CLR_BG_DARK_TABLE);
            _table.getTableHeader().setForeground(OMUD.CLR_FG_WHITE);
            _table.getTableHeader().setFont(OMUD.getTerminalFont());

            // scroll...
            JScrollPane scroll = new JScrollPane(_table); // table must go inside a scroll pane to show column headers properly
            scroll.getViewport().setBackground(OMUD.CLR_BG_DARK_GREY);

            layoutChars(scroll);
            pack();
        }

        private void layoutChars(JScrollPane scroll){
            GridBagLayout gbl = new GridBagLayout();
            GridBagConstraints gblc = new GridBagConstraints();

            JPanel pnlRight = new JPanel();
            pnlRight.setBackground(OMUD.CLR_BG_DARK_GREY);

            gblc.weightx =      0.0;
            gblc.weighty =      0.0;
            gblc.gridwidth =    1;
            gblc.gridheight =   1;
            gblc.fill =         GridBagConstraints.HORIZONTAL;
            gbl.setConstraints(_tbar, gblc);
            gblc.weightx =      1.0;
            gblc.gridwidth =    GridBagConstraints.REMAINDER;
            gblc.fill =         GridBagConstraints.BOTH;
            gbl.setConstraints(pnlRight, gblc);

            gblc.weightx =      1.0;
            gblc.weighty =      1.0;
            gblc.gridwidth =    0;
            gblc.gridheight =   GridBagConstraints.REMAINDER;
            gbl.setConstraints(scroll, gblc);

            setLayout(gbl);
            add(_tbar);
            add(pnlRight);
            add(scroll);
        }

        public void toolbarButtonClicked(int btn_id){
                 if (btn_id == OMUD_GUIToolbarChars.eBtnType.CHAR_OPEN.ordinal());
            else if (btn_id == OMUD_GUIToolbarChars.eBtnType.CHAR_CREATE.ordinal());
            else if (btn_id == OMUD_GUIToolbarChars.eBtnType.CHAR_EDIT.ordinal());
            else if (btn_id == OMUD_GUIToolbarChars.eBtnType.CHAR_REMOVE.ordinal());
            else if (btn_id == OMUD_GUIToolbarChars.eBtnType.WIN_SHOW.ordinal());
            else if (btn_id == OMUD_GUIToolbarChars.eBtnType.WIN_HIDE.ordinal());
            else if (btn_id == OMUD_GUIToolbarChars.eBtnType.SETTINGS.ordinal());
            else if (btn_id == OMUD_GUIToolbarChars.eBtnType.WEB_HELP.ordinal()){
                try{ if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE))
                         Desktop.getDesktop().browse(new URI("http://www.omegamud.com"));
                } catch (Exception e){OMUD_Log.toConsoleError("Error about website link: " + e.getMessage());}
            }
        }

        public OMUD_Char getSelectedChar(){return _omc;}

        /*
        private void createMenu(){
            OMUD_GUIMenuBar menu = new OMUD_GUIMenuBar();
            OMUD_GUIMenu mOmegaMUD = new OMUD_GUIMenu("OmegaMUD");
            mOmegaMUD.add((_mnuiOMPrefs = new OMUD_GUIMenuItem("Settings")));
            mOmegaMUD.add((_mnuiOMWeb =   new OMUD_GUIMenuItem("Website")));
            mOmegaMUD.addSeparator();
            mOmegaMUD.add((_mnuiOMExit =  new OMUD_GUIMenuItem("Exit")));
            OMUD_GUIMenu mChars  = new OMUD_GUIMenu("Chars");
            mChars.add((_mnuiCharAdd =  new OMUD_GUIMenuItem("Open")));
            mChars.add((_mnuiCharAdd =  new OMUD_GUIMenuItem("Create")));
            mChars.addSeparator();
            mChars.add((_mnuiCharDel =  new OMUD_GUIMenuItem("Delete Selected")));
            mChars.add((_mnuiCharEdit = new OMUD_GUIMenuItem("Edit Selected")));
            OMUD_GUIMenu mWindows = new OMUD_GUIMenu("Windows");
            mWindows.add((_mnuiWinShow = new OMUD_GUIMenuItem("Show All")));
            mWindows.add((_mnuiWinHide = new OMUD_GUIMenuItem("Hide All")));
            menu.add(mOmegaMUD);
            menu.add(mChars);
            menu.add(mWindows);
            setJMenuBar(menu);

            AL_MenuItems almi = new AL_MenuItems();
            _mnuiOMPrefs.addActionListener(almi);
            _mnuiOMWeb.addActionListener(almi);
            _mnuiOMExit.addActionListener(almi);
            _mnuiCharAdd.addActionListener(almi);
            _mnuiCharDel.addActionListener(almi);
            _mnuiCharEdit.addActionListener(almi);
            _mnuiWinShow.addActionListener(almi);
            _mnuiWinHide.addActionListener(almi);
        }

        public class AL_MenuItems implements ActionListener{
            public void actionPerformed(ActionEvent event){
                     if (event.getSource() == _mnuiOMPrefs);
                else if (event.getSource() == _mnuiOMWeb){
                    try{ if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE))
                             Desktop.getDesktop().browse(new URI("http://www.omegamud.com"));
                    } catch (Exception e){OMUD_Log.toConsoleError("Error about website link: " + e.getMessage());}
                }
                else if (event.getSource() == _mnuiOMExit){
                    System.exit(0);
                }
                else if (event.getSource() == _mnuiCharAdd);
                else if (event.getSource() == _mnuiCharDel);
                else if (event.getSource() == _mnuiCharEdit);
                else if (event.getSource() == _mnuiWinShow);
                else if (event.getSource() == _mnuiWinHide);
            }
        }
        */
    }

    // ------------------
    // OMUD_GUI
    // ------------------
    private FrameCharList _fCharList = null;
    public OMUD_GUI() {
        _fCharList = new FrameCharList();
        final OMUD_GUIFCharView fView = _fCharList.getSelectedChar().getViewFrame();
        final OMUD_GUIFCharInfo fInfo = _fCharList.getSelectedChar().getInfoFrame();
        final OMUD_GUIFCharInv  fInv =  _fCharList.getSelectedChar().getInvFrame();

        //fView.setLocationRelativeTo(null); // center in main display
        Dimension size = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getBounds().getSize();
        _fCharList.setSize(_fCharList.getMinimumSize());
        fInfo.setLocation((int) (size.getWidth() - fInfo.getSize().getWidth()) - 50, (int) _fCharList.getSize().getHeight() + 50);
        fView.setLocation((int) (size.getWidth() - (fView.getSize().getWidth() * 1.5)), fInfo.getLocation().y + 100);
        _fCharList.setLocation(fInfo.getLocation().x, 50);
        _fCharList.setVisible(true);
        fInfo.setVisible(true);
        fView.setVisible(true);
        fInv.setVisible(true);

        // do some post-processing...
        SwingUtilities.invokeLater(new Runnable(){public void run(){
            fView.finalizeGUI();
        }});

        OMUD_Log.toConsoleInfo("GUI Created");
    }
}
