import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_CSCombat extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_NO_RESPONSE =     "[0;37;40m";
    private final String MSTR_COMBAT_ON =       "[79D[K[0;33m*Combat Engaged*";
    private final String MSTR_BAD_CAST_CMD =    "[0;37;40m[1;31;40mSyntax: CAST {spell} [{target}]";
    private final String MSTR_CANT_SMASH =      "[0;37;40m[79D[K[1m[31mYou don't know the first thing about smashing!";

    private final int POS_CMD_ATTACK = 0;
    private final int POS_CMD_CAST =   1;
    private final int POS_CMD_BREAK =  2;

    public static String getCmdText(){return "";}
    public OMUD_MMUD_ParseBlock_CSCombat(){
        _arrlCmdText.add(new CmdText("attack", 1, CmdText.eDynamicType.DYNAMIC_SPACE)); // POS_CMD_ATTACK
        _arrlCmdText.add(new CmdText("cast",   1, CmdText.eDynamicType.DYNAMIC_SPACE)); // POS_CMD_CAST
        _arrlCmdText.add(new CmdText("break",  3, CmdText.eDynamicType.STATIC));        // POS_CMD_BREAK
        _arrlCmdText.add(new CmdText("smash",  3, CmdText.eDynamicType.DYNAMIC_SPACE));
    }

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        // ------------------
        // Combat: ON
        // ------------------
        if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_COMBAT_ON, "")) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.COMBAT);
            updateCombatState(omcd.getMMUDData(), true, false);
            omcd.getMMUDData().dataCombat.arrlLines.add(new OMUD_MMUD_DataBlock_Combat.CombatLine(OMUD_MMUD_DataBlock_Combat.eLineType.COMBAT_ON));

        // ------------------
        // Cast
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_BAD_CAST_CMD, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[BAD_CAST_CMD]\n");

        // ------------------
        // Smash
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_CANT_SMASH, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[CANT_SMASH]\n");

        // ------------------
        // Attack No Response (command was sent without dynamic text and action did nothing)
        // ------------------
        } else if (strCmd.indexOf(" ") == -1 &&
            (_arrlCmdText.get(POS_CMD_ATTACK).text.indexOf(strCmd) > -1  ||
             _arrlCmdText.get(POS_CMD_CAST).text.indexOf(strCmd)   > -1  ||
             _arrlCmdText.get(POS_CMD_BREAK).text.indexOf(strCmd)  > -1) &&
            (pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_NO_RESPONSE, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[NO_RESPONSE_ATTACK: " + strCmd + "]\n");
        }

        return pos_data_left;
    }
}
