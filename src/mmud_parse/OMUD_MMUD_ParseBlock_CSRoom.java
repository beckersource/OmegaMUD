import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_CSRoom extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_OPT_RESET_WHBL =      "[0;37;40m";
    private final String MSTR_NO_RESPONSE =         "[0;37;40m";
    private final String MSTR_ROOM_NAME =           "[79D[K[1;36m";
    private final String MSTR_OBVIOUS_EXITS =       "Obvious exits: ";
    private final String MSTR_ROOM_DESC =           "    ";
    private final String MSTR_YOU_NOTICE_PRE =      "You notice ";
    private final String MSTR_YOU_NOTICE_END =      "here.";
    private final String MSTR_ALSO_HERE_PRE =       "Also here: ";
    private final String MSTR_ALSO_HERE_END =       ".";
    private final String MSTR_LIGHT_DIM =           "The room is dimly lit";
    private final String MSTR_LIGHT_DARK =          "The room is very dark";
    private final String MSTR_LIGHT_BARELY =        "The room is barely visible";
    private final String MSTR_LIGHT_BLACK =         "The room is pitch black";
    private final String MSTR_SEARCH_NONE =         "[0;36mYour search revealed nothing.";
    private final String MSTR_SEARCH_PRE =          "[0;37;40m[0;36mYou notice ";
    private final String MSTR_MOVE_NO_EXIT =        "[0;37;40m[79D[KThere is no exit in that direction!";
    private final String MSTR_DOOR_CLOSED_MOVE1 =   "[1;31;40mThe door is closed!";                    // move attempt
    private final String MSTR_DOOR_CLOSED_MOVE2 =   "[1;31;40mThe door is Closed!";                    // move attempt
    private final String MSTR_DOOR_CLOSED_LOOK =    "[0;37;40mThe door is closed in that direction!";  // look attempt
    private final String MSTR_BASH_OPEN =           "You bashed the door open.";
    private final String MSTR_BASH_FAIL =           "Your attempts to bash through fail!";
    private final String MSTR_CANT_BASH =           "[0;37;40m[79D[K[1;31mYou don't know the first thing about bashing!";
    private final String MSTR_BASH_DMG_PRE =        "You take ";
    private final String MSTR_BASH_DMG_END =        " damage for bashing the door!";
    private final String MSTR_CLOSE_OK =            "The door is now closed.";
    private final String MSTR_DOOR_WAS_OPEN =       "[0;37;40mThe door is already open.";
    private final String MSTR_CL_FAIL_PRE =         "[0;37;40mYou may not ";
    private final String MSTR_CL_FAIL_END =         " doors or gates while attacking or being attacked!";
    private final String MSTR_LOCK_NONE =           "[0;37;40m[79D[KThere is no benefit to locking in that direction.";
    private final String MSTR_PICK_FAIL =           "[0;37;40m[79D[KYour skill fails you this time.";

    private final int POS_CMD_BASH = 2;

    public static String getCmdText(){return "\n";}
    public OMUD_MMUD_ParseBlock_CSRoom(){
        _arrlCmdText.add(new CmdText("look",   0, CmdText.eDynamicType.DYNAMIC_SPACE)); // 0-len covers LF/enter only (zero-len) and all chars as part of look
        _arrlCmdText.add(new CmdText("search", 3, CmdText.eDynamicType.STATIC));        // only "sea" is required
        _arrlCmdText.add(new CmdText("bash",   3, CmdText.eDynamicType.DYNAMIC_SPACE)); // POS_CMD_BASH
        _arrlCmdText.add(new CmdText("pick",   2, CmdText.eDynamicType.DYNAMIC_SPACE));
        _arrlCmdText.add(new CmdText("close",  2, CmdText.eDynamicType.DYNAMIC_SPACE));
        _arrlCmdText.add(new CmdText("lock",   3, CmdText.eDynamicType.DYNAMIC_SPACE));

        // ------------------------
        // NOT USING NOW BUT PERHAPS THE FUTURE
        // ------------------------
        // move commands: full...
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_STRINGS[OMUD_MMUD_DataExit.eExitDir.NORTH.ordinal()],          5, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_STRINGS[OMUD_MMUD_DataExit.eExitDir.SOUTH.ordinal()],          5, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_STRINGS[OMUD_MMUD_DataExit.eExitDir.EAST.ordinal()],           3, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_STRINGS[OMUD_MMUD_DataExit.eExitDir.WEST.ordinal()],           4, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_STRINGS[OMUD_MMUD_DataExit.eExitDir.NE.ordinal()],             6, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_STRINGS[OMUD_MMUD_DataExit.eExitDir.NW.ordinal()],             6, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_STRINGS[OMUD_MMUD_DataExit.eExitDir.SE.ordinal()],             6, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_STRINGS[OMUD_MMUD_DataExit.eExitDir.SW.ordinal()],             6, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_STRINGS[OMUD_MMUD_DataExit.eExitDir.UP.ordinal()],             1, CmdText.eDynamicType.STATIC)); // also covers short version
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_STRINGS[OMUD_MMUD_DataExit.eExitDir.DOWN.ordinal()],           3, CmdText.eDynamicType.STATIC));
        // move commands: short...
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_SHORTL_STRINGS[OMUD_MMUD_DataExit.eExitDir.NORTH.ordinal()],   1, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_SHORTL_STRINGS[OMUD_MMUD_DataExit.eExitDir.SOUTH.ordinal()],   1, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_SHORTL_STRINGS[OMUD_MMUD_DataExit.eExitDir.EAST.ordinal()],    1, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_SHORTL_STRINGS[OMUD_MMUD_DataExit.eExitDir.WEST.ordinal()],    1, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_SHORTL_STRINGS[OMUD_MMUD_DataExit.eExitDir.NE.ordinal()],      2, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_SHORTL_STRINGS[OMUD_MMUD_DataExit.eExitDir.NW.ordinal()],      2, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_SHORTL_STRINGS[OMUD_MMUD_DataExit.eExitDir.SE.ordinal()],      2, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_SHORTL_STRINGS[OMUD_MMUD_DataExit.eExitDir.SW.ordinal()],      2, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText(OMUD_MMUD_DataExit.EXIT_DIR_SHORTL_STRINGS[OMUD_MMUD_DataExit.eExitDir.DOWN.ordinal()],    1, CmdText.eDynamicType.STATIC));
    }

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){

        // ------------------
        // New Room Data
        // ------------------
        if (omcd.getParseData().sbBuffer.lastIndexOf(MSTR_OBVIOUS_EXITS, omcd.getParseData().sbBuffer.length() - 1) > -1 &&
            (pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_ROOM_NAME, "")) > -1){

            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.ROOM);
            omcd.getMMUDData().dataRoom = new OMUD_MMUD_DataBlock_Room();
            cleanData(omcd.getParseData().sbBlockDataLeft, true, true);

            int pos_left  = 0;
            int pos_right = 0;

            // ------------------
            // Obvious Exits + Optional Light
            // ------------------
            pos_left = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_OBVIOUS_EXITS, omcd.getParseData().sbBlockDataLeft.length() - 1) + MSTR_OBVIOUS_EXITS.length();
                 if ((pos_right = omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_LIGHT_DIM,      pos_left))  > -1)
                omcd.getMMUDData().dataRoom.light = OMUD_MMUD_DataBlock_Room.eRoomLight.DIMLY_LIT;
            else if ((pos_right = omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_LIGHT_DARK,     pos_left))  > -1)
                omcd.getMMUDData().dataRoom.light = OMUD_MMUD_DataBlock_Room.eRoomLight.VERY_DARK;
            else if ((pos_right = omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_LIGHT_BARELY,   pos_left))  > -1)
                omcd.getMMUDData().dataRoom.light = OMUD_MMUD_DataBlock_Room.eRoomLight.BARELY_VIS;
            else if ((pos_right = omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_LIGHT_BLACK,    pos_left))  > -1)
                omcd.getMMUDData().dataRoom.light = OMUD_MMUD_DataBlock_Room.eRoomLight.PITCH_BLACK;
            // if no light string, set right to the end...
            if (pos_right == -1)
                pos_right = omcd.getParseData().sbBlockDataLeft.length();

            // get exits here...
            buildRoomExits(omcd.getParseData().sbBlockDataLeft.substring(pos_left, pos_right), omcd.getMMUDData().dataRoom);
            pos_left = pos_right = pos_left - MSTR_OBVIOUS_EXITS.length() - 1;

            // ------------------
            // Optional: Also Here (Units)
            // ------------------
            int pos_right_prev = pos_right;
            if ((pos_right = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_ALSO_HERE_END, pos_right)) > -1 &&
                (pos_left  = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_ALSO_HERE_PRE, pos_right)) > -1){
                ArrayList<String> arrlNew = new ArrayList<String>();
                splitCommaListToArray(omcd.getParseData().sbBlockDataLeft.substring(pos_left + MSTR_ALSO_HERE_PRE.length(), pos_right), arrlNew);
                buildUnits(arrlNew, omcd.getMMUDData().dataRoom.arrlUnits);
                pos_right = pos_left - 1;
            } else pos_right = pos_right_prev;

            // ------------------
            // Optional: Items (You Notice)
            // ------------------
            pos_right_prev = pos_right;
            if ((pos_right = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_YOU_NOTICE_END, pos_right)) > -1 &&
                (pos_left  = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_YOU_NOTICE_PRE, pos_right)) > -1){
                ArrayList<String> arrlNew = new ArrayList<String>();
                splitCommaListToArray(omcd.getParseData().sbBlockDataLeft.substring(pos_left + MSTR_YOU_NOTICE_PRE.length(), pos_right), arrlNew);
                buildItems(arrlNew, omcd.getMMUDData().dataRoom.arrlItems, omcd.getMMUDData().dataRoom.coins, null);
                pos_right = pos_left - 1;
            } else pos_right = pos_right_prev;

            // ------------------
            // Optional: Room Description
            // ------------------
            if ((pos_left = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_ROOM_DESC, pos_right)) > -1){
                omcd.getMMUDData().dataRoom.desc = omcd.getParseData().sbBlockDataLeft.substring(pos_left + MSTR_ROOM_DESC.length(), pos_right + 1).trim();
                pos_right = pos_left - 1;
            }

            // ------------------
            // Room Name
            // ------------------
            if (!omcd.getMMUDData().got_statline){
                StringBuilder sbWelcome = new StringBuilder(omcd.getParseData().sbBuffer.substring(0, pos_data_left));
                cleanData(sbWelcome, false, true);
                omcd.getMMUDData().strWelcome = sbWelcome.toString().trim();
                omcd.getParseData().sbBuffer.delete(0, pos_data_left);
                pos_data_left = 0;
            // PREFIX: if room name is shown after a move command, it will have a white/black reset prefix...
            } else {
                pos_data_left = removeOptPrefix(omcd, "Room Name After Move", pos_data_left, MSTR_OPT_RESET_WHBL);
            }

            omcd.getMMUDData().dataRoom.name = omcd.getParseData().sbBlockDataLeft.substring(0, pos_right);
            // create Megamud RoomID after the exit data is built above...
            omcd.getMMUDData().dataRoom.megaID =
                OMUD_MEGA.getRoomNameHash(omcd.getMMUDData().dataRoom.name) +
                OMUD_MEGA.getRoomExitsCode(omcd.getMMUDData().dataRoom.arrlExits);

        // ------------------
        // Search: Found Items
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_SEARCH_PRE, MSTR_YOU_NOTICE_END)) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.ROOM);
            cleanData(omcd.getParseData().sbBlockDataLeft, true, false);

            ArrayList<String> arrlNew = new ArrayList<String>();
            splitCommaListToArray(omcd.getParseData().sbBlockDataLeft.toString(), arrlNew);
            // special: reset coins -
            // coins and their full count are always shown on first search, so reset coins to avoid extra string processing...
            buildItems(arrlNew, omcd.getMMUDData().dataRoom.arrlItemsHidden, (omcd.getMMUDData().dataRoom.coins_hidden = new OMUD_MMUD_DataCoins()), null);

        // ------------------
        // Search: No Items
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_SEARCH_NONE, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[ROOM_SEARCH_NONE]\n");

        // ------------------
        // Invalid Move Dir (no exit in dir)
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_MOVE_NO_EXIT, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[MOVE_NO_EXIT_DIR]\n");

        // ------------------
        // Door Closed
        // ------------------
        } else if (
            (pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_DOOR_CLOSED_MOVE1, "")) > -1 ||
            (pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_DOOR_CLOSED_MOVE2, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[DOOR_CLOSED_MOVE]\n");
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_DOOR_CLOSED_LOOK, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[DOOR_CLOSED_LOOK]\n");

        // ------------------
        // Bash
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_BASH_OPEN, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[BASH_OPEN]\n");
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_BASH_FAIL, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[BASH_FAIL]\n");
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_DOOR_WAS_OPEN, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[DOOR_WAS_OPEN]\n");
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_CANT_BASH, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[CANT_BASH]\n");
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_BASH_DMG_PRE, MSTR_BASH_DMG_END)) > -1){
            omcd.getParseData().arrlDebugText.add("[BASH_DAMAGE: " + omcd.getParseData().sbBlockDataLeft + "]\n");
        // ------------------
        // Bash No Response (command was sent without dynamic text and action did nothing)
        // ------------------
        } else if (strCmd.indexOf(" ") == -1 && _arrlCmdText.get(POS_CMD_BASH).text.indexOf(strCmd) > -1 &&
            (pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_NO_RESPONSE, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[NO_RESPONSE_BASH]\n");

        // ------------------
        // Close/Lock
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_CLOSE_OK,  "")) > -1){
            omcd.getParseData().arrlDebugText.add("[CLOSE_OK]\n");
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_LOCK_NONE, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[LOCK_NO_BENEFIT]\n");
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_CL_FAIL_PRE, MSTR_CL_FAIL_END)) > -1){
            omcd.getParseData().arrlDebugText.add("[CLOSE_LOCK_DURING_COMBAT: " + omcd.getParseData().sbBlockDataLeft + "]\n");

        // ------------------
        // Picklocks
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_PICK_FAIL, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[PICK_FAIL]\n");
        }

        return pos_data_left;
    }

    private void buildRoomExits(String strExits, OMUD_MMUD_DataBlock_Room dr){
        dr.arrlExits.clear();

        String[] dirs = strExits.split(",");
        for (String dir : dirs){

            // split again, for now just use the last token to get the direction...
            String[] words = dir.trim().split(" ");
            if (words.length > 0){
                String word_door_state = "";
                String word_door_name  = "";
                String word_dir = words[words.length - 1];
                OMUD_MMUD_DataExit.eExitDir  edir =  OMUD_MMUD_DataExit.eExitDir.NONE;
                OMUD_MMUD_DataExit.eDoorType edoor = OMUD_MMUD_DataExit.eDoorType.NONE;

                // if exists, match up the door data...
                if (words.length == 3){
                    word_door_state = words[0];
                    word_door_name  = words[1]; // not sure if door name will ever be needed or not
                    // match up the door state -
                    // ignore case by default, not sure if needed for doors...
                    int ds_count = OMUD_MMUD_DataExit.DOOR_TYPE_STRINGS.length;
                    for (int i = 0; i < ds_count; ++i){
                        if (word_door_state.equalsIgnoreCase(OMUD_MMUD_DataExit.DOOR_TYPE_STRINGS[i])){
                            edoor = OMUD_MMUD_DataExit.eDoorType.values()[i];
                            break;
                        }
                    }
                }

                // match up the direction -
                // ignore case for some situations with secret passages with upper first char directions and probably others...
                int ed_count = OMUD_MMUD_DataExit.EXIT_DIR_STRINGS.length;
                for (int i = 0; i < ed_count; ++i){
                    if (word_dir.equalsIgnoreCase(OMUD_MMUD_DataExit.EXIT_DIR_STRINGS[i])){
                        edir = OMUD_MMUD_DataExit.eExitDir.values()[i];
                        break;
                    }
                }

                dr.arrlExits.add(new OMUD_MMUD_DataExit(edir, edoor));
            }
        }
    }

    private void buildUnits(ArrayList<String> arrlNew, ArrayList<OMUD_MMUD_DataUnit> arrlUnits){
        for (int i = 0; i < arrlNew.size(); ++i)
            arrlUnits.add(new OMUD_MMUD_DataUnit(arrlNew.get(i)));
    }
}
