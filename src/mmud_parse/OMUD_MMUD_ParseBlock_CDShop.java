import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_CDShop extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_NO_RESPONSE = "[0;37;40m";
    private final String MSTR_SHOP_YES =    "[0;37;40m[0;37;40m[79D[KThe following items are for sale here:\n\n[0;32mItem                          [36mQuantity    Price\n------------------------------------------------------\n";
    private final String MSTR_SHOP_NO =     "[0;37;40m[1;31;40mYou cannot LIST if you are not in a shop!";
    private final String MSTR_ITEM_NAME =   "[32m";
    private final String MSTR_ITEM_QTY =    "[36m";
    private final String MSTR_NO_USE =      " (You can't use)";
    private final String MSTR_FREE =        "Free";

    private final String MSTR_BUY_SYNTAX =          "[0;37;40m[1;31;40mSyntax: BUY {item}";
    private final String MSTR_SELL_SYNTAX =         "[0;37;40m[1;31;40mSyntax: SELL {item}";
    private final String MSTR_BUY_NO_SHOP =         "[0;37;40m[79D[K[1;31;40mYou cannot BUY if you are not in a shop!";
    private final String MSTR_SELL_NO_SHOP =        "[0;37;40m[79D[K[1;31;40mYou cannot SELL if you are not in a shop!";

    private final String MSTR_BUY_NO_UNKNOWN_PRE =  "[0;37;40m";
    private final String MSTR_BUY_NO_UNKNOWN_END =  " is not a known item.";
    private final String MSTR_BUY_NO_PRICE =        "[0;37;40mYou cannot afford ";

    private final String MSTR_SELL_NO_UNKNOWN_PRE = "You don't have ";
    private final String MSTR_SELL_NO_UNKNOWN_END = " to sell!";
    private final String MSTR_SELL_NO_LOC_PRE =     "You cannot sell ";
    private final String MSTR_SELL_NO_LOC_END =     " here.";

    private final String MSTR_BUY_YES_PRE =         "You just bought ";
    private final String MSTR_SELL_YES_PRE =        "You sold ";

    private final String MSTR_BUY_SELL_END =        ".";
    private final String MSTR_BUY_SELL_FOR =        " for ";

    private final String MSTR_ROW_REGEX =           "^\\[32m[a-zA-Z -]+\\[36m[0-9 ]+[^ ].+$";

    private final int POS_CMD_LIST = 0;

    public static String getCmdText(){return "lis\n";}
    public OMUD_MMUD_ParseBlock_CDShop(){
        _arrlCmdText.add(new CmdText("list", 3, CmdText.eDynamicType.STATIC)); // POS_CMD_LIST
        _arrlCmdText.add(new CmdText("sell", 2, CmdText.eDynamicType.DYNAMIC_SPACE));
        _arrlCmdText.add(new CmdText("buy",  2, CmdText.eDynamicType.DYNAMIC_SPACE));
    }

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        // ------------------
        // Shop: YES
        // ------------------
        if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_SHOP_YES, "")) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.SHOP);
            omcd.getMMUDData().dataShop = new OMUD_MMUD_DataBlock_Shop();
            omcd.getMMUDData().dataShop.megaID =   omcd.getMMUDData().dataRoom.megaID;
            omcd.getMMUDData().dataShop.roomName = omcd.getMMUDData().dataRoom.name;

            int pos_left = 0;
            int pos_right = 0;
            String[] tokens = omcd.getParseData().sbBlockDataLeft.toString().split("\n");
            for (String token : tokens){

                // do initial check for valid row data...
                if (token.matches(MSTR_ROW_REGEX)){
                    OMUD_MMUD_DataBlock_Shop.ShopItem item = new OMUD_MMUD_DataBlock_Shop.ShopItem();

                    // ---------------
                    // Item Name
                    // ---------------
                    pos_left = MSTR_ITEM_NAME.length();
                    if ((pos_right = token.indexOf(MSTR_ITEM_QTY, pos_left)) > -1){
                        item.name  = token.substring(pos_left, pos_right).trim();

                        // ---------------
                        // Item Quantity + Price
                        // ---------------
                        pos_left = pos_right + MSTR_ITEM_QTY.length();
                        if ((pos_right = token.indexOf(" ", pos_left)) > -1){
                            item.qty = Integer.parseInt(token.substring(pos_left, pos_right).trim());
                            String strPrice = token.substring(pos_right, token.length()).trim();

                            // check for "can't use"...
                            if ((pos_right = strPrice.lastIndexOf(MSTR_NO_USE, strPrice.length() - 1)) > -1){
                                strPrice = strPrice.substring(0, pos_right);
                                item.can_use = false;
                            }

                            // get price/coins (checks for free): price defaults to zero...
                            if (!strPrice.equals(MSTR_FREE) &&
                                (pos_right = strPrice.indexOf(" ", 0)) > -1){
                                findCoins(item.coins,
                                    Integer.parseInt(strPrice.substring(0, pos_right)),
                                    strPrice.substring(pos_right + 1, strPrice.length()));
                                item.coins.convert(true); // default up-conversion
                            }
                        }
                    }
                    omcd.getMMUDData().dataShop.shop_items.add(item);
                } else break; // break out if something is wrong with the row data
            }

        // ------------------
        // Shop: NO
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_SHOP_NO, "")) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.SHOP);
            omcd.getParseData().arrlDebugText.add("[NO_SHOP_IN_ROOM]\n");

        // ------------------
        // Buy: Success
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_BUY_YES_PRE, MSTR_BUY_SELL_END)) > -1){
            getBuySellData(omcd, "[BUY: ");

        // ------------------
        // Buy: Not Listed
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_BUY_NO_UNKNOWN_PRE, MSTR_BUY_NO_UNKNOWN_END)) > -1){
            omcd.getParseData().arrlDebugText.add("[BUY_NOT_LISTED: " + omcd.getParseData().sbBlockDataLeft + "]\n");

        // ------------------
        // Buy: Can't Afford
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_BUY_NO_PRICE, MSTR_BUY_SELL_END)) > -1){
            omcd.getParseData().arrlDebugText.add("[BUY_CANT_AFFORD: " + omcd.getParseData().sbBlockDataLeft + "]\n");

        // ------------------
        // Sell: Success
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_SELL_YES_PRE, MSTR_BUY_SELL_END)) > -1){
            getBuySellData(omcd, "[SELL: ");

        // ------------------
        // Sell: Don't Have
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_SELL_NO_UNKNOWN_PRE, MSTR_SELL_NO_UNKNOWN_END)) > -1){
            omcd.getParseData().arrlDebugText.add("[SELL_DONT_HAVE: " + omcd.getParseData().sbBlockDataLeft + "]\n");

        // ------------------
        // Sell: Wrong Shop
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_SELL_NO_LOC_PRE, MSTR_SELL_NO_LOC_END)) > -1){
            omcd.getParseData().arrlDebugText.add("[SELL_WRONG_SHOP: " + omcd.getParseData().sbBlockDataLeft + "]\n");

        // ------------------
        // Buy/Sell: No Shop
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_BUY_NO_SHOP, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[BUY_NO_SHOP]\n");
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_SELL_NO_SHOP, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[SELL_NO_SHOP]\n");

        // ------------------
        // Buy/Sell: Bad Syntax
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_BUY_SYNTAX, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[BUY_BAD_SYNTAX]\n");
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_SELL_SYNTAX, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[SELL_BAD_SYNTAX]\n");

        // ------------------
        // Shop List No Response (command was sent without dynamic text and action did nothing)
        // ------------------
        } else if (strCmd.indexOf(" ") == -1 && _arrlCmdText.get(POS_CMD_LIST).text.indexOf(strCmd) > -1 &&
            (pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_NO_RESPONSE, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[NO_RESPONSE_SHOP_LIST_AT_TRAINER]\n");
        }

        return pos_data_left;
    }

    private void getBuySellData(OMUD_Char_Data omcd, String strDebugPrefix){
        int pos_left = -1;
        if ((pos_left = omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_BUY_SELL_FOR)) > -1){
            String item = omcd.getParseData().sbBlockDataLeft.substring(0, pos_left);
            String cost = omcd.getParseData().sbBlockDataLeft.substring(pos_left + MSTR_BUY_SELL_FOR.length(), omcd.getParseData().sbBlockDataLeft.length());
            omcd.getParseData().arrlDebugText.add(strDebugPrefix + item + " (" + cost + ")]\n");
        }
    }
}
