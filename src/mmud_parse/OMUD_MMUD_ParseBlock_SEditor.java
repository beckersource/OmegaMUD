import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_SEditor extends OMUD_MMUD_ParseBlocks.ParseBlock{

    public static String getCmdText(){return "";}
    public OMUD_MMUD_ParseBlock_SEditor(){
        _arrlCmdText.add(new CmdText("train stats", 11, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText("set suicide", 5,  CmdText.eDynamicType.STATIC)); // only "set s" required
        _arrlCmdText.add(new CmdText("reroll",      6,  CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText("x",           1,  CmdText.eDynamicType.STATIC));
    }

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        return -1;
    }
}
