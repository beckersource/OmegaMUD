public class OMUD_MMUD_Parser{
    private static OMUD_MMUD_ParseBlocks _pblocks = new OMUD_MMUD_ParseBlocks();

    private static boolean setAllowSend(boolean allow, String strReason){
        //OMUD_Log.toConsoleDebug("__AS__: " + allow + ": " + strReason);
        return allow;
    }

    private static boolean findMenuCmd(OMUD_Char_Data omcd, char cmd_char_first, boolean allow_send){
        OMUD.eBBSLoc eNewLoc = omcd.eBBSLoc;
        boolean valid_cmd = false;
             if ((valid_cmd = cmd_char_first == 'e'))
            eNewLoc = OMUD.eBBSLoc.MUD_EDITOR;
        else if ((valid_cmd = cmd_char_first == 'x'))
            eNewLoc = OMUD.eBBSLoc.BBS;

        // valid command and was found in telnet data...
        if (eNewLoc != omcd.eBBSLoc){
            omcd.omme.notifyMUDEvent(new OMUD_MUDEvent_BBSLoc((omcd.eBBSLoc = eNewLoc)));
            allow_send = setAllowSend(true, "findMenuCmd()_FOUND");
        // else have a valid command and still waiting for it in the telnet data...
        } else if (valid_cmd){
            allow_send = setAllowSend(false, "findMenuCmd()_NOT_FOUND");
        }

        return allow_send;
    }

    public static boolean parseData(OMUD_Char_Data omcd, String strCmd, int cmds_remaining, boolean smode_cmd){
        OMUD_Char_Data.DataParse dataParse = omcd.getParseData();
        OMUD_Char_Data.DataMMUD  dataMMUD  = omcd.getMMUDData();
        boolean clear_cmds =     cmds_remaining == -1;
        boolean clear_buffer =   clear_cmds;
        boolean allow_send =     true;
        boolean has_cmd =        strCmd.length() > 0;

        // ------------------
        // Inside BBS or MUD
        // ------------------
        if (dataParse.sbBuffer.length() > 0){
            // check for MUD menu: reset data and location...
            if (_pblocks.parseMUDMenu(omcd)){
                dataParse.sbBuffer.setLength(0);
                dataMMUD.reset();
                omcd.omme.notifyMUDEvent(new OMUD_MUDEvent_BBSLoc((omcd.eBBSLoc = OMUD.eBBSLoc.MUD_MENU)));
            // check for pause prompts...
            } else if (_pblocks.parsePause(omcd)){
                omcd.omme.notifyMUDEvent(new OMUD_MUDEvent_SendCmd("\n"));
            }
        }

        // ------------------
        // MUD Menu: Enter/Exit
        // ------------------
        // user sent a command at the BBS MUD menu...
        if (has_cmd && omcd.eBBSLoc == OMUD.eBBSLoc.MUD_MENU)
            allow_send = findMenuCmd(omcd, Character.toLowerCase(strCmd.charAt(0)), allow_send);

        // ------------------
        // Inside MUD: Anywhere
        // ------------------
        if (OMUD.isInsideMUD(omcd.eBBSLoc)){
            boolean render_mud =   false;
            boolean noncmd_loop =  false;
            boolean got_noncmd =   false;
            boolean statline_end = false;
            int pos_statline = -1;
            int pos_data_end =  0;

            // ------------------
            // Inside MUD Game
            // ------------------
            if (omcd.eBBSLoc == OMUD.eBBSLoc.MUD){

                // ------------------
                // Parse Statline & Non-Cmd Loop
                // ------------------
                //OMUD_Log.toConsoleDebug("________");
                do {
                    // statline...
                    if ((pos_statline = _pblocks.parseStatline(omcd)) > -1){
                        // if first statline we found was at the very end of the buffer...
                        if (pos_statline == dataParse.sbBuffer.length())
                            statline_end  = true;
                        // enforce zero pos boundary...
                        if ((pos_data_end = pos_statline - 1) == -1)
                             pos_data_end = 0;
                        render_mud = true;
                    }

                    // non-cmds...
                    if (pos_statline > -1){
                        // check for non-cmds at the found statline location first.
                        // when no statlines are left, do a final attempt for any non-cmd blocks...
                        if ((noncmd_loop = pos_data_end < dataParse.sbBuffer.length() - 1)) // -1 equires more than last char of buffer (check against end statline)
                             noncmd_loop = _pblocks.parseNonCmdBlocks(omcd, pos_data_end, false);
                    }  else  noncmd_loop = _pblocks.parseNonCmdBlocks(omcd, 0, true);
                    if (noncmd_loop){
                        got_noncmd = true;
                        render_mud = true;
                        // some non-cmds don't have a statline, so check if we're past the buffer size...
                        if (pos_data_end >= dataParse.sbBuffer.length() && dataParse.sbBuffer.length() > 0)
                            pos_data_end  = dataParse.sbBuffer.length() - 1;
                    }
                    //if  (pos_statline > -1 || noncmd_loop)
                    //    OMUD_Log.toConsoleDebug("__LOOP__: " + pos_data_end + ", " + dataParse.sbBuffer.length() + ": " + dataParse.sbBuffer);
                } while (pos_statline > -1 || noncmd_loop);

                // ------------------
                // User Cmd Parsing
                // ------------------
                if (has_cmd && !dataParse.hasActiveCmdBlock()){
                    allow_send = setAllowSend(false, "parseData()_CMD_LF");

                    if (!dataMMUD.tmrStatline.isRunning()){
                         dataMMUD.tmrStatline.restart();
                         render_mud = true;
                    }

                    if (smode_cmd){
                        int  pos_lf = -1;
                        if ((pos_lf = OMUD.getNextLF(dataParse.sbBuffer, 0)) > -1 && pos_lf < strCmd.length())
                             strCmd = dataParse.sbBuffer.substring(0, pos_lf + 1);
                    }

                    if (dataParse.sbBuffer.length() >= strCmd.length() &&
                        strCmd.equals(dataParse.sbBuffer.substring(0, strCmd.length()))){

                        // update data pos and main buffer...
                        if (pos_data_end >= strCmd.length())
                            pos_data_end -= strCmd.length();
                        dataParse.sbBuffer.delete(0, strCmd.length());

                        has_cmd = false;

                        // find the command and notify if in editor...
                        StringBuilder sbCmdDetails = new StringBuilder();
                        if (_pblocks.findCmd(dataParse, strCmd.substring(0, strCmd.length() - 1).toLowerCase(), sbCmdDetails) == -1){
                            omcd.omme.notifyMUDEvent(new OMUD_MUDEvent_BBSLoc((omcd.eBBSLoc = OMUD.eBBSLoc.MUD_EDITOR)));
                            allow_send = setAllowSend(true, "parseData()_CMD_EDITOR");
                        }
                        omcd.omme.notifyMUDEvent(new OMUD_MUDEvent_UserCmd(sbCmdDetails.toString(), cmds_remaining));
                        //OMUD_Log.toConsoleDebug("__CMD_YES__: " + strCmd + ": " + dataParse.sbBuffer);
                    }// else OMUD_Log.toConsoleDebug("__CMD_NO__: " + strCmd + ": " + dataParse.sbBuffer);
                }

                // ------------------
                // Check for Mixed Cmd/Non-Cmd
                // ------------------
                if (got_noncmd && dataParse.hasActiveCmdBlock()){
                    // multi-pass if we're in a dynamic block and still have data in the buffer...
                    if (!statline_end && _pblocks.isDynamicCmdBlock(dataParse.getActiveCmdBlock()) && dataParse.sbBuffer.length() > 0){
                        allow_send = setAllowSend(false, "parseData()_MULTI_PASS");
                        pos_data_end = dataParse.sbBuffer.length(); // prevent active cmd check below
                        //OMUD_Log.toConsoleDebug("__MULTI__: " + strCmd + ": " + dataParse.sbBuffer);
                    // if had an active cmd but got a non-cmd result, clear the active cmd...
                    } else if (statline_end && dataParse.sbBuffer.length() == 0){
                        dataParse.clearActiveCmdBlock();
                        //OMUD_Log.toConsoleDebug("__CLR_ACTIVE_EMPTY__: " + strCmd + ": " + dataParse.sbBuffer);
                    }
                }

                // ------------------
                // Parse Cmds & Check for Completion
                // ------------------
                if (dataParse.hasActiveCmdBlock() && pos_data_end < dataParse.sbBuffer.length()){
                    // found matching cmd data...
                    if (_pblocks.parseActiveCmdBlock(omcd, pos_data_end)){
                        dataParse.clearActiveCmdBlock();
                        allow_send = setAllowSend(true, "parseData()_CMD_DATA_OK");
                    // didn't find matching cmd parse block but at the end so
                    // just clear out the buffer and allow send to prevent a statline wait.
                    // (unmatched data will be in mud buffer log)
                    } else if (!got_noncmd && statline_end){
                        dataParse.clearActiveCmdBlock();
                        allow_send = setAllowSend(true, "parseData()_CMD_DATA_END");
                        clear_buffer = true;
                    }
                }

                // ------------------
                // Parse Colored Text & More Send Checks
                // ------------------
                if (!dataParse.hasActiveCmdBlock()){
                    boolean got_colors = false;
                    if ((got_colors = _pblocks.parseColors(omcd)))
                        render_mud = true;

                    if (!has_cmd){
                        if (!allow_send && dataParse.sbBuffer.length() == 0){
                            // this added for bearfather newhaven spam -
                            // comes in split chunks: first without no LF, and can prevent statline...
                            if (got_colors){
                                allow_send = setAllowSend(true, "parseData()_COLORS");
                            // no cmds or data found, allow send...
                            } else if (cmds_remaining == 0){
                                allow_send = setAllowSend(true, "parseData()_NO_CMDS_OR_DATA");
                            }
                        }

                        // no active cmd block and no cmd, found ending statline but still have more data -
                        // just clear the buffer so we can accurately process any future incoming commands.
                        // Seems to happen mostly with dynamic cmd data like "top 100" and mixed combat lines.
                        if (allow_send && statline_end && dataParse.sbBuffer.length() > 0){
                            render_mud = clear_buffer = true;
                            //OMUD_Log.toConsoleDebug("__CLR_BUFFER_END__: " + dataParse.sbBuffer);
                        }
                    }

                } else {
                    allow_send = setAllowSend(false, "parseData()_CMD_ACTIVE");
                }

                // ------------------
                // Timer: Statline
                // ------------------
                if (allow_send || clear_cmds){
                    if (clear_cmds)
                        omcd.omme.notifyMUDEvent(new OMUD_MUDEvent_UserCmd("<CLEAR>", 0));
                    if (dataMMUD.tmrStatline.isRunning())
                        render_mud = true;
                    dataMMUD.tmrStatline.stop();
                } else if (dataMMUD.tmrStatline.isTicked()){
                    dataMMUD.tmrStatline.resetTicked();
                    render_mud = true;

                    // refresh the room daa if timed out...
                    if (dataMMUD.tmrStatline.isAtMax()){
                        dataParse.arrlGetTypes.add(OMUD_MMUD_DataBlock.eBlockType.ROOM);
                        dataMMUD.tmrStatline.stop();
                        dataParse.clearActiveCmdBlock();
                        allow_send = setAllowSend(true, "parseData()_STATLINE_TIMEOUT");
                        clear_buffer = true;
                    }
                }

                // ------------------
                // Timer: Combat
                // ------------------
                if (dataMMUD.tmrCombat.isTicked()){
                    dataMMUD.tmrCombat.resetTicked();
                    render_mud = true;

                    // set as a new round if we had at least a second go by (better round detection)...
                    if (!dataMMUD.dataCombat.new_round && dataMMUD.tmrCombat.getData().seconds > 2)
                         dataMMUD.dataCombat.new_round = true;

                    // stuff for only a timeout...
                    if (dataMMUD.tmrCombat.isAtMax()){
                        // don't change the state if already something other than combat (rest, med, etc)...
                        if (dataMMUD.dataStatline.action_state == OMUD_MMUD_DataBlock_Statline.eActionState.COMBAT)
                            dataMMUD.dataStatline.action_state  = OMUD_MMUD_DataBlock_Statline.eActionState.READY;
                        dataMMUD.tmrCombat.stop();
                    }
                }

            // ------------------
            // Return from Editor
            // ------------------
            } else if (omcd.eBBSLoc == OMUD.eBBSLoc.MUD_EDITOR){
                if ((pos_statline = _pblocks.parseStatline(omcd)) > -1){
                    omcd.omme.notifyMUDEvent(new OMUD_MUDEvent_BBSLoc((omcd.eBBSLoc = OMUD.eBBSLoc.MUD)));
                    render_mud = true;

                    // check for welcome msg...
                    if (!dataMMUD.got_statline && _pblocks.parseEditorReturn(omcd, pos_statline - 1)){
                         dataMMUD.got_statline = true;
                         dataMMUD.is_kai =
                             dataMMUD.dataStatline.ma_str.length() > 0 &&
                            !dataMMUD.dataStatline.ma_str.equals(OMUD_MMUD_DataBlock_Statline.MSTR_SLINE_MA);
                        omcd.omme.notifyMUDEvent(new OMUD_MUDEvent_Welcome(dataMMUD.strWelcome));
                    } else dataParse.sbBuffer.setLength(0); // delete editor buffer data

                    // some basic auto commands on enter - can make manual/auto modes for this later...
                    if (dataMMUD.dataRoom.name.length() == 0)
                        dataParse.arrlGetTypes.add(OMUD_MMUD_DataBlock.eBlockType.ROOM);
                    dataParse.arrlGetTypes.add(OMUD_MMUD_DataBlock.eBlockType.STATS);
                    dataParse.arrlGetTypes.add(OMUD_MMUD_DataBlock.eBlockType.EXP);
                    dataParse.arrlGetTypes.add(OMUD_MMUD_DataBlock.eBlockType.INV);
                    if (dataMMUD.dataStatline.ma_str.length() > 0)
                        dataParse.arrlGetTypes.add(OMUD_MMUD_DataBlock.eBlockType.SPELLS);
                    dataParse.arrlGetTypes.add(OMUD_MMUD_DataBlock.eBlockType.WHO);
                }
            }

            // ------------------
            // Render MUD
            // ------------------
            if (render_mud){
                omcd.renderMMUD();

                if (clear_buffer && dataParse.sbBuffer.length() > 0){
                    OMUD_Log.toFile(OMUD_Log.eLogTypes.MUD_BUFFER,
                        "\n----[DATA LEN: " + dataParse.sbBuffer.length() + "]----\n" + dataParse.sbBuffer + "\n------------------------\n");
                    dataParse.sbBuffer.setLength(0);
                }// else OMUD_Log.toConsoleDebug("__NOTIFY_BUFF__: " + clear_buffer + ", " + dataParse.sbBuffer.length());
            }
        }

        // return true to allow more commands, false to prevent...
        return omcd.eBBSLoc == OMUD.eBBSLoc.BBS ? true : allow_send;
    }
}
