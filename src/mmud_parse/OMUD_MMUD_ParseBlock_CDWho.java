import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_CDWho extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_WHO =         "[0;37;40m[79D[K[1;33m         Current Adventurers\n[1;30m         ===================\n\n";
    private final String MSTR_ROW1_PRE =    "[0m";
    private final String MSTR_ROW_END =     " [37m ";
    private final String MSTR_DELIM_LR =    "  -  [35m";
    private final String MSTR_NAME =        "[0;32m";
    private final String MSTR_TITLE_END =   "[32m";
    private final String MSTR_GUILD =       "  of [33m";

    private final String MSTR_ROW_REGEX =   "^.+\\[0;32m[a-zA-Z -]+\\[35m[a-zA-Z ]+\\[32m[a-z ]+\\[.*37m.*$"; // skips optional align and gang fields

    public static String getCmdText(){return "who\n";}
    public OMUD_MMUD_ParseBlock_CDWho(){
        _arrlCmdText.add(new CmdText("who", 3, CmdText.eDynamicType.STATIC));
    }

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_WHO, "")) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.WHO);
            omcd.getMMUDData().dataWho = new OMUD_MMUD_DataBlock_Who();

            // col1 header won't exist if no results...
            if (omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_ROW1_PRE) == 0){
                omcd.getParseData().sbBlockDataLeft.delete(0, MSTR_ROW1_PRE.length()); // delete first col ansi for consistency with split

                // split rows, get data...
                String[] rows = omcd.getParseData().sbBlockDataLeft.toString().split("\n");
                for (String row: rows){
                    if (row.matches(MSTR_ROW_REGEX)){
                        int pos_name_left =     -1;
                        int pos_name_end =      -1;
                        int pos_title_end =     -1;
                        int pos_guild_left =    -1;
                        int pos_row_end =       -1;

                        // validate row...
                        if ((pos_name_left = row.indexOf(MSTR_NAME)) > -1 &&
                            (pos_row_end   = row.lastIndexOf(MSTR_ROW_END, row.length() - 1)) > -1){
                            row = row.substring(0, pos_row_end);

                            // ---------------
                            // Left/Right Sections
                            // ---------------
                            if ((pos_name_end = row.indexOf(MSTR_DELIM_LR)) > -1){
                                String strLeft =  row.substring(0, pos_name_end);
                                String strRight = row.substring(pos_name_end + MSTR_DELIM_LR.length(), row.length());

                                OMUD_MMUD_DataBlock_Who.WhoChar whoChar = new OMUD_MMUD_DataBlock_Who.WhoChar();

                                // ---------------
                                // Char Alignment + First/Last Name
                                // ---------------
                                whoChar.name_first = strLeft.substring(pos_name_left + MSTR_NAME.length(), pos_name_end).trim();
                                String[] tokens = whoChar.name_first.split(" ");
                                if (tokens.length == 2){
                                    whoChar.name_first = tokens[0];
                                    whoChar.name_last =  tokens[1];
                                }

                                // if title is non-neutral, more data to the left (skip the pre-space)...
                                String strAlign = strLeft.substring(0, pos_name_left).trim();
                                if (strAlign.length() > 0){
                                    for (int i = OMUD_MMUD_DataBlock_Who.eAlignment.NEUTRAL.ordinal() + 1; i < OMUD_MMUD_DataBlock_Who.ALIGNMENT_STRINGS.length; ++i)
                                        if (strAlign.lastIndexOf(OMUD_MMUD_DataBlock_Who.ALIGNMENT_STRINGS[i], strAlign.length() - 1) > -1){
                                            whoChar.alignment = OMUD_MMUD_DataBlock_Who.eAlignment.values()[i];
                                            break;
                                        }
                                }

                                // ---------------
                                // Char Title + Guild
                                // ---------------
                                if ((pos_title_end = strRight.indexOf(MSTR_TITLE_END)) > -1){
                                    whoChar.title  = strRight.substring(0, pos_title_end);

                                    if ((pos_guild_left = strRight.indexOf(MSTR_GUILD, pos_title_end)) > -1)
                                        whoChar.guild = strRight.substring(pos_guild_left + MSTR_GUILD.length(), strRight.length());
                                }

                                omcd.getMMUDData().dataWho.chars.add(whoChar);
                            }
                        }
                    } else break; // break out if something is wrong with the row data
                }
            }
        }

        return pos_data_left;
    }
}
