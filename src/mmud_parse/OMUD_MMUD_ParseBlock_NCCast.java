import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_NCCast extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_OPT_RESET_WHBL =  "[0;37;40m";
    private final String MSTR_CAST =            "[79D[K[1;34m";
    private final String MSTR_CAST_YOU =        "You cast ";
    private final String MSTR_CAST_THEY =       " casts ";
    private final String MSTR_CAST_ON_TGT =     " on ";

    private final String MSTR_FAIL_YOU_PRE =    "[79D[K[0;36mYou attempt to cast ";
    private final String MSTR_FAIL_YOU_END =    ", but fail.";

    private final String MSTR_BAD_TGT_PRE =     "[1;31;40mYou do not see ";
    private final String MSTR_BAD_TGT_END =     " here!";

    private final String MSTR_NEED_TGT =        "[79D[K[1;31;40mYou must specify a target for that spell!";
    private final String MSTR_ALREADY_CAST =    "You have already cast a spell this round!";

    private final String MSTR_YOU_CAPS =        "YOU";

    public static String getCmdText(){return "";}
    public OMUD_MMUD_ParseBlock_NCCast(){}

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        // ------------------
        // Cast: Successful/Result
        // ------------------
        if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_CAST, "")) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.COMBAT);

            pos_data_left = removeOptPrefix(omcd, "Bought Healing from Healer", pos_data_left, MSTR_OPT_RESET_WHBL);

            OMUD_MMUD_DataBlock_Combat.CombatLine clOne = findCastText(omcd.getParseData().sbBlockDataLeft);
            OMUD_MMUD_DataBlock_Combat.CombatLine clTwo = null;

            // non-cast text line - check for a 2nd effect text line...
            if (clOne != null){
                // check if the next line is a spell effect line...
                int pos_effect_right = -1;
                if (omcd.getParseData().sbBuffer.length() >= pos_data_left + MSTR_CAST.length() &&
                    omcd.getParseData().sbBuffer.substring(pos_data_left, pos_data_left + MSTR_CAST.length()).equals(MSTR_CAST) &&
                    (pos_effect_right = OMUD.getNextLF(omcd.getParseData().sbBuffer, pos_data_left)) > -1 &&
                    findData(omcd.getParseData(), pos_data_left, pos_effect_right, true, MSTR_CAST, "") > -1){

                    cleanData(omcd.getParseData().sbBlockDataLeft, true, false);
                    clTwo = new OMUD_MMUD_DataBlock_Combat.CombatLine(OMUD_MMUD_DataBlock_Combat.eLineType.CAST_TEXT);
                    clTwo.unit =        new OMUD_MMUD_DataUnit(MSTR_YOU_CAPS);
                    clTwo.unit_action = omcd.getParseData().sbBlockDataLeft.toString();
                }
            } else {
                clOne = new OMUD_MMUD_DataBlock_Combat.CombatLine(OMUD_MMUD_DataBlock_Combat.eLineType.CAST_TEXT);
                clOne.unit =        new OMUD_MMUD_DataUnit(MSTR_YOU_CAPS);
                clOne.unit_action = omcd.getParseData().sbBlockDataLeft.toString();
            }

            omcd.getMMUDData().dataCombat.arrlLines.add(clOne);
            if (clTwo != null)
                omcd.getMMUDData().dataCombat.arrlLines.add(clTwo);

        // ------------------
        // Cast: Failure
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_FAIL_YOU_PRE, MSTR_FAIL_YOU_END)) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.COMBAT);
            cleanData(omcd.getParseData().sbBlockDataLeft, true, false);

            OMUD_MMUD_DataBlock_Combat.CombatLine cl = new OMUD_MMUD_DataBlock_Combat.CombatLine(OMUD_MMUD_DataBlock_Combat.eLineType.CAST_FAIL);
            cl.unit =        new OMUD_MMUD_DataUnit(MSTR_YOU_CAPS);
            cl.unit_action = omcd.getParseData().sbBlockDataLeft.toString();
            omcd.getMMUDData().dataCombat.arrlLines.add(cl);

        // ------------------
        // Already Cast
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_ALREADY_CAST, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[MSTR_ALREADY_CAST]\n");

        // ------------------
        // Combat: Invalid / Specify Target
        // ------------------
        } else if (
            (pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_BAD_TGT_PRE, MSTR_BAD_TGT_END)) > -1 ||
            (pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_NEED_TGT, "")) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.COMBAT);
            cleanData(omcd.getParseData().sbBlockDataLeft, true, false);

            OMUD_MMUD_DataBlock_Combat.CombatLine cl = new OMUD_MMUD_DataBlock_Combat.CombatLine(OMUD_MMUD_DataBlock_Combat.eLineType.CAST_INVALID);
            cl.unit =      new OMUD_MMUD_DataUnit(MSTR_YOU_CAPS);
            cl.tgt_name = omcd.getParseData().sbBlockDataLeft.toString(); // empty on "need target"
            omcd.getMMUDData().dataCombat.arrlLines.add(cl);
        }

        return pos_data_left;
    }

    private OMUD_MMUD_DataBlock_Combat.CombatLine findCastText(StringBuilder sbBlockDataLeft){
        OMUD_MMUD_DataBlock_Combat.CombatLine cl = null;
        cleanData(sbBlockDataLeft, true, false);

         // I cast on someone...
        int pos_left  = -1;
        int pos_right = sbBlockDataLeft.lastIndexOf(MSTR_CAST_ON_TGT, sbBlockDataLeft.length() - 1);

        if ((pos_left = sbBlockDataLeft.indexOf(MSTR_CAST_YOU)) == 0){
            cl =      new OMUD_MMUD_DataBlock_Combat.CombatLine(OMUD_MMUD_DataBlock_Combat.eLineType.CAST_SUCCESS);
            cl.unit = new OMUD_MMUD_DataUnit(MSTR_YOU_CAPS);
            cl.unit_action = sbBlockDataLeft.substring(MSTR_CAST_YOU.length(), pos_right > -1 ? pos_right : sbBlockDataLeft.length() - 1); // -1 skip trailing punctuation

        // they cast on someone...
        } else if ((pos_left = sbBlockDataLeft.indexOf(MSTR_CAST_THEY)) > -1){
            cl =        new OMUD_MMUD_DataBlock_Combat.CombatLine(OMUD_MMUD_DataBlock_Combat.eLineType.CAST_SUCCESS);
            cl.unit =   new OMUD_MMUD_DataUnit(sbBlockDataLeft.substring(0, pos_left));
            cl.unit_action = sbBlockDataLeft.substring(pos_left + MSTR_CAST_THEY.length(), pos_right > -1 ? pos_right : sbBlockDataLeft.length() - 1); // -1 skip trailing punctuation
        }

        if (cl != null && pos_right > -1)
            cl.tgt_name = sbBlockDataLeft.substring(pos_right + MSTR_CAST_ON_TGT.length(), sbBlockDataLeft.length() - 1); // -1 skip trailing punctuation

        return cl;
    }
}
