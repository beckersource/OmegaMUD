import java.util.ArrayList;

public class OMUD_MMUD_ParseBlocks{
    // ------------------
    // CmdBlock
    // ------------------
    public static class CmdBlock{
        public int      cmdblock_pos =  -1;
        public String   strCmd =        "";
        public CmdBlock(int cbp, String cmd){
            cmdblock_pos =  cbp;
            strCmd =        cmd;
        }
    }

    // ------------------
    // ParseBlock
    // ------------------
    protected static abstract class ParseBlock{
        protected ArrayList<CmdText> _arrlCmdText = new ArrayList<CmdText>();
        protected static class CmdText{
            public enum eDynamicType{
                STATIC,
                DYNAMIC_SPACE,
                DYNAMIC_CHAR
            }
            public String   text  =     "";
            public int      min_len =   0;
            public eDynamicType dynamic_type = eDynamicType.STATIC;
            public CmdText(String t, int ml, eDynamicType dt){
                text =          t;
                min_len =       ml;
                dynamic_type =  dt;
            }
        }

        public StringBuilder matchCmdText(String strCmd){
            int i = 0;
            boolean found = false;
            while (i < _arrlCmdText.size() && !found){
                found = strCmd.length() >= _arrlCmdText.get(i).min_len; // initial range check
                for (int j = 0; j < strCmd.length() && found; ++j){

                    // match within cmd char length...
                    if (j < _arrlCmdText.get(i).text.length()){

                        // not at end yet and not a match:  check for dynamic cmd...
                        if (!(found = strCmd.charAt(j) == _arrlCmdText.get(i).text.charAt(j))){
                            int len = j + 1;
                            found =
                                _arrlCmdText.get(i).dynamic_type == CmdText.eDynamicType.DYNAMIC_SPACE &&
                                strCmd.charAt(j) == OMUD.ASCII_SPC &&
                                len > _arrlCmdText.get(i).min_len && len < strCmd.length();
                            break;
                        }
                    // matched all cmd chars and still going: check for dynamic cmd...
                    } else {
                        found =
                            (_arrlCmdText.get(i).dynamic_type == CmdText.eDynamicType.DYNAMIC_SPACE && strCmd.charAt(j) == OMUD.ASCII_SPC) ||
                            (_arrlCmdText.get(i).dynamic_type == CmdText.eDynamicType.DYNAMIC_CHAR  && strCmd.charAt(j) != OMUD.ASCII_SPC);
                        break;
                    }
                }

                if (!found)
                    i++;
            }
            return found ? new StringBuilder(_arrlCmdText.get(i).text) : null;
        }

        public abstract int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd);
        protected int findData(OMUD_Char_Data.DataParse dataParse, int pos_begin, int pos_end, boolean dynamic_data, String strStartSeq, String strMidSeq){
            int pos_startseq_left =  -1;
            int pos_startseq_right = -1;
            int pos_midseq_left =    -1;
            int pos_midseq_right =   -1;
            int pos_data_left =      -1;
            boolean valid_range =    strStartSeq.length() > 0 && dataParse.sbBuffer.length() >= strStartSeq.length() + strMidSeq.length();

            dataParse.sbBlockDataLeft.setLength(0);
            dataParse.sbBlockDataRight.setLength(0);

            // basic buffer range check...
            if (valid_range){

                // get start sequence bounds...
                if ((pos_startseq_left  = dataParse.sbBuffer.indexOf(strStartSeq, pos_begin)) > -1 && pos_startseq_left < pos_end){
                     pos_startseq_right = pos_startseq_left + strStartSeq.length() - 1;

                    // mid sequence check (optional)...
                    if (strMidSeq.length() > 0){
                        if ((pos_midseq_left  = dataParse.sbBuffer.indexOf(strMidSeq, pos_startseq_right + 1)) > -1)
                             pos_midseq_right = pos_midseq_left + strMidSeq.length() - 1;
                    // else no mid-seq...
                    } else pos_midseq_left = pos_end + 1;

                    boolean has_dynamic_data_one = dynamic_data &&
                        pos_startseq_right > -1 && pos_startseq_right + 1 < pos_midseq_left;
                    boolean has_dynamic_data_two = has_dynamic_data_one &&
                        pos_midseq_right   > -1 && pos_midseq_right   + 1 < pos_end;

                    //if (strStartSeq.equals("[0;37;40m[79D[K") && strMidSeq.equals(" auctions: [0;35m") && has_dynamic_data_one)
                    //    OMUD_Log.toConsoleDebug("__FD__:" + has_dynamic_data_one + ", " + has_dynamic_data_two + ", " + pos_begin + ", " + pos_end + ", " + pos_startseq_left + ", " + pos_startseq_right + ", " + pos_midseq_left + ", " + pos_midseq_right + ":\n" + dataParse.sbBuffer);

                    // get data if dynamic or finish up for non-dynamic...
                    if (!dynamic_data || has_dynamic_data_one){

                        if (has_dynamic_data_one)
                            dataParse.sbBlockDataLeft.append(dataParse.sbBuffer.substring(pos_startseq_right + 1, pos_midseq_left));
                        if (has_dynamic_data_two)
                            dataParse.sbBlockDataRight.append(dataParse.sbBuffer.substring(pos_midseq_right + 1, pos_end + 1)); // +1 for exclusive

                        // delete the data in the buffer...
                        dataParse.sbBuffer.delete(pos_startseq_left, pos_end + 1); // +1 for exclusive
                        pos_data_left = pos_startseq_left;
                    }
                }
            }
            return pos_data_left;
        }

        // cleanData(): trim lf/space and strip ansi as requested...
        protected void cleanData(StringBuilder sbData, boolean trim_lf_spc, boolean strip_ansi){
            int pos_first_char = -1;
            int pos_last_char  = -1;
            for (int i = 0; i < sbData.length(); ++i){
                if (strip_ansi && sbData.charAt(i) == OMUD.ASCII_ESC){
                    int pos_ansi_end = sbData.indexOf(OMUD.CSI_GRAPHICS_STR, i);
                    // if matching ansi end is not found, just delete the escape char.
                    if (pos_ansi_end == -1)
                        pos_ansi_end = i + 1;   // +1 for exclusive end
                    else pos_ansi_end++;        //
                    sbData.delete(i--, pos_ansi_end); // move 'i' back after delete so that we pick up the first char after the delete

                } else if (trim_lf_spc){
                    if (sbData.charAt(i) == OMUD.ASCII_LF){
                        sbData.setCharAt(i, OMUD.ASCII_SPC);
                    } else if (sbData.charAt(i) != OMUD.ASCII_SPC){
                        pos_last_char = i;
                        if (pos_first_char == -1)
                            pos_first_char = i;
                    }
                }
            }

            // trim: delete end first...
            if (trim_lf_spc){
                if (pos_last_char + 1 <= sbData.length() - 1)
                    sbData.delete(pos_last_char + 1, sbData.length()); // +1 to move forward to begin at space after last char
                if (pos_first_char > 0)
                    sbData.delete(0, pos_first_char);
            }
        }

        // removeOptPrefix(): some mud strings have an optional/possible prefix based on certain situations - check for it and remove it -
        // NOTE: strDbgReason is just for internal visual/coding reference.  Not a string required for function.
        protected int removeOptPrefix(OMUD_Char_Data omcd, String strDbgReason, int pos_offset, String strPrefix){
            int prefix_len = strPrefix.length();
            int pos_start = pos_offset - prefix_len;
            if (pos_start >= 0 && omcd.getParseData().sbBuffer.substring(pos_start, pos_start + prefix_len).equals(strPrefix)){
                omcd.getParseData().sbBuffer.delete(pos_start, pos_start + prefix_len);
                pos_offset = pos_start;

                //if (strDbgReason.length() > 0)
                //    omcd.getParseData().arrlDebugText.add("ParseBlock Prefix Found/Removed: " + strDbgReason + "\n");
            }
            return pos_offset;
        }

        protected void splitCommaListToArray(String strItems, ArrayList<String> arrlItems){
            String[] items = strItems.split(",");
            for (String item : items)
                arrlItems.add(item.trim());
        }

        protected boolean findCoins(OMUD_MMUD_DataCoins coins, int qty, String strText){
            boolean coin_match = false;
                 if ((coin_match = (strText.indexOf(OMUD_MMUD_DataCoins.COIN_ITEM_STRINGS[OMUD_MMUD_DataCoins.eCoinType.RUNIC.ordinal()], 0))      > -1))
                coins.runic =   qty;
            else if ((coin_match = (strText.indexOf(OMUD_MMUD_DataCoins.COIN_ITEM_STRINGS[OMUD_MMUD_DataCoins.eCoinType.PLATINUM.ordinal()], 0))   > -1))
                coins.plat =    qty;
            else if ((coin_match = (strText.indexOf(OMUD_MMUD_DataCoins.COIN_ITEM_STRINGS[OMUD_MMUD_DataCoins.eCoinType.GOLD.ordinal()], 0))       > -1))
                coins.gold =    qty;
            else if ((coin_match = (strText.indexOf(OMUD_MMUD_DataCoins.COIN_ITEM_STRINGS[OMUD_MMUD_DataCoins.eCoinType.SILVER.ordinal()], 0))     > -1))
                coins.silver =  qty;
            else if ((coin_match = (strText.indexOf(OMUD_MMUD_DataCoins.COIN_ITEM_STRINGS[OMUD_MMUD_DataCoins.eCoinType.COPPER.ordinal()], 0))     > -1))
                coins.copper =  qty;
            return coin_match;
        }

        protected void buildItems(ArrayList<String> arrlNew, ArrayList<OMUD_MMUD_DataItem> arrlItems, OMUD_MMUD_DataCoins coins, ArrayList<OMUD_MMUD_DataItem> arrlItemsEQ){
            for (int i = 0; i < arrlNew.size(); ++i){
                OMUD_MMUD_DataItem ri = new OMUD_MMUD_DataItem(arrlNew.get(i));

                boolean is_coin = false;
                boolean is_dupe = false;

                // if inventory call (equipped non-null), set equipped items...
                if (arrlItemsEQ != null && ri.name.charAt(ri.name.length() - 1) == ')'){
                    int pos_slot_start = -1;
                    for (int j = OMUD_MMUD_DataItem.eItemLoc.WEAPON.ordinal(); j < OMUD_MMUD_DataItem.EQUIP_SLOT_STRINGS.length && pos_slot_start == -1; ++j)
                        if ((pos_slot_start = ri.name.lastIndexOf(OMUD_MMUD_DataItem.EQUIP_SLOT_STRINGS[j], ri.name.length() - 1)) > -1){
                            ri.item_loc = OMUD_MMUD_DataItem.eItemLoc.values()[j];
                            ri.name = ri.name.substring(0, pos_slot_start - 1); // remove slot and trailing space (-1)
                            arrlItemsEQ.add(ri);
                        }
                }

                // non-equipped: check for quantity prefixes and dupes...
                if (ri.item_loc == OMUD_MMUD_DataItem.eItemLoc.NONE){

                    // check for quantity prefix...
                    int pos_left = -1;
                    if ((pos_left = ri.name.indexOf(" ", 0)) > -1){
                        boolean is_number = true;

                        String strFirstWord = ri.name.substring(0, pos_left);
                        for (int j = 0; j < strFirstWord.length() && is_number; ++j)
                            is_number = strFirstWord.charAt(j) >= 48 && strFirstWord.charAt(j) <= 57; // 0-9 ascii

                        if (is_number){
                            ri.qty =  Integer.parseInt(strFirstWord);
                            ri.name = ri.name.substring(pos_left + 1, ri.name.length());

                            // coin item names always have two words, so check for another space first for efficiency and
                            // then coin names - coins are always displayed first and in order,
                            // so can exit early if out-of-range or if copper is found...
                            if (ri.name.indexOf(" ", 0) > -1)
                                for (int j = 0; j < OMUD_MMUD_DataCoins.COIN_ITEM_STRINGS.length && coins.copper == 0; ++j)
                                    is_coin = findCoins(coins, ri.qty, ri.name);
                        }
                    }

                    // check for existing and update quantities...
                    if (!is_coin){
                        for (int j = 0; j < arrlItems.size() && !is_dupe; ++j)
                            if ((is_dupe = ri.name.equals(arrlItems.get(j).name))){
                                // inventory: equipped item with extra non-equipped in inv -
                                // keep the counter on the original equipped item
                                if (arrlItems.get(j).item_loc != OMUD_MMUD_DataItem.eItemLoc.NONE)
                                    arrlItems.get(j).qty = ri.qty + 1;
                                // else this is from a search and the greater quantity should be updated
                                // based on the search results...
                                else if (ri.qty > arrlItems.get(j).qty)
                                    arrlItems.get(j).qty = ri.qty;
                            }
                    }
                }

                if (!is_coin && !is_dupe)
                    arrlItems.add(ri);
            }
        }

        protected void updateCombatState(OMUD_Char_Data.DataMMUD dataMMUD, boolean is_combat_engaged, boolean is_combat_off){
            // manually restart the combat timer on a new combat round -
            // should be called when combat lines (swings, etc) are found but not engaged/off...
            if (!is_combat_engaged && !is_combat_off)
                dataMMUD.tmrCombat.restart();

            if (!is_combat_off)
                dataMMUD.dataStatline.action_state = OMUD_MMUD_DataBlock_Statline.eActionState.COMBAT;
            else if (dataMMUD.dataStatline.action_state == OMUD_MMUD_DataBlock_Statline.eActionState.COMBAT)
                dataMMUD.dataStatline.action_state = OMUD_MMUD_DataBlock_Statline.eActionState.READY;
        }
    }

    // ------------------
    // OMUD_MMUD_ParseBlocks
    // ------------------
    private static ArrayList<ParseBlock>           _arrlCmdBlocks =            null;
    private static ArrayList<ParseBlock>           _arrlNonCmdBlocks =         null;
    private static OMUD_MMUD_ParseBlock_SStatline  _blkStatline =              null;
    private static OMUD_MMUD_ParseBlock_SMenu      _blkMUDMenu =               null;
    private static OMUD_MMUD_ParseBlock_SPause     _blkPause =                 null;
    private static OMUD_MMUD_ParseBlock_SColors    _blkColors =                null;
    private static OMUD_MMUD_ParseBlock_SEditor    _blkEditor =                null;
    private static int POS_BLOCK_CMD_ROOM =     0;
    private static int POS_BLOCK_CMD_DYNAMICS = 0;

    public OMUD_MMUD_ParseBlocks(){
        // command blocks: static layout...
        _arrlCmdBlocks = new ArrayList<ParseBlock>();
        POS_BLOCK_CMD_ROOM = _arrlCmdBlocks.size();
        _arrlCmdBlocks.add(new OMUD_MMUD_ParseBlock_CSRoom());
        _arrlCmdBlocks.add(new OMUD_MMUD_ParseBlock_CSCombat());
        _arrlCmdBlocks.add(new OMUD_MMUD_ParseBlock_CSParty());
        _arrlCmdBlocks.add(new OMUD_MMUD_ParseBlock_CSChat());
        _arrlCmdBlocks.add(new OMUD_MMUD_ParseBlock_CSExp());
        _arrlCmdBlocks.add(new OMUD_MMUD_ParseBlock_CSInv());
        _arrlCmdBlocks.add(new OMUD_MMUD_ParseBlock_CSStats());
        // command blocks: dynamic layout (rows)...
        POS_BLOCK_CMD_DYNAMICS = _arrlCmdBlocks.size();
        _arrlCmdBlocks.add(new OMUD_MMUD_ParseBlock_CDShop());
        _arrlCmdBlocks.add(new OMUD_MMUD_ParseBlock_CDSpells());
        _arrlCmdBlocks.add(new OMUD_MMUD_ParseBlock_CDWho());
        _arrlCmdBlocks.add(new OMUD_MMUD_ParseBlock_CDTop());

        // non-command blocks...
        _arrlNonCmdBlocks = new ArrayList<ParseBlock>();
        _arrlNonCmdBlocks.add(new OMUD_MMUD_ParseBlock_NCCombat());
        _arrlNonCmdBlocks.add(new OMUD_MMUD_ParseBlock_NCCast());
        _arrlNonCmdBlocks.add(new OMUD_MMUD_ParseBlock_NCChat());
        _arrlNonCmdBlocks.add(new OMUD_MMUD_ParseBlock_NCParty());
        _arrlNonCmdBlocks.add(new OMUD_MMUD_ParseBlock_NCRoom());
        _arrlNonCmdBlocks.add(new OMUD_MMUD_ParseBlock_NCOther());

        // special blocks...
        _blkStatline =  new OMUD_MMUD_ParseBlock_SStatline();
        _blkMUDMenu =   new OMUD_MMUD_ParseBlock_SMenu();
        _blkPause =     new OMUD_MMUD_ParseBlock_SPause();
        _blkColors =    new OMUD_MMUD_ParseBlock_SColors();
        _blkEditor =    new OMUD_MMUD_ParseBlock_SEditor();
    }

    public static boolean parseActiveCmdBlock(OMUD_Char_Data omcd, int pos_data_right){
        return _arrlCmdBlocks.get(omcd.getParseData().getActiveCmdBlock().cmdblock_pos).findBlockData(omcd, 0, pos_data_right, omcd.getParseData().getActiveCmdBlock().strCmd) > -1;
    }

    public static boolean parseNonCmdBlocks(OMUD_Char_Data omcd, int pos_data_left, boolean entire_buffer){
        return parseLFBlocks(omcd, pos_data_left, entire_buffer, _arrlNonCmdBlocks);
    }

    public boolean parseColors(OMUD_Char_Data omcd){
        return parseLFBlocks(omcd, 0, true, null);
    }

    public static int parseStatline(OMUD_Char_Data omcd){
        return _blkStatline.findBlockData(omcd, -1, -1, "");
    }

    public static boolean parseMUDMenu(OMUD_Char_Data omcd){
        return _blkMUDMenu.findBlockData(omcd, -1, -1, "") > -1;
    }

    public static boolean parsePause(OMUD_Char_Data omcd){
        return _blkPause.findBlockData(omcd, -1, -1, "") > -1;
    }

    // parseEditorReturn(): parse the room block for room data / welcome message.
    public static boolean parseEditorReturn(OMUD_Char_Data omcd, int pos_data_right){
        return omcd.getParseData().sbBuffer.length() > 0 && _arrlCmdBlocks.get(POS_BLOCK_CMD_ROOM).findBlockData(omcd, 0, pos_data_right, "") > -1;
    }

    private static boolean parseLFBlocks(OMUD_Char_Data omcd, int pos_data_left, boolean entire_buffer, ArrayList<ParseBlock> arrlBlocks){
        boolean got_data =  false;
        int pos_data_lf =  -1;
        while (omcd.getParseData().sbBuffer.length() > 0 && (pos_data_lf = OMUD.getNextLF(omcd.getParseData().sbBuffer, pos_data_left)) > -1){
            if (parseBlocks(omcd, pos_data_left, pos_data_lf, arrlBlocks))
                 got_data = true;
            else pos_data_left = pos_data_lf + 1;

            if (!entire_buffer)
                break;
        }
        return got_data;
    }

    private static boolean parseBlocks(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, ArrayList<ParseBlock> arrlBlocks){
        boolean got_data = false;
        if (arrlBlocks != null){
            for (int i = 0; i < arrlBlocks.size() && !got_data; ++i)
                got_data = arrlBlocks.get(i).findBlockData(omcd, pos_data_left, pos_data_right, "") > -1;
        } else  got_data =_blkColors.findBlockData(omcd, pos_data_left, pos_data_right, "") > -1;
        return  got_data;
    }

    // findCmd(): main external call to match a user-input command (assumes passed in as lower-case).
    // return values: 1 on non-editor, 0 for not found, -1 for editor.
    public static int findCmd(OMUD_Char_Data.DataParse dataParse, String strCmd, StringBuilder sbCmdDetails){
        int result = 1;

        StringBuilder sbCmdMatch = null; // null is used for comparison
        for (int i = 0; i < _arrlCmdBlocks.size() && sbCmdMatch == null; ++i)
            if ((sbCmdMatch = _arrlCmdBlocks.get(i).matchCmdText(strCmd)) != null)
                dataParse.setActiveCmdBlock(new CmdBlock(i, strCmd));

        // check for an editor cmd - return true if inside editor...
        if (sbCmdMatch == null && (sbCmdMatch = _blkEditor.matchCmdText(strCmd)) != null)
            result = -1;
        if (sbCmdMatch == null)
            result = 0;

        // get the full descriptive/display version of the command -
        // can be null, so check...
        if (sbCmdDetails != null){
            sbCmdDetails.setLength(0);
            sbCmdDetails.append(
                (strCmd.length() == 0 ? "<ENTER>" : strCmd) + // special case for "room" enter/look
                " (" + (sbCmdMatch != null ? sbCmdMatch : "?") + ")");
        }

        return result;
    }

    public static boolean isDynamicCmdBlock(OMUD_MMUD_ParseBlocks.CmdBlock cb){
        return cb != null ? cb.cmdblock_pos >= POS_BLOCK_CMD_DYNAMICS : false;
    }
}
