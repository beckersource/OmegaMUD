import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_NCChat extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_GOS_AUC_PRE =         "[0;37;40m[79D[K";
    private final String MSTR_GANG_PRE =            "[0;37;40m[79D[K[0;32m";
    private final String MSTR_GOS_MID =             " gossips: [0;35m";
    private final String MSTR_AUC_MID =             " auctions: [0;35m";
    private final String MSTR_GANG_MID =            " gangpaths: [0;33m";

    private final String MSTR_TEL_PRE =             "[79D[K[32m";
    private final String MSTR_TEL_MID =             " telepaths: [0;37m";

    private final String MSTR_ROOM_SAY_ME =         "[0;32mYou say ";
    private final String MSTR_ROOM_YELL_ME =        "[0;32mYou yell ";

    private final String MSTR_ROOM_SAY_THEM_PRE =   "[79D[K[0;32m";
    private final String MSTR_ROOM_SAY_THEM_MID =   " says ";

    private final String MSTR_ROOM_YELL_THEM_PRE =  "[79D[K[0;32mFrom ";
    private final String MSTR_ROOM_YELL_THEM_MID =  " (Yelling): ";
    private final String MSTR_ROOM_YELL_ADJ =       "[79D[K[0;32mSomeone yells from the "; // full direction text is first word after (northwest), then chat text

    // NOTE: both say and yell chat text are wrapped in double quotes
    // NOTE: directed say chat puts user name like this: Char1 says (to Char2) "yomamasofat" && Char1 says (to you) "yomamasofat"

    public static String getCmdText(){return "";}
    public OMUD_MMUD_ParseBlock_NCChat(){}

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        // -----------------------
        // Gos/Auc/Gang/Telepath
        // -----------------------
               if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_GOS_AUC_PRE,   MSTR_GOS_MID))  > -1){
            addChatData(omcd, OMUD_MMUD_DataBlock_Chat.eChatType.GOSSIP, false, "");
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_GOS_AUC_PRE,   MSTR_AUC_MID))  > -1){
            addChatData(omcd, OMUD_MMUD_DataBlock_Chat.eChatType.AUCTION, false, "");
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_GANG_PRE,      MSTR_GANG_MID)) > -1){
            addChatData(omcd, OMUD_MMUD_DataBlock_Chat.eChatType.GANG, false, "");
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_TEL_PRE,       MSTR_TEL_MID))  > -1){
            addChatData(omcd, OMUD_MMUD_DataBlock_Chat.eChatType.TELEPATH, false, omcd.getMMUDData().dataStats.name_first);

        // -----------------------
        // Me Say or Yell (to room)
        // -----------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_ROOM_SAY_ME,  "")) > -1){
            addChatData(omcd, OMUD_MMUD_DataBlock_Chat.eChatType.SAY_ROOM, true, "");
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_ROOM_YELL_ME, "")) > -1){
            addChatData(omcd, OMUD_MMUD_DataBlock_Chat.eChatType.YELL_ROOM, true, "");

        // -----------------------
        // Them Yell (this Room)
        // -----------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_ROOM_YELL_THEM_PRE,  MSTR_ROOM_YELL_THEM_MID)) > -1){
            addChatData(omcd, OMUD_MMUD_DataBlock_Chat.eChatType.YELL_ROOM, true, "");

        // -----------------------
        // Them Say (and Directed)
        // -----------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_ROOM_SAY_THEM_PRE,  MSTR_ROOM_SAY_THEM_MID)) > -1){
            String target = "";
            OMUD_MMUD_DataBlock_Chat.eChatType type = OMUD_MMUD_DataBlock_Chat.eChatType.SAY_ROOM;

            // check for directed say...
            if (omcd.getParseData().sbBlockDataRight.charAt(0) == '('){
                int pos_name_end = -1;
                if ((pos_name_end = omcd.getParseData().sbBlockDataRight.indexOf(")", 1)) > -1){
                    target = omcd.getParseData().sbBlockDataRight.substring("(to ".length(), pos_name_end);
                    type = OMUD_MMUD_DataBlock_Chat.eChatType.SAY_DIR;
                    omcd.getParseData().sbBlockDataRight.delete(0, pos_name_end + 2); // +1 exclusives: delete closing paren, space
                }
            }

            addChatData(omcd, type, true, target);

        // -----------------------
        // Them Yell (Adjacent Room)
        // -----------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_ROOM_YELL_ADJ,  "")) > -1){
            // get the direction yelled...
            int pos_dir_end = omcd.getParseData().sbBlockDataLeft.indexOf(" ", 0);
            StringBuilder sbYellDir = new StringBuilder(omcd.getParseData().sbBlockDataLeft.substring(0, pos_dir_end));
            cleanData(sbYellDir, false, true);
            omcd.getParseData().sbBlockDataLeft.delete(0, pos_dir_end + 1); // remove the dir prefix for below

            // set data2 to the message
            // set data1 to ?DIR as the name and yell dir (shows as 'Someone' in the game)
            omcd.getParseData().sbBlockDataRight.append(omcd.getParseData().sbBlockDataLeft);
            omcd.getParseData().sbBlockDataLeft.setLength(0);
            omcd.getParseData().sbBlockDataLeft.append("?");

            // create the new chat, add the yell direction after...
            addChatData(omcd, OMUD_MMUD_DataBlock_Chat.eChatType.YELL_ADJ, true, "");
            for (int i = 0; i < OMUD_MMUD_DataExit.EXIT_DIR_STRINGS.length; ++i)
                if (OMUD.compareSBString(sbYellDir, OMUD_MMUD_DataExit.EXIT_DIR_STRINGS[i]))
                    omcd.getMMUDData().dataChat.arrlChats.get(omcd.getMMUDData().dataChat.arrlChats.size() - 1).yell_dir = OMUD_MMUD_DataExit.eExitDir.values()[i];
        }

        return pos_data_left;
    }

    private void addChatData(OMUD_Char_Data omcd, OMUD_MMUD_DataBlock_Chat.eChatType type, boolean msg_has_quotes, String target){
        omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.CHAT);

        OMUD_MMUD_DataBlock_Chat.Chat chat = new OMUD_MMUD_DataBlock_Chat.Chat();
        chat.type = type;

        // if data two exists, we have an author name in data1 and message in data2...
        if (omcd.getParseData().sbBlockDataRight.length() > 0){
            chat.name_author = omcd.getParseData().sbBlockDataLeft.toString();;
            if (msg_has_quotes)
                 chat.message = omcd.getParseData().sbBlockDataRight.substring(1, omcd.getParseData().sbBlockDataRight.length() - 2); // -2: exclude end quote and LF
            else chat.message = omcd.getParseData().sbBlockDataRight.substring(0, omcd.getParseData().sbBlockDataRight.length() - 1); // -1: exclude LF
        } else {
            chat.name_author =  omcd.getMMUDData().dataStats.name_first;
            chat.message =      omcd.getParseData().sbBlockDataLeft.substring(1, omcd.getParseData().sbBlockDataLeft.length() - 2); // this will always have quotes (room messages)
        }

        if (target.length() > 0){
            if (target.equals("you"))
                 chat.name_target = omcd.getMMUDData().dataStats.name_first;
            else chat.name_target = target;
        }

        chat.unix_time = System.currentTimeMillis() / 1000L;
        omcd.getMMUDData().dataChat.arrlChats.add(chat);

        // -----------------------------
        // MegaMUD: Check for Command
        // -----------------------------
        // don't check gossip, auction, and self chats...
        if (chat.type != OMUD_MMUD_DataBlock_Chat.eChatType.GOSSIP &&
            chat.type != OMUD_MMUD_DataBlock_Chat.eChatType.AUCTION &&
            !chat.name_author.equals(omcd.getMMUDData().dataStats.name_first))
            chat.mega_cmd = OMUD_MEGA.findCommand(chat.message);
    }
}
