import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_NCParty extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_INVITED_PRE =     "[79D[K[1m[34m";
    private final String MSTR_INVITED_END1 =    " has invited you to follow him.";
    private final String MSTR_INVITED_END2 =    " has invited you to follow her.";
    private final String MSTR_JOINED_PRE =      "[79D[K[0;37m";
    private final String MSTR_JOINED_END =      " started to follow you.";
    private final String MSTR_RANK_THEM_PRE =   "[0;37;40m[79D[K";
    private final String MSTR_RANK_THEM_FRONT = " just moved to the front rank in your group.";
    private final String MSTR_RANK_THEM_MID =   " just moved to the middle of your group.";
    private final String MSTR_RANK_THEM_BACK =  " just moved to the back rank in your group.";

    public static String getCmdText(){return "";}
    public OMUD_MMUD_ParseBlock_NCParty(){}

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        // i was invited (set up for auto-join)...
        if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_INVITED_PRE, MSTR_INVITED_END1)) > -1 ||
            (pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_INVITED_PRE, MSTR_INVITED_END2)) > -1){
            omcd.getParseData().arrlGetTypes.add(OMUD_MMUD_DataBlock.eBlockType.PARTY);
            omcd.getMMUDData().dataParty.inviter_name = omcd.getParseData().sbBlockDataLeft.toString();

        // they joined me...
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_JOINED_PRE, MSTR_JOINED_END)) > -1){
            omcd.getParseData().arrlGetTypes.add(OMUD_MMUD_DataBlock.eBlockType.PARTY);
            omcd.getMMUDData().dataParty.inviter_name = omcd.getMMUDData().dataStats.name_first;

        // they moved ranks...
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_RANK_THEM_PRE, MSTR_RANK_THEM_FRONT)) > -1){
            updateTheirRank(omcd, OMUD_MMUD_DataBlock_Party.eRank.FRONT);
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_RANK_THEM_PRE, MSTR_RANK_THEM_MID)) >   -1){
            updateTheirRank(omcd, OMUD_MMUD_DataBlock_Party.eRank.MID);
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_RANK_THEM_PRE, MSTR_RANK_THEM_BACK)) >  -1){
            updateTheirRank(omcd, OMUD_MMUD_DataBlock_Party.eRank.BACK);
        }

        return pos_data_left;
    }

    private void updateTheirRank(OMUD_Char_Data omcd, OMUD_MMUD_DataBlock_Party.eRank rank){
        omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.PARTY);

        boolean found = false;
        for (int i = 0; i < omcd.getMMUDData().dataParty.arrlMembers.size(); ++i)
            if (OMUD.compareSBString(omcd.getParseData().sbBlockDataLeft, omcd.getMMUDData().dataParty.arrlMembers.get(i).name_first)){
                omcd.getMMUDData().dataParty.arrlMembers.get(i).rank = rank;
                found = true;
                break;
            }

        // new member joined at some point and we didn't know (moved ranks after joining), force update...
        if (!found)
            omcd.getParseData().arrlGetTypes.add(OMUD_MMUD_DataBlock.eBlockType.PARTY);
    }
}
