import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_SMenu extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_MUD_MENU = "[1;30m[[0mMAJORMUD[1;30m][0m: [0;37;40m";

    public static String getCmdText(){return "";}
    public OMUD_MMUD_ParseBlock_SMenu(){}

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        return findData(omcd.getParseData(), 0, omcd.getParseData().sbBuffer.length() - 1, false, MSTR_MUD_MENU, "");
    }
}
