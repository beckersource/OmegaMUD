import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_CSChat extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_TEL_SENT_PRE =    "[0;37m--- Telepath Sent to ";
    private final String MSTR_SAY_DIR_PRE =     "[0;37m--- Message Directed to ";
    private final String MSTR_DIR_CHAT_END =    " ---";

    //private final String MSTR_TEL_BLOCKED =     "[0;37m--- Telepath Not Sent ---"; // "forgot" (ignore) command
    //private final String MSTR_TEL_NO_USER =     "Cannot find user!";
    //private final String MSTR_GOS_OFF =         "[0;37;40mYou may not use gossip while your gossip is turned off.";
    //private final String MSTR_AUC_OFF =         "[0;37;40mYou may not use auction while your auction is turned off.";
    //private final String MSTR_NO_GANG =         "[0;37;40m[79D[KYou are not in a gang at the present!";

    // NOTE: max length of a telepath message appears to be 232-242 chars, depending on how long the target name is (max first name len is 10 chars).
    // Anything over that is truncated before the send and is shown visually as being sent truncated in the terminal.

    public static String getCmdText(){return "";}
    public OMUD_MMUD_ParseBlock_CSChat(){
        _arrlCmdText.add(new CmdText("/", 1, CmdText.eDynamicType.DYNAMIC_CHAR)); // telepath
        _arrlCmdText.add(new CmdText(">", 1, CmdText.eDynamicType.DYNAMIC_CHAR)); // say directed
    }

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        // ------------------
        // Telepath
        // ------------------
               if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_TEL_SENT_PRE, MSTR_DIR_CHAT_END)) > -1){
            addChatData(omcd, OMUD_MMUD_DataBlock_Chat.eChatType.TELEPATH, strCmd);
        // ------------------
        // Directional Say
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_SAY_DIR_PRE, MSTR_DIR_CHAT_END)) > -1){
            addChatData(omcd, OMUD_MMUD_DataBlock_Chat.eChatType.SAY_DIR, strCmd);
        // ------------------
        // Invalid Channels
        // ------------------
        }

        return pos_data_left;
    }

    private void addChatData(OMUD_Char_Data omcd, OMUD_MMUD_DataBlock_Chat.eChatType type, String strCmd){
        int name_end = -1;
        if ((name_end = strCmd.indexOf(" ", 1)) > -1 &&
             name_end < strCmd.length() - 1){

            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.CHAT);

            OMUD_MMUD_DataBlock_Chat.Chat chat = new OMUD_MMUD_DataBlock_Chat.Chat();
            chat.type =         type;
            chat.name_author =  omcd.getMMUDData().dataStats.name_first;
            chat.name_target =  omcd.getParseData().sbBlockDataLeft.toString();
            chat.message =      strCmd.substring(name_end + 1, strCmd.length());
            chat.unix_time =    System.currentTimeMillis() / 1000L;
            omcd.getMMUDData().dataChat.arrlChats.add(chat);
        }
    }
}
