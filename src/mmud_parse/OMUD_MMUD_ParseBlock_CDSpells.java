import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_CDSpells extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_SPELLS =      "[0;37;40m[79D[K[1;37mYou have the following spells:\n[0;35mLevel Mana Short Spell Name\n[36m";
    private final String MSTR_POWERS =      "[0;37;40m[79D[K[1;37mYou have the following powers:\n[0;35mLevel Kai  Short Spell Name\n[36m";
    private final String MSTR_NO_SPELLS =   "[0;37;40mYou have no spells.";
    private final String MSTR_NO_POWERS =   "[0;37;40mYou have no powers.";
    private final String MSTR_ROW_REGEX =   "^[0-9 ]+[0-9 ]+[a-zA-Z][a-zA-Z][a-zA-Z][a-zA-Z] [a-zA-Z ]+$";

    public static String getCmdText(boolean is_kai){return is_kai ? "po\n" : "sp\n";}
    public OMUD_MMUD_ParseBlock_CDSpells(){
        _arrlCmdText.add(new CmdText("spells", 2, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText("powers", 2, CmdText.eDynamicType.STATIC));
    }

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        // ------------------
        // Have Spells/Powers
        // ------------------
        if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_SPELLS, "")) > -1 ||
            (pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_POWERS, "")) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.SPELLS);
            omcd.getMMUDData().dataSpells = new OMUD_MMUD_DataBlock_Spells();

            int pos_left =  -1;
            int pos_right = -1;

            // split lines, get data...
            String[] lines = omcd.getParseData().sbBlockDataLeft.toString().split("\n");
            for (String line: lines){
                line = line.trim();

                // do initial check for valid row data...
                if (line.matches(MSTR_ROW_REGEX)){
                    OMUD_MMUD_DataBlock_Spells.Spell spell = new OMUD_MMUD_DataBlock_Spells.Spell();

                    String[] tokens = line.split(" +"); // regex remove multiple spaces
                    if (tokens.length >= 4){
                        spell.level =       Integer.parseInt(tokens[0]);
                        spell.cost =        Integer.parseInt(tokens[1]);
                        spell.name_short =  tokens[2];

                        // concat the long name...
                        StringBuilder sb = new StringBuilder();
                        for (int i = 3; i < tokens.length; ++i)
                            sb.append(tokens[i] + (i + 1 < tokens.length ? " " : ""));
                        spell.name_long = sb.toString();

                        omcd.getMMUDData().dataSpells.arrlSpells.add(spell);
                    }
                } else break; // break out if something is wrong with the row data
            }
        // ------------------
        // No Spells/Powers Trained
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_NO_SPELLS, "")) > -1 ||
                   (pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_NO_POWERS, "")) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.SPELLS);
        }

        return pos_data_left;
    }
}
