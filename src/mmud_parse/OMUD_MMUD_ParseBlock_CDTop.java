import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_CDTop extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_TOP_PLAYERS1 =        "[0;37;40m[79D[K[0;33mTop Heroes of the Realm\n[37m-=-=-=-=-=-=-=-=-=-=-=-\n\n[31mRank [32mName                  [35mClass      [33mGang/Guild          [32mExperience   [35m      \n[1;30m=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n";
    private final String MSTR_TOP_PLAYERS2 =        "[0;37;40m[79D[K[0;33mTop Heroes of the Realm\n[37m-=-=-=-=-=-=-=-=-=-=-=-\n\n[31mClass[31mRank [32mName                  [35mClass      [33mGang/Guild          [32mExperience   [35m      \n[1;30m=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n";
    private final String MSTR_TOP_GANGS =           "[0;37;40m[79D[K[0;33mTop Gangs of the Realm\n[37m-=-==-=-=-=-=-=-=-=-=-\n\n[31mRank [32mGangname            [35mLeader      [33mMembers [32mCreated      [33mExp\n[1;30m=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n";
    private final String MSTR_ROW_SPLIT =           "\n\\[31m"; // double backslash because split assumes regex
    private final String MSTR_ROW1_PRE =            "[0m[31m";
    private final String MSTR_COL2 =                ". [32m";
    private final String MSTR_COL3 =                "[35m";
    private final String MSTR_COL4 =                "[33m";
    private final String MSTR_COL5 =                "[32m";
    private final String MSTR_COL6P =               "[35m";
    private final String MSTR_COL6G =               "[33m";
    private final String MSTR_TOP_GANGS_TIMES_PRE = "(";
    private final String MSTR_TOP_GANGS_TIMES_END = " times)";

    private final String MSTR_ROW_PLAYERS_PRE =     "^ *[0-9]+\\. \\[32m[a-zA-Z ]+\\[35m[a-zA-Z ]+\\[33m[^]+\\[32m[0-9 ]+\\[35m *$";
    private final String MSTR_ROW_GANGS_PRE =       "^ *[0-9]+\\. \\[32m[^]+\\[35m[a-zA-Z ]+\\[33m[0-9 ]+\\[32m[0-9][0-9]-[a-zA-Z][a-zA-Z][a-zA-Z]-[0-9][0-9] +\\[33m[0-9 ]+.*$";

    public static String getCmdText(){return "top\n";}
    public OMUD_MMUD_ParseBlock_CDTop(){
        _arrlCmdText.add(new CmdText("top", 2, CmdText.eDynamicType.DYNAMIC_SPACE));
    }

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        // ------------------
        // Top Players (All)
        // ------------------
        if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_TOP_PLAYERS1, "")) > -1){
            getPlayersTopData(omcd, OMUD_MMUD_DataBlock_Top.eTopType.PLAYERS_BY_ALL);

        // ------------------
        // Top Gangs
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_TOP_GANGS, "")) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.TOP);
            omcd.getMMUDData().dataTop = new OMUD_MMUD_DataBlock_Top(OMUD_MMUD_DataBlock_Top.eTopType.GANGS);

            // col1 header won't exist if no results...
            if (omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_ROW1_PRE) == 0){
                omcd.getParseData().sbBlockDataLeft.delete(0, MSTR_ROW1_PRE.length()); // delete first col ansi for consistency with split

                String[] rows = omcd.getParseData().sbBlockDataLeft.toString().split(MSTR_ROW_SPLIT);
                for (String row: rows){
                    row = row.replaceAll("\n", "");
                    if (row.matches(MSTR_ROW_GANGS_PRE)){
                        int pos_rank_end =          -1;
                        int pos_gang_left =         -1;
                        int pos_gang_end =          -1;
                        int pos_leader_left =       -1;
                        int pos_leader_end =        -1;
                        int pos_members_left =      -1;
                        int pos_members_end =       -1;
                        int pos_created_left =      -1;
                        int pos_created_end =       -1;
                        int pos_exp_base_left =     -1;
                        int pos_exp_base_end =      -1;
                        int pos_exp_times_left =    -1;
                        int pos_exp_times_end =     -1;
                        int pos_exp_plus_left =     -1;
                        int pos_exp_plus_end =      -1;

                        if ((pos_exp_base_left  = row.lastIndexOf(MSTR_COL6G)) > -1){
                             pos_exp_base_left += MSTR_COL6G.length();

                            // exp_base: normal amount (max 4294967295)
                            if ((pos_exp_base_end = row.indexOf(MSTR_TOP_GANGS_TIMES_PRE, pos_exp_base_left)) == -1)
                                 pos_exp_base_end = row.length();

                            // exp_times: multiple "times" of exp_base
                            if (pos_exp_base_end < row.length()){

                                if ((pos_exp_times_end  = row.indexOf(MSTR_TOP_GANGS_TIMES_END, pos_exp_base_end)) > -1)
                                     pos_exp_times_left = pos_exp_base_end + MSTR_TOP_GANGS_TIMES_PRE.length();

                                // exp_plus: additional +amount (exp_base * exp_times + exp_plus)
                                if ((pos_exp_plus_left = row.indexOf("+", pos_exp_base_end)) > -1){
                                    pos_exp_plus_left++;
                                    pos_exp_plus_end = row.length();
                                }
                            }

                            // remaining columns...
                            if (pos_exp_base_left > -1 && pos_exp_base_left < pos_exp_base_end &&
                                (pos_created_left  = row.lastIndexOf(MSTR_COL5, pos_exp_base_left)) > -1 &&
                                (pos_created_end   = row.indexOf(MSTR_COL6G, pos_created_left + MSTR_COL5.length())) > -1){
                                 pos_created_left += MSTR_COL5.length();

                                if ((pos_members_left  = row.lastIndexOf(MSTR_COL4, pos_created_left)) > -1 &&
                                    (pos_members_end   = row.indexOf(MSTR_COL5, pos_members_left + MSTR_COL4.length())) > -1){
                                     pos_members_left += MSTR_COL4.length();

                                    if ((pos_leader_left  = row.lastIndexOf(MSTR_COL3, pos_members_left)) > -1 &&
                                        (pos_leader_end   = row.indexOf(MSTR_COL4, pos_leader_left + MSTR_COL3.length())) > -1){
                                         pos_leader_left += MSTR_COL3.length();

                                        if ((pos_gang_left  = row.lastIndexOf(MSTR_COL2, pos_leader_left)) > -1 &&
                                            (pos_gang_end   = row.indexOf(MSTR_COL3, pos_gang_left + MSTR_COL2.length())) > -1){
                                             pos_rank_end   = pos_gang_left;
                                             pos_gang_left += MSTR_COL2.length();

                                            if (pos_rank_end > 0){
                                                OMUD_MMUD_DataBlock_Top.TopRow tr = new OMUD_MMUD_DataBlock_Top.TopRow();
                                                tr.rank =       Integer.valueOf(row.substring(0, pos_rank_end).trim());
                                                tr.strGang =    row.substring(pos_gang_left, pos_gang_end).trim();
                                                tr.strLeader =  row.substring(pos_leader_left, pos_leader_end).trim();
                                                tr.members =    Integer.valueOf(row.substring(pos_members_left, pos_members_end).trim());
                                                tr.strCreated = row.substring(pos_created_left, pos_created_end).trim();
                                                tr.exp_base =   Long.parseLong(row.substring(pos_exp_base_left, pos_exp_base_end).trim());
                                                if (pos_exp_times_left > -1 && pos_exp_times_left < pos_exp_times_end)
                                                    tr.exp_times = Long.parseLong(row.substring(pos_exp_times_left, pos_exp_times_end));
                                                if (pos_exp_plus_left > -1 && pos_exp_plus_left < pos_exp_plus_end)
                                                    tr.exp_plus =  Long.parseLong(row.substring(pos_exp_plus_left, pos_exp_plus_end).trim());
                                                omcd.getMMUDData().dataTop.arrlRows.add(tr);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else break; // break out if something is wrong with the row data
                }
            }
        // ------------------
        // Top Players (by Class)
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_TOP_PLAYERS2, "")) > -1){
            getPlayersTopData(omcd, OMUD_MMUD_DataBlock_Top.eTopType.PLAYERS_BY_CLASS);
        }

        return pos_data_left;
    }

    private void getPlayersTopData(OMUD_Char_Data omcd, OMUD_MMUD_DataBlock_Top.eTopType type){
        omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.TOP);
        omcd.getMMUDData().dataTop = new OMUD_MMUD_DataBlock_Top(type);

        // col1 header won't exist if no results...
        if (omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_ROW1_PRE) == 0){
            omcd.getParseData().sbBlockDataLeft.delete(0, MSTR_ROW1_PRE.length()); // delete first col ansi for consistency with split

            String[] rows = omcd.getParseData().sbBlockDataLeft.toString().split(MSTR_ROW_SPLIT);
            for (int i = 0; i < rows.length; ++i){
                String row = rows[i];

                // remove trailing LF on final row...
                if (i == rows.length - 1 && row.charAt(row.length() - 1) == OMUD.ASCII_LF)
                    row = row.substring(0, row.length() - 1);

                if (row.matches(MSTR_ROW_PLAYERS_PRE)){
                    int pos_rank_end =   -1;
                    int pos_name_left =  -1;
                    int pos_name_end =   -1;
                    int pos_class_left = -1;
                    int pos_class_end =  -1;
                    int pos_gang_left =  -1;
                    int pos_gang_end =   -1;
                    int pos_exp_left =   -1;
                    int pos_exp_end =    -1;

                    if ((pos_exp_left = row.lastIndexOf(MSTR_COL5)) > -1 &&
                        (pos_exp_end  = row.indexOf(MSTR_COL6P, pos_exp_left + MSTR_COL5.length())) > -1){
                        pos_exp_left += MSTR_COL5.length();

                        if ((pos_gang_left = row.lastIndexOf(MSTR_COL4, pos_exp_end)) > -1 &&
                            (pos_gang_end  = row.indexOf(MSTR_COL5, pos_gang_left + MSTR_COL4.length())) > -1){
                            pos_gang_left += MSTR_COL4.length();

                            if ((pos_class_left = row.lastIndexOf(MSTR_COL3, pos_gang_end)) > -1 &&
                                (pos_class_end  = row.indexOf(MSTR_COL4, pos_class_left + MSTR_COL3.length())) > -1){
                                pos_class_left += MSTR_COL3.length();

                                if ((pos_name_left  = row.lastIndexOf(MSTR_COL2, pos_class_end)) > -1 &&
                                    (pos_name_end   = row.indexOf(MSTR_COL3, pos_name_left + MSTR_COL2.length())) > -1){
                                     pos_rank_end   = pos_name_left;
                                     pos_name_left += MSTR_COL2.length();

                                    if (pos_rank_end > 0){
                                        OMUD_MMUD_DataBlock_Top.TopRow tr = new OMUD_MMUD_DataBlock_Top.TopRow();
                                        tr.rank =       Integer.valueOf(row.substring(0, pos_rank_end).trim());
                                        tr.strName =    row.substring(pos_name_left, pos_name_end).trim();
                                        tr.strClass =   row.substring(pos_class_left, pos_class_end).trim();
                                        tr.strGang =    row.substring(pos_gang_left, pos_gang_end).trim();
                                        tr.exp_base =   Long.parseLong(row.substring(pos_exp_left, pos_exp_end).trim());
                                        omcd.getMMUDData().dataTop.arrlRows.add(tr);
                                    }
                                }
                            }
                        }
                    }
                } else break; // break out if something is wrong with the row data
            }
        }
    }
}
