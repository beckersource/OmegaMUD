import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_CSInv extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_INV_PRE =     "[0;37;40m[0;37;40m[79D[KYou are carrying ";
    private final String MSTR_INV_END =     "%][0m";
    private final String MSTR_NO_ITEMS =    "Nothing!";
    private final String MSTR_KEYS_PRE =    "You have ";
    private final String MSTR_KEYS_END =    ".";
    private final String MSTR_KEYS_YES =    "the following keys:  ";
    private final String MSTR_WEALTH_PRE =  "Wealth: ";
    private final String MSTR_WEALTH_END =  " copper farthings";
    private final String MSTR_ENC_PRE =     "Encumbrance: ";
    private final String MSTR_ENC_MID =     " - ";
    private final String MSTR_ENC_END =     "[";

    public static String getCmdText(){return "i\n";}
    public OMUD_MMUD_ParseBlock_CSInv(){
        _arrlCmdText.add(new CmdText("i",           1, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText("inventory",   4, CmdText.eDynamicType.STATIC));        // "inve" is min ("in" and "inv" conflict with "invite" so are ignored in mud)
        _arrlCmdText.add(new CmdText("equip",       2, CmdText.eDynamicType.DYNAMIC_SPACE)); // can be "equip" to show the invetory or be "equip item"
        _arrlCmdText.add(new CmdText("wear",        3, CmdText.eDynamicType.DYNAMIC_SPACE));
    }

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_INV_PRE, MSTR_INV_END)) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.INV);
            omcd.getMMUDData().dataInv = new OMUD_MMUD_DataBlock_Inv();
            cleanData(omcd.getParseData().sbBlockDataLeft, true, true);

            int pos_left =  0;
            int pos_right = omcd.getParseData().sbBlockDataLeft.length() - 1;

            // ------------------
            // Encumbrance
            // ------------------
            if ((pos_right = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_ENC_END, pos_right)) > -1 &&
                (pos_left  = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_ENC_MID, pos_right)) > -1){
                omcd.getMMUDData().dataInv.enc_level = omcd.getParseData().sbBlockDataLeft.substring(pos_left + MSTR_ENC_MID.length(), pos_right).trim().toLowerCase();
                pos_right = pos_left - 1;

                if ((pos_left = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_ENC_PRE, pos_right)) > -1){
                    String[] tokens = omcd.getParseData().sbBlockDataLeft.substring(pos_left + MSTR_ENC_PRE.length(), pos_right + 1).trim().split("/");
                    if (tokens.length == 2){
                        omcd.getMMUDData().dataInv.enc_cur = Integer.parseInt(tokens[0]);
                        omcd.getMMUDData().dataInv.enc_max = Integer.parseInt(tokens[1]);
                    }
                    pos_right = pos_left - 1;
                }
            }

            // ------------------
            // Wealth
            // ------------------
            if ((pos_right = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_WEALTH_END, pos_right)) > -1 &&
                (pos_left  = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_WEALTH_PRE, pos_right)) > -1){
                omcd.getMMUDData().dataInv.wealth = Integer.parseInt(omcd.getParseData().sbBlockDataLeft.substring(pos_left + MSTR_WEALTH_PRE.length(), pos_right).trim());
                pos_right = pos_left - 1;
            }

            // ------------------
            // Keys
            // ------------------
            if ((pos_right = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_KEYS_END, pos_right)) > -1 &&
                (pos_left  = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_KEYS_PRE, pos_right)) > -1){
                if (omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_KEYS_YES, pos_left + MSTR_KEYS_PRE.length()) > -1){
                    String strKeys = omcd.getParseData().sbBlockDataLeft.substring(pos_left + MSTR_KEYS_PRE.length() + MSTR_KEYS_YES.length(), pos_right).trim().toLowerCase();
                    ArrayList<String> arrlKeys = new ArrayList<String>();
                    splitCommaListToArray(strKeys, arrlKeys);
                    for (int i = 0; i < arrlKeys.size(); ++i){
                        OMUD_MMUD_DataItem item = new OMUD_MMUD_DataItem(arrlKeys.get(i));
                        item.item_loc = OMUD_MMUD_DataItem.eItemLoc.KEY;
                        omcd.getMMUDData().dataInv.arrlKeys.add(item);
                    }
                    OMUD_MMUD_DataItem.sort(omcd.getMMUDData().dataInv.arrlKeys, OMUD_MMUD_DataItem.eSortType.NAME, true);
                } //else omcd.getMMUDData().dataInv.keys_str = "(no keys carried)";
                pos_right = pos_left - 1;
            }

            // ------------------
            // Items (+ Coins)
            // ------------------
            String strItems = omcd.getParseData().sbBlockDataLeft.substring(0, pos_right + 1).trim().toLowerCase();
            // get item list and coins...
            if (!strItems.equals(MSTR_NO_ITEMS)){
                ArrayList<String> arrlNew = new ArrayList<String>();
                splitCommaListToArray(strItems, arrlNew);
                buildItems(arrlNew, omcd.getMMUDData().dataInv.arrlItems, omcd.getMMUDData().dataInv.coins, omcd.getMMUDData().dataInv.arrlEQ);
                OMUD_MMUD_DataItem.sort(omcd.getMMUDData().dataInv.arrlItems, OMUD_MMUD_DataItem.eSortType.NAME, true);
                OMUD_MMUD_DataItem.sort(omcd.getMMUDData().dataInv.arrlEQ,  OMUD_MMUD_DataItem.eSortType.EQUIP_SLOT, true);
            } // else omcd.getMMUDData().dataInv.items_str = "(no items carried)";
        }

        return pos_data_left;
    }

    private int getItemCount(String strItem){
        int count = -1;
        String[] tokens = strItem.split(" ");
        if (tokens.length >= 2)
            count = Integer.parseInt(tokens[0]);
        return count;
    }
}
