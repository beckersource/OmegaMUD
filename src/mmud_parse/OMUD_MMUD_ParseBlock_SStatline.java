import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_SStatline extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_STATLINE_PRE =    "[79D[K[0;37m[";
    private final String MSTR_STATLINE_END =    "[0;37m]:";
    private final String MSTR_STATLINE_REST =   " (Resting) ";
    private final String MSTR_STATLINE_MED =    " (Meditating) ";

    public static String getCmdText(){return "";}
    public OMUD_MMUD_ParseBlock_SStatline(){}

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){

        // NOTE: use indexOf() first because we don't want to eat all the buffer data using the full buffer length...
        if ((pos_data_left = omcd.getParseData().sbBuffer.indexOf(MSTR_STATLINE_END, 0)) > -1 &&
            (pos_data_left = findData(omcd.getParseData(), 0, pos_data_left + MSTR_STATLINE_END.length() - 1, true, MSTR_STATLINE_PRE, MSTR_STATLINE_END)) > -1){

            // default to active first...
            OMUD_MMUD_DataBlock_Statline.eActionState sline_state = OMUD_MMUD_DataBlock_Statline.eActionState.READY;

            // ------------------
            // Statline: Resting/Meditation: MA/KAI Chars
            // ------------------
            // MA/KAI chars: resting/meditation strings are a suffix after the statline prompt outer brackets.
            if (pos_data_left < omcd.getParseData().sbBuffer.length()){
                int pos_rest_start = 0;
                if ((pos_rest_start = omcd.getParseData().sbBuffer.indexOf(MSTR_STATLINE_MED, pos_data_left)) > -1){
                    omcd.getParseData().sbBuffer.delete(pos_rest_start, pos_rest_start + MSTR_STATLINE_MED.length());
                    sline_state = OMUD_MMUD_DataBlock_Statline.eActionState.MED;
                } else if ((pos_rest_start = omcd.getParseData().sbBuffer.indexOf(MSTR_STATLINE_REST, pos_data_left)) > -1){
                    omcd.getParseData().sbBuffer.delete(pos_rest_start, pos_rest_start + MSTR_STATLINE_REST.length());
                    sline_state = OMUD_MMUD_DataBlock_Statline.eActionState.REST;
                }
            }

            // ------------------
            // Statline: Resting/Meditation: Non-MA/KAI Chars
            // ------------------
            // Non-MA/KAI chars: if not found above, try inside of the statline...
            if (sline_state == OMUD_MMUD_DataBlock_Statline.eActionState.READY){
                int pos_rest_start = 0;
                if ((pos_rest_start = omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_STATLINE_REST, 0)) > -1){
                    omcd.getParseData().sbBlockDataLeft.delete(pos_rest_start, pos_rest_start + MSTR_STATLINE_REST.length());
                    sline_state = OMUD_MMUD_DataBlock_Statline.eActionState.REST;
                // not sure if non-MA/KAI chars would ever use meditate?
                } else if ((pos_rest_start = omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_STATLINE_MED, 0)) > -1){
                    omcd.getParseData().sbBlockDataLeft.delete(pos_rest_start, pos_rest_start + MSTR_STATLINE_MED.length());
                    sline_state = OMUD_MMUD_DataBlock_Statline.eActionState.MED;
                }
            }

            // don't change the state if in combat and just a ready state found...
            if (!(sline_state == OMUD_MMUD_DataBlock_Statline.eActionState.READY &&
                  omcd.getMMUDData().dataStatline.action_state == OMUD_MMUD_DataBlock_Statline.eActionState.COMBAT))
                omcd.getMMUDData().dataStatline.action_state  = sline_state;

            // strip ansi...
            cleanData(omcd.getParseData().sbBlockDataLeft, false, true);

            // get mana (if exists)...
            int pos_equals  = 0;
            int pos_hp_end  = 0;
            if ((pos_hp_end = omcd.getParseData().sbBlockDataLeft.indexOf("/", pos_equals)) > -1 &&
                (pos_equals = omcd.getParseData().sbBlockDataLeft.indexOf("=", pos_hp_end)) > -1){
                if (omcd.getMMUDData().dataStatline.ma_str.length() == 0) // only get the mana string once for efficiency
                    omcd.getMMUDData().dataStatline.ma_str = omcd.getParseData().sbBlockDataLeft.substring(pos_hp_end + 1, pos_equals);
                omcd.getMMUDData().dataStatline.ma_cur = Integer.parseInt(omcd.getParseData().sbBlockDataLeft.substring(++pos_equals, omcd.getParseData().sbBlockDataLeft.length()));
            } else pos_hp_end = omcd.getParseData().sbBlockDataLeft.length();
            // get hp...
            if ((pos_equals = omcd.getParseData().sbBlockDataLeft.indexOf("=", 0)) > -1){
                if (omcd.getMMUDData().dataStatline.hp_str.length() == 0)
                    omcd.getMMUDData().dataStatline.hp_str = omcd.getParseData().sbBlockDataLeft.substring(0, pos_equals);
                omcd.getMMUDData().dataStatline.hp_cur = Integer.parseInt(omcd.getParseData().sbBlockDataLeft.substring(pos_equals + 1, pos_hp_end));
            }
        }

        return pos_data_left;
    }
}
