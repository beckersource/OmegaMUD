import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_NCCombat extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_OPT_RESET_WHBL =      "[0;37;40m";
    private final String MSTR_EXP_GAIN_PRE =        "[79D[KYou gain ";
    private final String MSTR_EXP_GAIN_END =        " experience.";
    private final String MSTR_DEATH_TEXT_PRE =      "[79D[K";
    private final String MSTR_DEATH_TEXT_NO_MONEY = "[79D[K[79D[K";

    private final String MSTR_COMBAT_OFF =      "[79D[K[0;33m*Combat Off*";

    private final String MSTR_AT_TGT =          " at ";
    private final String MSTR_YOU_PRE =         "You ";
    private final String MSTR_YOUR_PRE =        "Your ";
    private final String MSTR_YOU_TGT_TRIM =    "you";
    private final String MSTR_YOU_TGT_SPC =     " you ";
    private final String MSTR_YOU_CAPS =        "YOU";
    private final String MSTR_THE_PRE =         "The ";

    private final String MSTR_HIT_PRE1 =        "[79D[K[1;31m";
    private final String MSTR_HIT_PRE2 =        "[79D[K[1m[31m";
    private final String MSTR_HIT_FOR =         " for ";
    private final String MSTR_HIT_END =         " damage!"; // some MudRev strings are spelled "danage"

    private final String MSTR_MISS_PRE =        "[79D[K[0;36m";
    private final String MSTR_MISS_END =        "!";
    private final String MSTR_DODGE_PRE =       ", but ";
    private final String MSTR_DODGE_END =       " out of the way"; // don't include exclamation point - already included above in MISS_END string

    private final String MSTR_GLANCE_PRE =      "[79D[K[0;31m";
    private final String MSTR_GLANCE_MID =      " hits, but glances off ";
    private final String MSTR_DEFLECT_PRE =     "[79D[K[0;31m";
    private final String MSTR_DEFLECT_END =     " armour deflects the blow!";

    public static String getCmdText(){return "";}
    public OMUD_MMUD_ParseBlock_NCCombat(){}

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        // ------------------
        // Combat: OFF
        // ------------------
        if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_COMBAT_OFF, "")) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.COMBAT);
            updateCombatState(omcd.getMMUDData(), false, true);
            omcd.getMMUDData().dataCombat.arrlLines.add(new OMUD_MMUD_DataBlock_Combat.CombatLine(OMUD_MMUD_DataBlock_Combat.eLineType.COMBAT_OFF));

            pos_data_left = removeOptPrefix(omcd, "Combat Broken by Cmd", pos_data_left, MSTR_OPT_RESET_WHBL);

        // ------------------
        // EXP Gained
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_EXP_GAIN_PRE, MSTR_EXP_GAIN_END)) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.COMBAT);
            omcd.getParseData().arrlGetTypes.add(OMUD_MMUD_DataBlock.eBlockType.ROOM); // refresh room to rebuild the unit list in the room

            long exp = Long.parseLong(omcd.getParseData().sbBlockDataLeft.toString());
            omcd.getMMUDData().dataExp.cur_total   += exp;
            omcd.getMMUDData().dataExp.next_rem    -= exp;
            if (omcd.getMMUDData().dataExp.next_rem < 0)
                omcd.getMMUDData().dataExp.next_rem = 0;

            // accumulate exp gained during the current combat session (will be cleared)...
            omcd.getMMUDData().dataExp.arrlCombatExp.add(new OMUD_MMUD_DataBlock_Exp.DeathExp(exp));

            int pos_left          = -1;
            int pos_death_left    = -1;
            int pos_death_right   = -1;
            boolean money_dropped = true;

            // check for a no-money death text, else get the money drop version...
            if ((pos_death_right = OMUD.getPrevLF(omcd.getParseData().sbBuffer, pos_data_left)) > -1 &&
                (pos_death_left  = omcd.getParseData().sbBuffer.lastIndexOf(MSTR_DEATH_TEXT_NO_MONEY, pos_death_right)) > -1 &&
                (pos_left = findData(omcd.getParseData(), pos_death_left, pos_death_right, true, MSTR_DEATH_TEXT_NO_MONEY, "")) > -1)
                money_dropped = false;
            else if ((pos_death_left  = omcd.getParseData().sbBuffer.lastIndexOf(MSTR_DEATH_TEXT_PRE, pos_death_right)) > -1)
                pos_left = findData(omcd.getParseData(), pos_death_left, pos_death_right, true, MSTR_DEATH_TEXT_PRE, "");

            // i think there should always be a death text at least but check anyway...
            if (pos_left > -1){
                cleanData(omcd.getParseData().sbBlockDataLeft, true, false);

                pos_data_left = pos_left;
                omcd.getMMUDData().dataExp.arrlCombatExp.get(omcd.getMMUDData().dataExp.arrlCombatExp.size() - 1).desc = omcd.getParseData().sbBlockDataLeft.toString();

                // money drop death text has another prefix...
                if (money_dropped &&
                    (pos_death_right = OMUD.getPrevLF(omcd.getParseData().sbBuffer, pos_death_left)) > -1 &&
                    (pos_death_left = omcd.getParseData().sbBuffer.lastIndexOf(MSTR_DEATH_TEXT_PRE, pos_death_right)) > -1 &&
                    (pos_left = findData(omcd.getParseData(), pos_death_left, pos_death_right, true, MSTR_DEATH_TEXT_PRE, "")) > -1){
                    cleanData(omcd.getParseData().sbBlockDataLeft, true, false);
                    pos_data_left = pos_left;
                    omcd.getMMUDData().dataExp.arrlCombatExp.get(omcd.getMMUDData().dataExp.arrlCombatExp.size() - 1).money = omcd.getParseData().sbBlockDataLeft.toString();
                }
            }

        // ------------------
        // Combat: Hit (You/I & They)
        // ------------------
        } else if (
            (pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_HIT_PRE1, MSTR_HIT_END)) > -1 ||
            (pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_HIT_PRE2, MSTR_HIT_END)) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.COMBAT);
            updateCombatState(omcd.getMMUDData(), false, false);

            OMUD_MMUD_DataBlock_Combat.CombatLine cl = new OMUD_MMUD_DataBlock_Combat.CombatLine(OMUD_MMUD_DataBlock_Combat.eLineType.HIT);

            int pos_left =  0;
            int pos_right = 0;
            cl.unit = new OMUD_MMUD_DataUnit();

            // get damage...
            if ((pos_right = omcd.getParseData().sbBlockDataLeft.lastIndexOf(" ", omcd.getParseData().sbBlockDataLeft.length() - 1)) > -1)
                cl.tgt_dmg = Integer.parseInt(omcd.getParseData().sbBlockDataLeft.substring(pos_right + 1, omcd.getParseData().sbBlockDataLeft.length()));

            // find for, and check for spell casts and other attacks that have " at " target...
            if ((pos_right  = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_HIT_FOR, pos_right)) > -1 &&
                (pos_left   = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_AT_TGT,  pos_right)) > -1){
                cl.tgt_name = omcd.getParseData().sbBlockDataLeft.substring(pos_left + MSTR_AT_TGT.length(), pos_right);
                pos_right   = pos_left;
            }

            // check for you/I as prefix...
            if (omcd.getParseData().sbBlockDataLeft.substring(0, MSTR_YOU_PRE.length()).equals(MSTR_YOU_PRE)){
                pos_left = MSTR_YOU_PRE.length();
                cl.unit.name = MSTR_YOU_CAPS;
            } else pos_left = 0;

            // get a cropped version of the string after above...
            String strCrop = omcd.getParseData().sbBlockDataLeft.substring(pos_left, pos_right + 1);
            int pos_unit_left = 0;
            int pos_tgt_left  = 0;
            pos_right = strCrop.length() - 1;

            // find target name...
            if (cl.tgt_name.length() == 0){
                if ((pos_tgt_left = strCrop.lastIndexOf(MSTR_YOU_TGT_SPC, pos_right)) > -1){
                    cl.tgt_name = MSTR_YOU_CAPS;
                } else if ((pos_tgt_left = findUnitName(cl, omcd.getMMUDData().dataRoom.arrlUnits, strCrop, true)) == -1){
                    cl.tgt_name = "???";
                    pos_tgt_left = pos_right;
                }
            } else pos_tgt_left = pos_right;

            // find unit name...
            if (cl.unit.name.length() == 0){
                if ((pos_unit_left  = findUnitName(cl, omcd.getMMUDData().dataRoom.arrlUnits, strCrop, false)) > -1){
                     pos_unit_left += cl.unit.name.length() + 1; // +1 skip space to next word after name
                // unit is unknown, just get text until the target as the unit name...
                } else {
                    pos_left = 0;
                    // check for "The " prefix (skip it)...
                    if (strCrop.substring(0, MSTR_THE_PRE.length()).equals(MSTR_THE_PRE))
                        pos_left = MSTR_THE_PRE.length();
                    cl.unit.name  = strCrop.substring(pos_left, pos_tgt_left + 1).trim();
                    pos_unit_left = pos_tgt_left;
                }
            }

            // get unit action text...
            if (pos_unit_left < pos_tgt_left){
                cl.unit_action = strCrop.substring(pos_unit_left, pos_tgt_left).trim();
                // check for more on the right...
                pos_tgt_left += cl.tgt_name.length() + 1; // +1 skip space to next word after name
                if (pos_tgt_left < pos_right)
                    cl.unit_action += ", " + strCrop.substring(pos_tgt_left, pos_right + 1).trim();
            }

            omcd.getMMUDData().dataCombat.arrlLines.add(cl);

        // ------------------
        // Combat: Miss
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_MISS_PRE, MSTR_MISS_END)) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.COMBAT);
            updateCombatState(omcd.getMMUDData(), false, false);

            OMUD_MMUD_DataBlock_Combat.CombatLine cl = new OMUD_MMUD_DataBlock_Combat.CombatLine(OMUD_MMUD_DataBlock_Combat.eLineType.MISS);

            int pos_left =  0;
            int pos_right = 0;

            // ------------------
            // You/I
            // ------------------
            if (omcd.getParseData().sbBlockDataLeft.substring(0, MSTR_YOU_PRE.length()).equals(MSTR_YOU_PRE)){
                cl.unit = new OMUD_MMUD_DataUnit(MSTR_YOU_CAPS);

                pos_left = MSTR_YOU_PRE.length();
                if ((pos_right =     omcd.getParseData().sbBlockDataLeft.indexOf(" ", pos_left)) > -1)
                    cl.unit_action = omcd.getParseData().sbBlockDataLeft.substring(pos_left, pos_right);

                // skip " at " and get the attacked name...
                pos_left = pos_right + 1;
                if ((pos_right =  omcd.getParseData().sbBlockDataLeft.indexOf(" ", pos_left)) > -1)
                    cl.tgt_name = omcd.getParseData().sbBlockDataLeft.substring(pos_right + 1, omcd.getParseData().sbBlockDataLeft.length());

            // ------------------
            // They
            // ------------------
            } else if ((pos_right = omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_AT_TGT, pos_left))  > -1 &&
                       (pos_left =  omcd.getParseData().sbBlockDataLeft.lastIndexOf(" ", pos_right - 1)) > -1){

                int name_prefix_len = 0;
                if (omcd.getParseData().sbBlockDataLeft.substring(0, MSTR_THE_PRE.length()).equals(MSTR_THE_PRE))
                    name_prefix_len = MSTR_THE_PRE.length();

                cl.unit =        new OMUD_MMUD_DataUnit(omcd.getParseData().sbBlockDataLeft.substring(name_prefix_len, pos_left));
                cl.unit_action = omcd.getParseData().sbBlockDataLeft.substring(pos_left + 1, pos_right);

                pos_left =  pos_right + MSTR_AT_TGT.length();
                if ((pos_right = omcd.getParseData().sbBlockDataLeft.indexOf(" ", pos_left)) > -1){
                    // check for dodge (from the end)...
                    if (omcd.getParseData().sbBlockDataLeft.length() > MSTR_DODGE_END.length() &&
                        omcd.getParseData().sbBlockDataLeft.substring(omcd.getParseData().sbBlockDataLeft.length() - MSTR_DODGE_END.length(), omcd.getParseData().sbBlockDataLeft.length()).equals(MSTR_DODGE_END)){
                        cl.type = OMUD_MMUD_DataBlock_Combat.eLineType.DODGE;
                        pos_right = omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_DODGE_PRE, pos_left);
                    } else pos_right = omcd.getParseData().sbBlockDataLeft.length();

                    // weapon...
                    int pos_left_weap = omcd.getParseData().sbBlockDataLeft.lastIndexOf(" ", pos_right - 1);
                    if (pos_left_weap > pos_left){
                        cl.tgt_weapon = omcd.getParseData().sbBlockDataLeft.substring(pos_left_weap + 1, pos_right);
                        pos_right =     omcd.getParseData().sbBlockDataLeft.indexOf(" ", pos_left); // set right to original pos after name for below
                    }
                } else pos_right = omcd.getParseData().sbBlockDataLeft.length();

                // get name...
                cl.tgt_name = omcd.getParseData().sbBlockDataLeft.substring(pos_left, pos_right);
                if (cl.tgt_name.equals(MSTR_YOU_TGT_TRIM))
                    cl.tgt_name = cl.tgt_name.toUpperCase();
            }

            if (cl.unit.name.length() > 0)
                omcd.getMMUDData().dataCombat.arrlLines.add(cl);

        // ------------------
        // Combat: Glancing
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_GLANCE_PRE, MSTR_GLANCE_MID)) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.COMBAT);
            updateCombatState(omcd.getMMUDData(), false, false);

            OMUD_MMUD_DataBlock_Combat.CombatLine cl = new OMUD_MMUD_DataBlock_Combat.CombatLine(OMUD_MMUD_DataBlock_Combat.eLineType.GLANCE);

            int pos_left =  0;
            int pos_right = 0;

            // ------------------
            // You/I
            // ------------------
            if (omcd.getParseData().sbBlockDataLeft.substring(0, MSTR_YOUR_PRE.length()).equals(MSTR_YOUR_PRE)){
                cl.unit = new OMUD_MMUD_DataUnit(MSTR_YOU_CAPS);

                pos_left = MSTR_YOUR_PRE.length();
                if ((pos_right =     omcd.getParseData().sbBlockDataLeft.indexOf(" ", pos_left)) > -1)
                    cl.unit_action = omcd.getParseData().sbBlockDataLeft.substring(pos_left, pos_right);

                // skip " at " and get the attacked name...
                pos_left = pos_right + 1;
                if ((pos_right =  omcd.getParseData().sbBlockDataLeft.indexOf(" ", pos_left)) > -1)
                    cl.tgt_name = omcd.getParseData().sbBlockDataLeft.substring(pos_right + 1, omcd.getParseData().sbBlockDataLeft.length());
            }

            if (cl.unit.name.length() > 0)
                omcd.getMMUDData().dataCombat.arrlLines.add(cl);

        // ------------------
        // Combat: Deflection
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_DEFLECT_PRE, MSTR_DEFLECT_END)) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.COMBAT);
            updateCombatState(omcd.getMMUDData(), false, false);

            OMUD_MMUD_DataBlock_Combat.CombatLine cl = new OMUD_MMUD_DataBlock_Combat.CombatLine(OMUD_MMUD_DataBlock_Combat.eLineType.DEFLECT);

            int pos_left =  0;
            int pos_right = 0;

            if ((pos_left  = omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_AT_TGT, pos_left)) > -1 &&
                (pos_right = omcd.getParseData().sbBlockDataLeft.indexOf(",", pos_left)) > -1){
                cl.tgt_name = omcd.getParseData().sbBlockDataLeft.substring(pos_left + MSTR_AT_TGT.length(), pos_right);

                pos_right = pos_left - 1;
                if ((pos_left = omcd.getParseData().sbBlockDataLeft.lastIndexOf(" ", pos_right)) > -1){
                    cl.unit_action = omcd.getParseData().sbBlockDataLeft.substring(pos_left + 1, pos_right + 1);

                    pos_right = pos_left;
                    if (omcd.getParseData().sbBlockDataLeft.substring(0, MSTR_THE_PRE.length()).equals(MSTR_THE_PRE))
                         pos_left += MSTR_THE_PRE.length();
                    else pos_left = 0;
                    cl.unit = new OMUD_MMUD_DataUnit(omcd.getParseData().sbBlockDataLeft.substring(pos_left, pos_right));

                    omcd.getMMUDData().dataCombat.arrlLines.add(cl);
                }
            }
        }

        return pos_data_left;
    }

    private int findUnitName(OMUD_MMUD_DataBlock_Combat.CombatLine cl, ArrayList<OMUD_MMUD_DataUnit> arrlUnits, String strText, boolean for_target){
        int unit_num =      -1;
        int pos_left =      -1;
        int pos_left_ret =  -1;

        // loop through the room units and find a match in the text...
        for (int i = 0; i < arrlUnits.size(); ++i)
            if ((pos_left = strText.indexOf(arrlUnits.get(i).name, 0)) > -1){
                // if this is our first match or
                // check for best match on mob name descriptive prefixes (always use the prefix with the most words)...
                if (unit_num == -1 ||
                    (arrlUnits.get(i).name.length() > arrlUnits.get(unit_num).name.length())){
                    unit_num = i;
                    pos_left_ret = pos_left;
                }
            }

        if (unit_num > -1){
            if (for_target)
                 cl.tgt_name  = arrlUnits.get(unit_num).name;
            else cl.unit.name = arrlUnits.get(unit_num).name;
        }

        return pos_left_ret;
    }
}
