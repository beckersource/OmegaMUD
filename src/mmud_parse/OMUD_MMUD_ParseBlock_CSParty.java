import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_CSParty extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_PARTY_MULTI =     "[0;37mThe following people are in your travel party:";
    private final String MSTR_PARTY_SOLO =      "[0;37;40m[79D[K[1;31mYou are not in a party at the present time.";
    private final String MSTR_MEMBER_NAME_PRE = "  ";
    private final String MSTR_MEMBER_MANA =     "[M:";
    private final String MSTR_MEMBER_HP =       "[H:";
    private final String MSTR_MEMBER_RANK =     " - ";
    private final String MSTR_UNINVITE_PRE =    "[79D[K[0;31m";
    private final String MSTR_UNINVITE_END =    " has been removed from your followers.";
    private final String MSTR_JOINED_THEM =     "[79D[K[0;37mYou are now following ";
    private final String MSTR_LEFT_THEM =       "[0;37;40m[79D[K[1;34mYou are no longer following ";
    private final String MSTR_RANK_ME_FRONT =   "You have moved to the front ranks of your group.";
    private final String MSTR_RANK_ME_MID =     "You have moved to the middle ranks of your group.";
    private final String MSTR_RANK_ME_BACK =    "You have moved to the back ranks of your group.";

    public static String getCmdText() {return "par\n";}
    public OMUD_MMUD_ParseBlock_CSParty(){
        _arrlCmdText.add(new CmdText("party",     3, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText("join",      2, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText("follow",    2, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText("leave",     2, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText("uninvite",  3, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText("frontrank", 2, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText("midrank",   2, CmdText.eDynamicType.STATIC));
        _arrlCmdText.add(new CmdText("backrank",  5, CmdText.eDynamicType.STATIC));
    }

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        // ------------------
        // Party with Members
        // ------------------
        if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_PARTY_MULTI, "")) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.PARTY);

            omcd.getMMUDData().dataParty = new OMUD_MMUD_DataBlock_Party(omcd.getMMUDData().dataParty.inviter_name, omcd.getMMUDData().dataParty.leader_name);
            cleanData(omcd.getParseData().sbBlockDataLeft, false, true);

            String[] tokens = omcd.getParseData().sbBlockDataLeft.toString().split("\n");
            for (String token : tokens){
                int pos_left  = 0;
                int pos_right = token.length() - 1;
                OMUD_MMUD_DataBlock_Party.Member m = new OMUD_MMUD_DataBlock_Party.Member();

                // if rank is present, player has joined...
                if ((pos_left = token.lastIndexOf(MSTR_MEMBER_RANK, pos_right)) > -1){
                    m.accepted = true;

                    // rank (otherwise waiting to accept invite)...
                    String rank = token.substring(pos_left + MSTR_MEMBER_RANK.length(), pos_right + 1);
                         if (rank.equals(OMUD_MMUD_DataBlock_Party.RANK_STRINGS[OMUD_MMUD_DataBlock_Party.eRank.MID.ordinal()]))
                         m.rank = OMUD_MMUD_DataBlock_Party.eRank.MID;
                    else if (rank.equals(OMUD_MMUD_DataBlock_Party.RANK_STRINGS[OMUD_MMUD_DataBlock_Party.eRank.BACK.ordinal()]))
                         m.rank = OMUD_MMUD_DataBlock_Party.eRank.BACK;
                    else m.rank = OMUD_MMUD_DataBlock_Party.eRank.FRONT;
                    pos_right = pos_left - 1;

                    // hp...
                    if ((pos_left  = token.lastIndexOf(MSTR_MEMBER_HP, pos_right)) > -1 &&
                        (pos_right = token.indexOf("]", pos_left)) > -1){
                        m.hp_perc = Integer.parseInt(token.substring(pos_left + MSTR_MEMBER_HP.length(), pos_right - 1).trim()); // -1 to skip '%'
                        pos_right = pos_left - 1;

                        // mana...
                        if ((pos_left  = token.lastIndexOf(MSTR_MEMBER_MANA, pos_right)) > -1 &&
                            (pos_right = token.indexOf("]", pos_left)) > -1){
                            m.ma_perc = Integer.parseInt(token.substring(pos_left + MSTR_MEMBER_MANA.length(), pos_right - 1).trim()); // -1 to skip '%'
                            pos_right = pos_left - 1;
                        }
                    }
                }

                // class...
                if ((pos_left  = token.lastIndexOf("(", pos_right)) > -1 &&
                    (pos_right = token.indexOf(")", pos_left))  > -1){
                    m.class_name = token.substring(pos_left + 1, pos_right);
                    pos_right = pos_left - 1;

                    // names...
                    m.name_first = token.substring(MSTR_MEMBER_NAME_PRE.length(), pos_right).trim();
                    String[] name_tokens = m.name_first.split(" ");
                    if (name_tokens.length == 2){
                        m.name_first = name_tokens[0];
                        m.name_last  = name_tokens[1];
                    }

                    // check/set leader...
                    if (m.name_first.equals(omcd.getMMUDData().dataParty.inviter_name))
                        omcd.getMMUDData().dataParty.leader_name = m.name_first;

                    // add here in case invited and not joined yet...
                    omcd.getMMUDData().dataParty.arrlMembers.add(m);
                }
            }

            // reset the inviter string (we should have the leader from above)...
            omcd.getMMUDData().dataParty.inviter_name = "";

        // ------------------
        // No Party Members (Solo)
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_PARTY_SOLO, "")) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.PARTY);
            omcd.getMMUDData().dataParty = new OMUD_MMUD_DataBlock_Party(omcd.getMMUDData().dataParty.inviter_name, "");

        // ------------------
        // Member Uninvited
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_UNINVITE_PRE, MSTR_UNINVITE_END)) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.PARTY);
            for (int i = 0; i < omcd.getMMUDData().dataParty.arrlMembers.size(); ++i)
                if (OMUD.compareSBString(omcd.getParseData().sbBlockDataLeft, omcd.getMMUDData().dataParty.arrlMembers.get(i).name_first)){
                    // if the array only has one member after the delete (this player), just recreate the party as empty...
                    if  (omcd.getMMUDData().dataParty.arrlMembers.size() > 2)
                         omcd.getMMUDData().dataParty.arrlMembers.remove(i);
                    else omcd.getMMUDData().dataParty = new OMUD_MMUD_DataBlock_Party();
                    break;
                }

        // ------------------
        // I Joined Party
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_JOINED_THEM, "")) > -1){
            omcd.getParseData().arrlGetTypes.add(OMUD_MMUD_DataBlock.eBlockType.PARTY);
            // set inviter here, leader set in main party parsing above...
            omcd.getMMUDData().dataParty.inviter_name = omcd.getParseData().sbBlockDataLeft.substring(0, omcd.getParseData().sbBlockDataLeft.length() - 1); // -1 to not include trailing LF

        // ------------------
        // I Left Party
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_LEFT_THEM,   "")) > -1){
            omcd.getParseData().arrlGetTypes.add(OMUD_MMUD_DataBlock.eBlockType.PARTY);

        // ------------------
        // I Moved Ranks
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_RANK_ME_FRONT, "")) > -1){
            updateMyRank(omcd, OMUD_MMUD_DataBlock_Party.eRank.FRONT);
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_RANK_ME_MID,   "")) > -1){
            updateMyRank(omcd, OMUD_MMUD_DataBlock_Party.eRank.MID);
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_RANK_ME_BACK,  "")) > -1){
            updateMyRank(omcd, OMUD_MMUD_DataBlock_Party.eRank.BACK);
        }

        return pos_data_left;
    }

    private void updateMyRank(OMUD_Char_Data omcd, OMUD_MMUD_DataBlock_Party.eRank rank){
        omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.PARTY);
        for (int i = 0; i < omcd.getMMUDData().dataParty.arrlMembers.size(); ++i)
            if (omcd.getMMUDData().dataParty.arrlMembers.get(i).name_first.equals(omcd.getMMUDData().dataStats.name_first)){
                omcd.getMMUDData().dataParty.arrlMembers.get(i).rank = rank;
                break;
            }
    }
}
