import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_SPause extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_NQC = "[0m(N)onstop, (Q)uit, or (C)ontinue?";
    private final String MSTR_HAK = "Hit any key to continue...";

    public static String getCmdText(){return "";}
    public OMUD_MMUD_ParseBlock_SPause(){}

    // ------------------
    // Nonstop/Quit/Continue
    // ------------------
    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        pos_data_right = omcd.getParseData().sbBuffer.length() - 1;
        if ((pos_data_left = findData(omcd.getParseData(), 0, pos_data_right, false, MSTR_NQC, "")) == -1)
             pos_data_left = findData(omcd.getParseData(), 0, pos_data_right, false, MSTR_HAK, "");
        return pos_data_left;
    }
}
