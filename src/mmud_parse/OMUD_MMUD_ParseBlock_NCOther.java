import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_NCOther extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_OPT_RESET_WHBL =      "[0;37;40m";
    private final String MSTR_MEDITATE_WAKE =       "[79D[KYou awake from deep meditation feeling stronger!";
    private final String MSTR_MEDITATE_WONT_HELP =  "[0;37;40mMeditation will not help at this time.";
    private final String MSTR_CMD_NO_EFFECT =       "[0;37mYour command had no effect.";
    private final String MSTR_BE_MORE_SPECIFIC =    "[79D[K[1;31mPlease be more specific.  You could have meant any of these:";
    private final String MSTR_REG_RESTING =         "You are now resting.";
    private final String MSTR_REG_MEDITATING =      "You are now meditating.";
    private final String MSTR_REALM_PRE =           "[79D[K";
    private final String MSTR_REALM_ENTER =         " just entered the Realm.";
    private final String MSTR_REALM_LEAVE =         " just left the Realm.";
    private final String MSTR_HANG_PRE =            "[79D[K[1;37m";
    private final String MSTR_HANG_END =            " just hung up!!!";

    //[0;37;40m[79D[KPlayer1 stops to rest.
    //[0;37;40m[79D[KPlayer1 kneels to meditate.

    public static String getCmdText(){return "";}
    public OMUD_MMUD_ParseBlock_NCOther(){}

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){

        // ------------------
        // Invalid Command (command has no effect)
        // ------------------
        if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_CMD_NO_EFFECT, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[MSTR_CMD_NO_EFFECT]\n");

        // ------------------
        // Meditate Wake
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_MEDITATE_WAKE, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[MSTR_MEDITATE_WAKE]\n");

        // ------------------
        // Meditate Won't Help
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_MEDITATE_WONT_HELP, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[MSTR_MEDITATE_WONT_HELP]\n");

        // ------------------
        // Be More Specific (Multiple Valid Targets)
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_BE_MORE_SPECIFIC, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[BE_MORE_SPECIFIC]\n" + omcd.getParseData().sbBlockDataLeft + "\n");

        // ------------------
        // Realm Connections
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_REALM_PRE, MSTR_REALM_ENTER)) > -1){
            omcd.getParseData().arrlDebugText.add("[MSTR_USER_ENTER: " + omcd.getParseData().sbBlockDataLeft + "]\n");
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_REALM_PRE, MSTR_REALM_LEAVE)) > -1){
            omcd.getParseData().arrlDebugText.add("[MSTR_USER_LEAVE: " + omcd.getParseData().sbBlockDataLeft + "]\n");
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_HANG_PRE,  MSTR_HANG_END)) > -1){
            omcd.getParseData().arrlDebugText.add("[MSTR_USER_HANG: "  + omcd.getParseData().sbBlockDataLeft + "]\n");

        // ------------------
        // Plain (Non-Colored) Text
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_REG_RESTING, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[MSTR_REG_RESTING]\n");
            pos_data_left = removeOptPrefix(omcd, "Rest Cmd When Already Resting", pos_data_left, MSTR_OPT_RESET_WHBL);
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, false, MSTR_REG_MEDITATING, "")) > -1){
            omcd.getParseData().arrlDebugText.add("[MSTR_REG_MEDITATING]\n");
            pos_data_left = removeOptPrefix(omcd, "Med Cmd When Already Meditating", pos_data_left, MSTR_OPT_RESET_WHBL);
        }

        return pos_data_left;
    }
}
