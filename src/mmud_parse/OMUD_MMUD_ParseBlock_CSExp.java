import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_CSExp extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_EXP_PRE =             "[0;37;40m[0;32mExp: [36m";
    private final String MSTR_EXP_END =             "%]";
    private final String MSTR_EXP_LEVEL =           "Level:";
    private final String MSTR_EXP_NEXT_TOTAL_PRE =  "(";
    private final String MSTR_EXP_NEXT_TOTAL_END =  ")";

    public static String getCmdText(){return "exp\n";}
    public OMUD_MMUD_ParseBlock_CSExp(){
        _arrlCmdText.add(new CmdText("experience", 3, CmdText.eDynamicType.STATIC));
    }

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_EXP_PRE, MSTR_EXP_END)) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.EXP);
            omcd.getMMUDData().dataExp = new OMUD_MMUD_DataBlock_Exp();
            cleanData(omcd.getParseData().sbBlockDataLeft, false, true);

            int pos_left =  0;
            int pos_right = 0;
            if ((pos_right = omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_EXP_LEVEL, pos_left)) > -1){
                omcd.getMMUDData().dataExp.cur_total = Long.parseLong(omcd.getParseData().sbBlockDataLeft.substring(pos_left, pos_right).trim());

                if ((pos_left  = omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_EXP_NEXT_TOTAL_PRE, pos_right)) > -1 &&
                    (pos_right = omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_EXP_NEXT_TOTAL_END, pos_left))  > -1){
                    omcd.getMMUDData().dataExp.next_total = Long.parseLong(omcd.getParseData().sbBlockDataLeft.substring(pos_left + MSTR_EXP_NEXT_TOTAL_PRE.length(), pos_right).trim());

                    if (omcd.getMMUDData().dataExp.next_total > omcd.getMMUDData().dataExp.cur_total)
                         omcd.getMMUDData().dataExp.next_rem = omcd.getMMUDData().dataExp.next_total - omcd.getMMUDData().dataExp.cur_total;
                    else omcd.getMMUDData().dataExp.next_rem = 0;

                    omcd.getMMUDData().dataExp.cur_perc = (int) (omcd.getMMUDData().dataExp.cur_total * 100.0 / omcd.getMMUDData().dataExp.next_total); // round down
                    //omcd.getMMUDData().dataExp.cur_perc = (int) ((omcd.getMMUDData().dataExp.cur_total * 100.0 / omcd.getMMUDData().dataExp.next_total) + 0.5); // round up
                }
            }
        }

        return pos_data_left;
    }
}
