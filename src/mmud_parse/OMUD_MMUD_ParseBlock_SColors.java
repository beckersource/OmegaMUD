import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_SColors extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_BOLD_WHITE =  "[79D[K[1;37m";
    //private final String MSTR_MAGENTA =     "[79D[K[0;35m";
    //private final String MSTR_CYAN =        "[79D[K[0;36m";
    private final String MSTR_BROWN =       "[79D[K[0;33m";

    private final String MSTR_YOU_CAPS =    "YOU";

    public static String getCmdText(){return "";}
    public OMUD_MMUD_ParseBlock_SColors(){}

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        // ------------------
        // Brown: Cast: Wears Off
        // ------------------
        if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_BROWN, "")) > -1){
            omcd.getParseData().arrlFoundTypes.add(OMUD_MMUD_DataBlock.eBlockType.COMBAT);
            cleanData(omcd.getParseData().sbBlockDataLeft, true, false);

            OMUD_MMUD_DataBlock_Combat.CombatLine cl = new OMUD_MMUD_DataBlock_Combat.CombatLine(OMUD_MMUD_DataBlock_Combat.eLineType.CAST_OFF);
            cl.unit =        new OMUD_MMUD_DataUnit(MSTR_YOU_CAPS);
            cl.unit_action = omcd.getParseData().sbBlockDataLeft.toString();
            omcd.getMMUDData().dataCombat.arrlLines.add(cl);

        // ------------------
        // White: mainly added for bears bbs mudrev newhaven advert spam
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_BOLD_WHITE, "\n")) > -1){
            omcd.getParseData().arrlDebugText.add("[MSTR_BOLD_WHITE_TEXT]\n" + omcd.getParseData().sbBlockDataLeft + "\n");
        }

        return pos_data_left;
    }
}
