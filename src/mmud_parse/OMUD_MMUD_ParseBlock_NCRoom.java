import java.util.ArrayList;

public class OMUD_MMUD_ParseBlock_NCRoom extends OMUD_MMUD_ParseBlocks.ParseBlock{
    private final String MSTR_UNIT_MOVE_PRE =   "[79D[K[1;33m"; // "A giant rat enters the room..."
    private final String MSTR_UNIT_MOVE_MID =   "[0;32m ";
    private final String MSTR_UNIT_ENTERS =    " from ";
    private final String MSTR_UNIT_LEAVES  =    "just left ";
    private final String MSTR_THE_ROOM =        " the room";
    private final String MSTR_TO_THE =          "to the ";
    private final String MSTR_THE =             "the ";
    private final String MSTR_UNIT_ENTER_A =    "A ";
    private final String MSTR_NOWHERE =         "nowhere";
    private final String MSTR_HEAR_MOVE =       "[79D[K[0;35mYou hear movement ";

    public static String getCmdText(){return "";}
    public OMUD_MMUD_ParseBlock_NCRoom(){}

    public int findBlockData(OMUD_Char_Data omcd, int pos_data_left, int pos_data_right, String strCmd){
        // ------------------
        // Unit Enters Room
        // ------------------
        if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_UNIT_MOVE_PRE, MSTR_UNIT_ENTERS)) > -1){
            int pos_left  =  0;
            int pos_right = -1;

            // "MSTR_UNIT_MOVE_MID" doesn't always exist for entering, so just remove it.
            // I noticed the cave bear in arena respawned as all yellow with no "MSTR_UNIT_MOVE_MID", so it might just be for bosses.
            cleanData(omcd.getParseData().sbBlockDataLeft, false, true);

            // remove "the room" suffix...
            if ((pos_left = omcd.getParseData().sbBlockDataLeft.lastIndexOf(MSTR_THE_ROOM)) > -1)
                omcd.getParseData().sbBlockDataLeft.delete(pos_left, omcd.getParseData().sbBlockDataLeft.length());
            // remove descriptive "into" or "in"-type adjective at the end...
            omcd.getParseData().sbBlockDataLeft.delete(omcd.getParseData().sbBlockDataLeft.lastIndexOf(" "), omcd.getParseData().sbBlockDataLeft.length());

            // get name and action...
            pos_left =  0;
            pos_right = omcd.getParseData().sbBlockDataLeft.lastIndexOf(" "); // space before action word
            // remove "A " prefix...
            if (omcd.getParseData().sbBlockDataLeft.substring(0, MSTR_UNIT_ENTER_A.length()).equals(MSTR_UNIT_ENTER_A))
                pos_left = MSTR_UNIT_ENTER_A.length();
            String strName =    omcd.getParseData().sbBlockDataLeft.substring(pos_left,      pos_right);
            String strAction =  omcd.getParseData().sbBlockDataLeft.substring(pos_right + 1, omcd.getParseData().sbBlockDataLeft.length());

            // get from...
            if (omcd.getParseData().sbBlockDataRight.substring(0, MSTR_THE.length()).equals(MSTR_THE))
                pos_left = MSTR_THE.length();
            String strFrom = omcd.getParseData().sbBlockDataRight.substring(pos_left, omcd.getParseData().sbBlockDataRight.length() - 2); // -1 to skip the ending LF and period punctuation

            // hack: for now, only refresh the room when unit enters from nowhere
            // to avoid enter spam when moving around and mobs chasing...
            if (strFrom.equals(MSTR_NOWHERE))
                omcd.getParseData().arrlGetTypes.add(OMUD_MMUD_DataBlock.eBlockType.ROOM);

            omcd.getParseData().arrlDebugText.add("[UNIT_ENTERS: " + strName + ", " + strAction + ", " + strFrom + "]\n");

        // ------------------
        // Unit Left Room
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_UNIT_MOVE_PRE, MSTR_UNIT_LEAVES)) > -1){
            omcd.getParseData().arrlGetTypes.add(OMUD_MMUD_DataBlock.eBlockType.ROOM);

            int pos_left  =  0;
            int pos_right = -1;

            // get the data fields...
            if ((pos_right = omcd.getParseData().sbBlockDataLeft.indexOf(MSTR_UNIT_MOVE_MID)) > -1){
                String strName =    omcd.getParseData().sbBlockDataLeft.substring(pos_left, pos_right);
                String strAction =  "left";
                // skip "to the" prefix...
                if (omcd.getParseData().sbBlockDataRight.substring(0, MSTR_TO_THE.length()).equals(MSTR_TO_THE))
                    pos_left = MSTR_TO_THE.length();
                String strFrom = omcd.getParseData().sbBlockDataRight.substring(pos_left, omcd.getParseData().sbBlockDataRight.length() - 2); // -1 to skip the ending LF and period punctuation

                omcd.getParseData().arrlDebugText.add("[UNIT_LEFT: " + strName + ", " + strAction + ", " + strFrom + "]\n");
            }
        // ------------------
        // Hear Movement
        // ------------------
        } else if ((pos_data_left = findData(omcd.getParseData(), pos_data_left, pos_data_right, true, MSTR_HEAR_MOVE, "")) > -1){
            int pos_left  = 0;
            int pos_right = omcd.getParseData().sbBlockDataLeft.length();
            // directionals: ends with period...
            if ((pos_left = omcd.getParseData().sbBlockDataLeft.lastIndexOf(" ")) > -1){
                pos_left++;
                pos_right =  omcd.getParseData().sbBlockDataLeft.length() - 2; // -1 skip LF and period
            // above or below you: ends with exclamation...
            } else {
                pos_right = omcd.getParseData().sbBlockDataLeft.indexOf(" ");
            }
            omcd.getParseData().arrlDebugText.add("[HEAR_MOVE: " + omcd.getParseData().sbBlockDataLeft.substring(pos_left, pos_right) + "]\n");
        }

        return pos_data_left;
    }
}
