import java.util.ArrayList;

public class OMUD_MMUD_DataItem{
    public static enum eItemLoc{
        NONE,   // not a real slot
        WEAPON,
        OFFHAND,
        HEAD,
        TORSO,
        ARMS,
        WRIST,  // 2 slots
        HANDS,
        WAIST,
        LEGS,
        FEET,
        BACK,
        FACE,
        EYES,
        EARS,
        NECK,
        FINGER, // 2 slots
        WORN,
        KEY
    }

    public static final String[] EQUIP_SLOT_STRINGS = {
        "",     // not a real slot
        "(weapon)",
        "(off-hand)",
        "(head)",
        "(torso)",
        "(arms)",
        "(wrist)",
        "(hands)",
        "(waist)",
        "(legs)",
        "(feet)",
        "(back)",
        "(face)",
        "(eyes)",
        "(ears)",
        "(neck)",
        "(finger)",
        "(worn)",
        ""
    };

    public static enum eSortType{
        NAME,
        EQUIP_SLOT
    }

    public int      id  =       -1;
    public int      qty =       1;
    public String   name =      "";
    public eItemLoc item_loc =  eItemLoc.NONE;

    public OMUD_MMUD_DataItem(String n)  {name = n;}
    public OMUD_MMUD_DataItem(OMUD_MMUD_DataItem item){
        id =        item.id;
        qty =       item.qty;
        name =      item.name;
        item_loc =  item.item_loc;
    }

    public static void sort(ArrayList<OMUD_MMUD_DataItem> arrl, eSortType sort_type, boolean sort_asc){
        for (int i = 0;   i < arrl.size(); i++){
        for (int j = i+1; j < arrl.size(); j++){
            // match sort types to java func compareToIgnoreCase():
            // less is negative, greater is positive, same is 0...
            int res = 0;
            if (sort_type == eSortType.EQUIP_SLOT)
                res = arrl.get(i).item_loc.ordinal() - arrl.get(j).item_loc.ordinal();
            else if (sort_type == eSortType.NAME)
                res = arrl.get(i).name.compareToIgnoreCase(arrl.get(j).name);

            if (res != 0){
                int bef = -1;
                int aft = -1;
                if (sort_asc && res > 0){
                    bef = i;
                    aft = j;
                } else if (!sort_asc && res < 0) {
                    bef = j;
                    aft = i;
                }

                if (bef > -1 && aft > -1){
                    OMUD_MMUD_DataItem diTemp = new OMUD_MMUD_DataItem(arrl.get(bef));
                    arrl.set(bef, arrl.get(aft));
                    arrl.set(aft, diTemp);
                }
            }
        }}
    }
}
