import java.util.ArrayList;

public class OMUD_MMUD_DataBlock_Who extends OMUD_MMUD_DataBlock{
    public static enum eAlignment{
        NEUTRAL,
        LAWFUL,
        SAINT,
        GOOD,
        SEEDY,
        OUTLAW,
        CRIMINAL,
        VILLAIN,
        FIEND
    }

    public static final String[] ALIGNMENT_STRINGS = {
        "Neutral", // shown as empty string in mud who list
        "Lawful",
        "Saint",
        "Good",
        "Seedy",
        "Outlaw",
        "Criminal",
        "Villain",
        "FIEND"
    };

    public static class WhoChar{
        public eAlignment   alignment =     eAlignment.NEUTRAL;
        public String       name_first =    "";
        public String       name_last =     "";
        public String       title =         "";
        public String       guild =         "";

        public WhoChar(){}
        public WhoChar(WhoChar dwc){
            alignment =     dwc.alignment;
            name_first =    dwc.name_first;
            name_last =     dwc.name_last;
            title =         dwc.title;
            guild =         dwc.guild;
        }
    }
    public ArrayList<WhoChar> chars = new ArrayList<WhoChar>();

    public eBlockType getType(){return eBlockType.WHO;}
    public OMUD_MMUD_DataBlock_Who(){}
    public OMUD_MMUD_DataBlock_Who(OMUD_MMUD_DataBlock_Who dw){
         for (int i = 0; i < dw.chars.size(); ++i)
            chars.add(new WhoChar(dw.chars.get(i)));
    }
}
