public abstract class OMUD_MMUD_DataBlock{
    public static int NAME_FIRST_MAXLEN = 10;
    public static int NAME_LAST_MAXLEN  = 18;
    public static int NAME_FULL_MAXLEN  = (NAME_FIRST_MAXLEN + NAME_LAST_MAXLEN + 1); // +1 for F/L space sep
    public static int CLASS_NAME_MAXLEN = 10;
    public static int NAME_GANG_MAXLEN  = 19;

    public static enum eBlockType{
        STATLINE,
        ROOM,
        INV,
        STATS,
        EXP,
        SHOP,
        SPELLS,
        WHO,
        PARTY,
        COMBAT,
        CHAT,
        TOP,
    }

    public static final String[] BLOCK_CMD_STRINGS = {
        OMUD_MMUD_ParseBlock_SStatline.getCmdText(),
        OMUD_MMUD_ParseBlock_CSRoom.getCmdText(),
        OMUD_MMUD_ParseBlock_CSInv.getCmdText(),
        OMUD_MMUD_ParseBlock_CSStats.getCmdText(),
        OMUD_MMUD_ParseBlock_CSExp.getCmdText(),
        OMUD_MMUD_ParseBlock_CDShop.getCmdText(),
        "", // SPELLS: called manually because of kai check arg
        OMUD_MMUD_ParseBlock_CDWho.getCmdText(),
        OMUD_MMUD_ParseBlock_CSParty.getCmdText(),
        OMUD_MMUD_ParseBlock_CSCombat.getCmdText(),
        OMUD_MMUD_ParseBlock_CSChat.getCmdText(),
        OMUD_MMUD_ParseBlock_CDTop.getCmdText()
    };

    public abstract eBlockType getType();
}
