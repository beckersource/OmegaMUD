import java.util.ArrayList;

public class OMUD_MMUD_DataBlock_Spells extends OMUD_MMUD_DataBlock{
    public static class Spell{
        public int      level =         0;
        public int      cost =          0;
        public String   name_short =    "";
        public String   name_long =     "";

        public Spell(){}
        public Spell(Spell sp){
            level =         sp.level;
            cost  =         sp.cost;
            name_short =    sp.name_short;
            name_long =     sp.name_long;
        }
    }
    public ArrayList<Spell> arrlSpells = new ArrayList<Spell>();

    public eBlockType getType(){return eBlockType.SPELLS;}
    public OMUD_MMUD_DataBlock_Spells(){}
    public OMUD_MMUD_DataBlock_Spells(OMUD_MMUD_DataBlock_Spells ds){
        for (int i = 0; i < ds.arrlSpells.size(); ++i)
            arrlSpells.add(new Spell(ds.arrlSpells.get(i)));
    }
}
