import java.util.ArrayList;

public class OMUD_MMUD_DataBlock_Top extends OMUD_MMUD_DataBlock{
    public static enum eTopType{
        PLAYERS_BY_ALL,
        PLAYERS_BY_CLASS,
        GANGS
    }

    public static class TopRow{
        public int      rank =          0;
        public String   strName =       "";
        public String   strClass =      "";
        public String   strGang =       "";
        public String   strLeader =     "";
        public int      members =       0;
        public String   strCreated =    "";
        public long     exp_base =      0;
        public long     exp_times =     0;
        public long     exp_plus =      0;

        public TopRow(){}
        public TopRow(TopRow tr){
            rank =          tr.rank;
            strName =       tr.strName;
            strClass =      tr.strClass;
            strGang =       tr.strGang;
            strLeader =     tr.strLeader;
            members =       tr.members;
            strCreated =    tr.strCreated;
            exp_base =      tr.exp_base;
            exp_times =     tr.exp_times;
            exp_plus =      tr.exp_plus;
        }
    }

    public eTopType type = eTopType.PLAYERS_BY_ALL;
    public ArrayList<TopRow> arrlRows = new ArrayList<TopRow>();

    public eBlockType getType(){return eBlockType.TOP;}
    public OMUD_MMUD_DataBlock_Top(){}
    public OMUD_MMUD_DataBlock_Top(eTopType t){type = t;}
    public OMUD_MMUD_DataBlock_Top(OMUD_MMUD_DataBlock_Top top){
        type = top.type;
        for (int i = 0; i < top.arrlRows.size(); ++i)
            arrlRows.add(new TopRow(top.arrlRows.get(i)));
    }
}
