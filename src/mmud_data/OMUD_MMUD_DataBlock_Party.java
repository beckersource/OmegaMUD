import java.util.ArrayList;

public class OMUD_MMUD_DataBlock_Party extends OMUD_MMUD_DataBlock{
    public static enum eRank{
        FRONT,
        MID,
        BACK,
    }

    public static final String[] RANK_STRINGS = {
        "Frontrank",
        "Midrank",
        "Backrank"
    };

    public static class Member{
        public boolean  accepted =      false;
        public String   name_first =    "";
        public String   name_last =     "";
        public String   class_name =    "";
        public int      hp_cur =        0;
        public int      ma_cur =        0;
        public int      hp_max =        0;
        public int      ma_max =        0;
        public int      hp_perc =       0;
        public int      ma_perc =       0;
        public eRank    rank =          eRank.FRONT;
        public Member(){}
        public Member(Member m){
            name_first =    m.name_first;
            name_last =     m.name_last;
            class_name =    m.class_name;
            accepted =      m.accepted;
            hp_cur =        m.hp_cur;
            ma_cur =        m.ma_cur;
            hp_max =        m.hp_max;
            ma_max =        m.ma_max;
            hp_perc =       m.hp_perc;
            ma_perc =       m.ma_perc;
            rank =          m.rank;
        }
    }

    public ArrayList<Member> arrlMembers = new ArrayList<Member>();
    public String   inviter_name =  "";
    public String   leader_name  =  "";

    public eBlockType getType(){return eBlockType.PARTY;}
    public OMUD_MMUD_DataBlock_Party(){}
    public OMUD_MMUD_DataBlock_Party(OMUD_MMUD_DataBlock_Party dp){
        inviter_name =  dp.inviter_name;
        leader_name =   dp.leader_name;
        for (int i = 0; i < dp.arrlMembers.size(); ++i)
            arrlMembers.add(new Member(dp.arrlMembers.get(i)));
    }
    public OMUD_MMUD_DataBlock_Party(String iname, String lname){
        inviter_name =  iname;
        leader_name =   lname;
    }
}
