import java.util.ArrayList;

public class OMUD_MMUD_DataBlock_Chat extends OMUD_MMUD_DataBlock{
    public static enum eChatType{
        GOSSIP,
        AUCTION,
        GANG,
        SAY_ROOM,
        SAY_DIR,
        YELL_ROOM,
        YELL_ADJ,
        TELEPATH
    }

    public static final String[] CHAT_CMD_STRINGS = {
        "gos",
        "auc",
        "bg",
        ".",
        ">",
        "\"",
        "\"",
        "/",
    };

    public static final String[] CHAT_TYPE_STRINGS = {
        "GOS",
        "AUC",
        "GNG",
        "SRM",
        "SDR",
        "YRM",
        "YAD",
        "TEL"
    };

    public static class Chat{
        public eChatType type =     eChatType.GOSSIP;
        public long   unix_time =   0;
        public String name_author = "";
        public String name_target = "";
        public String message =     "";
        public OMUD_MMUD_DataExit.eExitDir yell_dir = OMUD_MMUD_DataExit.eExitDir.NONE;
        public OMUD_MEGA.Command mega_cmd = null;

        public Chat(){}
        public Chat(Chat c){
            type =          c.type;
            unix_time =     c.unix_time;
            name_author =   c.name_author;
            name_target =   c.name_target;
            message =       c.message;
            yell_dir =      c.yell_dir;
            mega_cmd =      c.mega_cmd != null ? new OMUD_MEGA.Command(c.mega_cmd.type, c.mega_cmd.arg) : null;
        }
    }

    public ArrayList<Chat> arrlChats = new ArrayList<Chat>();

    public eBlockType getType(){return eBlockType.CHAT;}
    public OMUD_MMUD_DataBlock_Chat(){}
    public OMUD_MMUD_DataBlock_Chat(OMUD_MMUD_DataBlock_Chat dc){
        for (int i = 0; i < dc.arrlChats.size(); ++i)
            arrlChats.add(new Chat(dc.arrlChats.get(i)));
    }
}
