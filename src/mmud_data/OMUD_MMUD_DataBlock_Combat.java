import java.util.ArrayList;

public class OMUD_MMUD_DataBlock_Combat extends OMUD_MMUD_DataBlock{
    public static enum eLineType{
        COMBAT_ON,
        COMBAT_OFF,
        HIT,
        MISS,
        DODGE,
        DEFLECT,
        GLANCE,
        CAST_SUCCESS,
        CAST_FAIL,
        CAST_INVALID,
        CAST_TEXT,
        CAST_OFF
    }

    public static final String[] HIT_TYPE_STRINGS = {
        "COMBAT_ON",
        "COMBAT_OFF",
        "HIT",
        "MISS",
        "DODGE",
        "DEFLECT",
        "GLANCE",
        "CAST_SUCCESS",
        "CAST_FAIL",
        "CAST_INVALID",
        "CAST_TEXT",
        "CAST_OFF"
    };

    public static class CombatLine{
        public OMUD_MMUD_DataUnit unit = new OMUD_MMUD_DataUnit();
        public String       unit_action =   "";
        public String       tgt_name =      "";
        public String       tgt_weapon =    "";
        public int          tgt_dmg =       0;
        public eLineType    type =          eLineType.HIT;

        public CombatLine(){}
        public CombatLine(eLineType htype){type = htype;}
        public CombatLine(CombatLine cl){
            unit =          new OMUD_MMUD_DataUnit(cl.unit);
            unit_action =   cl.unit_action;
            tgt_name =      cl.tgt_name;
            tgt_weapon =    cl.tgt_weapon;
            tgt_dmg =       cl.tgt_dmg;
            type =          cl.type;
        }
    }

    /*
    public static class CombatRound{
        public ArrayList<CombatLine> arrlLines = new ArrayList<CombatLine>();

        public CombatRound(){}
        public CombatRound(CombatRound cr){
            for (int i = 0; i < cr.arrlLines.size(); ++i)
                arrlLines.add(new CombatLine(cr.arrlLines.get(i)));
        }
    }
    public ArrayList<CombatRound> rounds = new ArrayList<CombatRound>();
    */

    public boolean new_round = false;
    public ArrayList<CombatLine> arrlLines = new ArrayList<CombatLine>();

    public eBlockType getType(){return eBlockType.COMBAT;}
    public OMUD_MMUD_DataBlock_Combat(){}
    public OMUD_MMUD_DataBlock_Combat(OMUD_MMUD_DataBlock_Combat dc){
        new_round = dc.new_round;
        for (int i = 0; i < dc.arrlLines.size(); ++i)
            arrlLines.add(new CombatLine(dc.arrlLines.get(i)));
        //for (int i = 0; i < dc.rounds.size(); ++i)
        //    rounds.add(new CombatRound(dc.rounds.get(i)));
    }
}
