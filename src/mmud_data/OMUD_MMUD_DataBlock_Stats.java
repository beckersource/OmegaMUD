import java.util.ArrayList;

// OMUD_MMUD_DataBlock_Stats(): HP/MA in DataStatline, EXP in DataExp
public class OMUD_MMUD_DataBlock_Stats extends OMUD_MMUD_DataBlock{
    public static class Buff{
        String active_text = "";
        public Buff(String str){active_text = str;}
        public Buff(Buff b){
            active_text = b.active_text;
        }
    }

    public String   name_first =    "";
    public String   name_last =     "";
    public String   stats_race =    ""; // (see note below) -
    public String   stats_class =   ""; // 'stats' prefix because of reserved keyword 'class' (and race for consistency)
    public int      level =         -1;
    public int      lives =         -1;
    public int      cp =            -1;
    public int      str =           -1;
    public int      intel =         -1;
    public int      wil =           -1;
    public int      agi =           -1;
    public int      hea =           -1;
    public int      cha =           -1;
    public int      ac_ac =         -1;
    public int      ac_accy =       -1;
    public int      sc =            -1;
    public int      perc =          -1;
    public int      stealth =       -1;
    public int      thievery =      -1;
    public int      traps =         -1;
    public int      pick =          -1;
    public int      track =         -1;
    public int      ma =            -1;
    public int      mr =            -1;
    public boolean  str_mod =       false;
    public boolean  intel_mod =     false;
    public boolean  wil_mod =       false;
    public boolean  agi_mod =       false;
    public boolean  hea_mod =       false;
    public boolean  cha_mod =       false;
    public ArrayList<Buff> buffs = new ArrayList<Buff>();

    public eBlockType getType(){return eBlockType.STATS;}
    public OMUD_MMUD_DataBlock_Stats(){}
    public OMUD_MMUD_DataBlock_Stats(OMUD_MMUD_DataBlock_Stats ds){
        name_first =    ds.name_first;
        name_last =     ds.name_last;
        stats_race =    ds.stats_race;
        stats_class =   ds.stats_class;
        level =         ds.level;
        lives =         ds.lives;
        cp =            ds.cp;
        str =           ds.str;
        intel =         ds.intel;
        wil =           ds.wil;
        agi =           ds.agi;
        hea =           ds.hea;
        cha =           ds.cha;
        ac_ac =         ds.ac_ac;
        ac_accy =       ds.ac_accy;
        sc =            ds.sc;
        perc =          ds.perc;
        stealth =       ds.stealth;
        thievery =      ds.thievery;
        traps =         ds.traps;
        pick =          ds.pick;
        track =         ds.track;
        ma =            ds.ma;
        mr =            ds.mr;
        str_mod =       ds.str_mod;
        intel_mod =     ds.intel_mod;
        wil_mod =       ds.wil_mod;
        agi_mod =       ds.agi_mod;
        hea_mod =       ds.hea_mod;
        cha_mod =       ds.cha_mod;

        for (int i = 0; i < ds.buffs.size(); ++i)
            buffs.add(new Buff(ds.buffs.get(i)));
    }
}
