import java.util.ArrayList;

public class OMUD_MMUD_DataBlock_Exp extends OMUD_MMUD_DataBlock{
    public static class DeathExp{
        public long     exp =   0;
        public String   desc =  "";
        public String   money = "";
        public DeathExp(long exp_gained){
            exp = exp_gained;
        }
        public DeathExp(DeathExp ce){
            exp =   ce.exp;
            desc =  ce.desc;
            money = ce.money;
        }
    };

    public int  cur_perc =   -1;
    public long cur_total =   0;
    public long next_total = -1;
    public long next_rem =   -1;
    public long per_hr =     -1;
    public ArrayList<DeathExp> arrlCombatExp = new ArrayList<DeathExp>();

    public eBlockType getType(){return eBlockType.EXP;}
    public OMUD_MMUD_DataBlock_Exp(){}
    public OMUD_MMUD_DataBlock_Exp(OMUD_MMUD_DataBlock_Exp de){
        cur_total =     de.cur_total;
        cur_perc =      de.cur_perc;
        next_total =    de.next_total;
        next_rem =      de.next_rem;
        per_hr =        de.per_hr;
        for (int i = 0; i < de.arrlCombatExp.size(); ++i)
            arrlCombatExp.add(new DeathExp(de.arrlCombatExp.get(i)));
    }
}
