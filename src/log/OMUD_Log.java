import java.io.File;
import java.io.FileWriter;

public class OMUD_Log{
    // --------------
    // Console Logging
    // --------------
    public  static void toConsoleError(String msg)  {toConsole(msg, "ERR");}
    public  static void toConsoleInfo(String msg)   {toConsole(msg, "INF");}
    public  static void toConsoleDebug(String msg)  {toConsole(msg, "DBG");}
    private static void toConsole(String msg, String prefix){
        //SwingUtilities.invokeLater(new Runnable(){public void run(){
        //    _term.appendLocalANSI(prefix + msg + "]\n");}});
        System.out.println("[" + prefix + ": " + msg + "]");
    }

    // --------------
    // File Logging
    // --------------
    public static enum eLogTypes{
        TELNET_RAW,
        TELNET_CLEAN,
        TELNET_DEBUG,
        TELNET_CMDS,
        MUD_BUFFER,
        MUD_DEBUG,
        EVENTS_MUD,
        EVENTS_TELNET,
        EVENTS_GUI
    }

    private static final String[] LOG_FILENAME_STRINGS = {
        "log_telnet_raw.log",
        "log_telnet_clean.log",
        "log_telnet_debug.log",
        "log_telnet_cmds.log",
        "log_mud_buffer.log",
        "log_mud_debug.log",
        "log_events_mud.log",
        "log_events_telnet.log",
        "log_events_gui.log"
    };

    private static FileWriter[] LOG_WRITER = {
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    };

    public static void resetFiles(){
        try{
            for (int i = 0; i < eLogTypes.values().length; ++i){
                if (LOG_WRITER[i] != null)
                    LOG_WRITER[i].close();

                File f = new File(LOG_FILENAME_STRINGS[i]);
                if (f.exists())
                    f.delete();
                LOG_WRITER[i] = new FileWriter(f, true);
            }
        } catch (Exception e){
            OMUD_Log.toConsoleError("Error resetting log files: " + e.getMessage());
        }
    }

    public static void toFile(eLogTypes ltype, String data){
        try{
            if (LOG_WRITER[ltype.ordinal()] != null && data != null && data.length() > 0){
                LOG_WRITER[ltype.ordinal()].append(data);
                LOG_WRITER[ltype.ordinal()].flush();
            }
        } catch (Exception e){
            OMUD_Log.toConsoleError("Error logging data to file '" + LOG_FILENAME_STRINGS[ltype.ordinal()] + "': " + e.getMessage());
        }
    }
}
