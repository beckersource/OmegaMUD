@echo off
setlocal EnableDelayedExpansion
cls

set "JAR_OMUD=OmegaMUD_%1.jar"
set "BUILD_DIR=_BUILD_%1"
set "LIB_DIR=lib\%1"
set "MAN=MANIFEST.MF"
set "SRC=src"
set "LIB_FILES="
set "NEW_BUILD=%2"

echo -------------------------
echo Build: %JAR_OMUD% "%JAVA_HOME%"
echo -------------------------
echo.

REM ------------
REM Create Build Dir Structure
REM ------------
if exist %JAR_OMUD% del %JAR_OMUD%
if "%NEW_BUILD%" == "1" (rmdir /s /q %BUILD_DIR% >NUL)
if exist %BUILD_DIR% (
    del /f /q %BUILD_DIR%\*.class >NUL
) else (
    set "NEW_BUILD=1"
    mkdir %BUILD_DIR%\fonts
    copy fonts\*     %BUILD_DIR%\fonts >NUL
    copy src\%MAN%   %BUILD_DIR%       >NUL
    copy %LIB_DIR%\* %BUILD_DIR%       >NUL
)

REM ------------
REM Compile
REM ------------
REM build lib file list...
for %%F in (%LIB_DIR%\*) do set LIB_FILES=!LIB_FILES!%BUILD_DIR%/%%~nxF;
set LIB_FILES=%LIB_FILES:~1%
REM get source dirs...
set "SRC_DIRS=%SRC%\*.java"
for /d %%d in (%SRC%\*) do (set "SRC_DIRS=!SRC_DIRS! %%d\*.java")
REM compile...
javac -Xlint:deprecation -Xlint:unchecked -d %BUILD_DIR% -cp %LIB_FILES% %SRC_DIRS%

REM ------------
REM Check Class Files
REM ------------
set "SRC_FILES="
for /r %SRC% %%f in (*.java) do (set "SRC_FILES=!SRC_FILES! %BUILD_DIR%\%%~nf.class")
for %%f in (%SRC_FILES%) do (
	if not exist %%f (
		echo:
		echo ---------------------------------------------
	    echo Failed: class file not found: %%f
	    goto :EOF
	)
)

REM ------------
REM Create Jar
REM ------------
cd %BUILD_DIR%
REM expand the lib jars on a new build...
if "%NEW_BUILD%" == "1" ( for %%F in (*.jar) do jar xf %%~nxF )
REM create the omud jar...
jar cfm %JAR_OMUD% %MAN% *.class fonts org com net
cd ..\

REM ------------
REM Clean Up + Run Jar
REM ------------
if exist %BUILD_DIR%\%JAR_OMUD% (
	move /Y %BUILD_DIR%\%JAR_OMUD% .
	java -jar %JAR_OMUD%
)

REM ------------
REM EOF/Exit
REM ------------
:EOF
