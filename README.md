# OmegaMUD
Another attempt at a MegaMUD replacement in Java.

# Releases & Media Files ("OmegaMUD_Bin" Project)
- https://gitlab.com/beckersource/OmegaMUD_Bin

## Java Version Requirements
Tested with OpenJDK 1.6 (J6), 1.7 (J7), and 1.8 (J8).\
I assume equivalent Oracle Java versions will work.

## Apache Commons Net
Apache Commons Net telnet class files are built into the JAR during the build process.\
No external JARs are required.

## Environment Setup (Building/Running)
JAVA_HOME environment variable must be set to your Java location.\
The PATH environment variable on your system must have the Java bin path.

## Building
Run the batch (Win) or bash (Linux) script for the Java/Apache version you want to build for (see below).\
These scripts should be run from within the OmegaMUD dir:
```
_BUILD_J6.bat/sh
_BUILD_J7.bat/sh
_BUILD_J8.bat/sh
````
*Example: If you are building for Java 1.8, just run _BUILD_J8.bat (or .sh if on Linux)*

## Running
Command line run example for Java 1.6 version of OmegaMUD:
> java -jar OmegaMUD_J6.jar

*NOTE: the JAR filename will be different depending on which Java version it was built for.*\
*NOTE: only run builds with their respective Java versions. I have noticed threading and parsing issues when mixing versions.*

## What Works
* Network:
  * Telnet
  * ANSI/terminal support (required CSI/commands, adding more as needed).
  * Command throttling/queueing. Helpful for commands like search, pick, and bash.
* MegaMUD:
  * MegaMUD RoomID generation for backward MegaMUD support.
  * Remote Commands: @health, @exp, @join, @do, @party
* MME Database:
  * Basic open/query/close working.
* GUI:
  * Info display frame with combo drop-down of parsed MUD data groups.
  * Toggle between single and multi-char input (send) type.
  * Inventory list inside grid/table with paper doll and slot frames for drag-drop (FUTURE).
* Parsed MUD Data:
  * BBS Menu Detection
  * Editor Support + Prompt Detection
  * Statline
  * Rooms
  * Inventory
  * Stats
  * Exp
  * Shops
  * Spells
  * Party
  * Who's Online
  * Basic Combat
  * Chat (Gos/Auc/Gang/Tel/Say/Yell/Dir)
  * Top Lists (Players + Gangs)

## Tested Systems
WinXP, Win7, Win8, Win10\
Slackware Linux 15.0 x64\
Mac: Untested
